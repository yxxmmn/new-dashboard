<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<table style="display: inline-block;" border=1 frame=void rules=rows>
		<tr>
			<th align="center" colspan=3>Absolute Values</th>
		</tr>
		<tr>
			<th>Score</th>
			<th>Lower Limit</th>
			<th>Upper Limit</th>
		</tr>

		<tr>
			<th>1</th>
			<th>4</th>
			<th>1</th>
		</tr>

		<tr>
			<th>2</th>
			<th>5</th>
			<th>9</th>
		</tr>

		<tr>
			<th>3</th>
			<th>10</th>
			<th>20</th>
		</tr>

		<tr>
			<th>4</th>
			<th>21</th>
			<th>32</th>
		</tr>

		<tr>
			<th>5</th>
			<th>33</th>
			<th>50</th>
		</tr>

	</table>


	<table style="display: inline-block;" border=1 frame=void rules=rows>
		<tr>
			<th align="center" colspan=3>Percentile</th>
		</tr>
		<tr>
			<th>Score</th>
			<th>Lower Limit</th>
			<th>Upper Limit</th>
		</tr>

	</table>


	<table style="display: inline-block;" border=1 frame=void rules=rows>
		<tr>
			<th align="center" colspan=3>Gaussian (Normal)</th>
		</tr>
		<tr>
			<th>Score</th>
			<th>Lower Limit</th>
			<th>Upper Limit</th>
		</tr>

	</table>

	<br> Select FRM Calculation method
	<form action="some.jsp">
		<select name="Method">
			<option value="absolute">Absolute</option>
			<option value="percentile">Percentile</option>
			<option value="normal">Gaussian (Normal)</option>
		</select> <input type="submit" value="Submit">
	</form>






</body>
</html>