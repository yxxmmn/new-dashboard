package com.dashboard.dashboardcloud.type;

public enum RolesType {
	D(1), S(1), E(1), R(2), INV(3), MGT(2), CRM(5);

	private int roleCode;

	private RolesType(int roleCode) {
		this.roleCode = roleCode;

	}

	public int getRoleCode() {
		return roleCode;
	}
}
