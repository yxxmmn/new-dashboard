package com.dashboard.dashboardcloud.type;

public enum LogsType {
	// why prime number ya?
	// because i can use the prime number to do a combination in the future for
	// selective notification.

	RECEIPT(2), NOSALES(3), VOID(5), CLOCKIN(7), CLOCKOUT(11), CASHDRAWER(13), CLOSESALES(17), PRODUCT_SYNC(19);

	private int logCode;

	private LogsType(int logCode) {
		this.logCode = logCode;

	}

	public int getLogCode() {
		return logCode;
	}

	public static void main(String[] args) {
		System.out.println(LogsType.RECEIPT.toString());
		System.out.println(LogsType.RECEIPT.getLogCode());
	}
}
