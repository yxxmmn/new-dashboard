package com.dashboard.dashboardcloud.model.dao;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "")
public abstract class VarDao<KEY> extends GenericDao<KEY> {
	protected String var1;
	protected String var2;
	protected String var3;
	protected String var4;
	protected String var5;
	protected String var6;
	protected String var7;
	protected String var8;
	protected String var9;
	protected String var10;
	protected String var11;
	protected String var12;

	public VarDao() {
	}

	public String getVar1() {
		return var1;
	}

	public void setVar1(String var1) {
		this.var1 = var1;
	}

	public String getVar2() {

		return var2;
	}

	public void setVar2(String var2) {
		this.var2 = var2;
	}

	public String getVar3() {

		return var3;
	}

	public void setVar3(String var3) {
		this.var3 = var3;
	}

	public String getVar4() {
		return var4;
	}

	public void setVar4(String var4) {
		this.var4 = var4;
	}

	public String getVar5() {
		return var5;
	}

	public void setVar5(String var5) {
		this.var5 = var5;
	}

	public String getVar6() {
		return var6;
	}

	public void setVar6(String var6) {
		this.var6 = var6;
	}

	public String getVar7() {
		return var7;
	}

	public void setVar7(String var7) {
		this.var7 = var7;
	}

	public String getVar8() {
		return var8;
	}

	public void setVar8(String var8) {
		this.var8 = var8;
	}

	public String getVar9() {
		return var9;
	}

	public void setVar9(String var9) {
		this.var9 = var9;
	}

	public String getVar10() {
		return var10;
	}

	public void setVar10(String var10) {
		this.var10 = var10;
	}

	public String getVar11() {
		return var11;
	}

	public void setVar11(String var11) {
		this.var11 = var11;
	}

	public String getVar12() {
		return var12;
	}

	public void setVar12(String var12) {
		this.var12 = var12;
	}

	@Override
	public abstract String getSpecificWhereClause();

	@Override
	public abstract String getInsertStatement();

}
