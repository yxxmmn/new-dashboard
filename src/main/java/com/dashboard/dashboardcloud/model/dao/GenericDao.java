package com.dashboard.dashboardcloud.model.dao;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "")
public abstract class GenericDao<Key> {

	public GenericDao() {
	}

	public String getSelectStatement() {
		String className = this.getClass().getSimpleName().toLowerCase();
		className = className.substring(0, className.length() - 3);
		return "select * from " + className;
	}

	public abstract String getSpecificWhereClause();

	public abstract String getInsertStatement();

	public abstract Key getId();
}
