package com.dashboard.security;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

public class MyAppConfig extends Application {
	@Override
	public Set<Class<?>> getClasses() {
		final Set<Class<?>> classes = new HashSet<Class<?>>();
		classes.add(ResponseCorsFilter.class);
		classes.add(AuthenticationFilter.class);
		classes.add(RoleAuthenticationFilter.class);
		return classes;
	}
}
