package com.dashboard.security;
import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;

public class ResponseCorsFilter implements ContainerResponseFilter {

	@Override
	public void filter(ContainerRequestContext req,
			ContainerResponseContext contResp) throws IOException {
		contResp.getHeaders().add("Access-Control-Allow-Origin", "*");
		contResp.getHeaders().add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		contResp.getHeaders().add("Access-Control-Allow-Credentials", "true");
		contResp.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
		contResp.getHeaders().add("Access-Control-Max-Age", "");
		
	}
 
}

