package com.dashboard.security;

import java.io.IOException;
import java.util.List;
import java.util.StringTokenizer;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.internal.util.Base64;

import com.dashboard.exception.RedException;

@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

	@Context
	private HttpServletRequest resourceRequest;
	private static final String AUTHORIZATION_PROPERTY = "Authorization";
	private static final String AUTHENTICATION_SCHEME = "Basic";

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		// get request headers
		MultivaluedMap<String, String> headers = requestContext.getHeaders();
		// Fetch authorization header
		List<String> authorization = headers.get(AUTHORIZATION_PROPERTY);

		// If no authorization information present; block access
		if (authorization == null || authorization.isEmpty()) {
			System.out.println("Need specific header");
			throw new RedException("No header");
		}
		// Get encoded username and password
		final String encodedUserPassword = authorization.get(0).replaceFirst(AUTHENTICATION_SCHEME + " ", "");

		// Decode username and password
		String usernameAndPassword = new String(Base64.decode(encodedUserPassword.getBytes()));

		// Split username and password tokens
		final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
		final String username = tokenizer.nextToken();
		final String password = tokenizer.nextToken();

		// Verifying Username and password
		//Outlets outlet = validateOutlet(username, password);
		// if (outlet == null) {
		// System.out.println("Wrong username or password");
		// throw new RedException("Wrong username or password");
		// } else {
		// resourceRequest.setAttribute("outletCompany",
		// outlet.getCompanyName());
		// resourceRequest.setAttribute("outlet", outlet.getTerminalName());
		// resourceRequest.setAttribute("outletDisplayName",
		// outlet.getDisplayName());
		// }

	}

	// private Outlets validateOutlet(final String username, final String
	// password) {
	// Outlets outlet = new Command2<Outlets>(null) {
	// @Override
	// public Outlets run(Connection conn) {
	// String sql =
	// "SELECT * FROM flexcloud.outlets WHERE terminalName = :username AND password = :password AND isAvailable = TRUE";
	//
	// Outlets results = conn.createQuery(sql).addParameter("username",
	// username).addParameter("password",
	// password).executeAndFetchFirst(Outlets.class);
	// return results;
	// }
	// }.execute();
	//
	// return outlet;
	// }
}
