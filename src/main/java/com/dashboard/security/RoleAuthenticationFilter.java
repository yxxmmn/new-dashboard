package com.dashboard.security;

import java.io.IOException;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.dashboard.dashboardcloud.type.RolesType;

@RoleSecured
@Provider
@Priority(Priorities.AUTHORIZATION)
public class RoleAuthenticationFilter implements ContainerRequestFilter {

	@Context
	private ResourceInfo resourceInfo;
	@Context
	private HttpServletRequest resourceRequest;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {

		// Get the resource class which matches with the requested URL
		// Extract the roles declared by it
		Class<?> resourceClass = resourceInfo.getResourceClass();
		List<RolesType> classRoles = extractRoles(resourceClass);

		// Get the resource method which matches with the requested URL
		// Extract the roles declared by it
		Method resourceMethod = resourceInfo.getResourceMethod();
		List<RolesType> methodRoles = extractRoles(resourceMethod);

		// Check if the user is allowed to execute the method
		// The method annotations override the class annotations
		boolean authorised = false;
		if (methodRoles.isEmpty()) {
			// authorised = checkPermissions(classRoles);
		} else {
			// authorised = checkPermissions(methodRoles);
		}

		if (!authorised) {
			requestContext.abortWith(Response.status(Response.Status.FORBIDDEN).build());
		}

	}

	// Extract the roles from the annotated element
	private List<RolesType> extractRoles(AnnotatedElement annotatedElement) {
		if (annotatedElement == null) {
			return new ArrayList<RolesType>();
		} else {
			RoleSecured secured = annotatedElement.getAnnotation(RoleSecured.class);
			if (secured == null) {
				return new ArrayList<RolesType>();
			} else {
				RolesType[] allowedRoles = secured.value();
				return Arrays.asList(allowedRoles);
			}
		}
	}

	// private boolean checkPermissions(List<RolesType> allowedRoles) {
	// // Check if the user contains one of the allowed roles
	// // Throw an Exception if the user has not permission to execute the
	// // method
	// HttpSession session = resourceRequest.getSession(true);
	// Object account = session.getAttribute("account");
	// if (account instanceof CredentialsDto) {
	// int authoriseLevel = ((CredentialsDto) account).getAuthoriseLevel();
	// for (RolesType role : allowedRoles) {
	// if (authoriseLevel % role.getRoleCode() == 0) {
	// return true;
	// }
	// }
	// return false;
	// }
	//
	// if (AccountResource.onDebug) {
	// return true;
	// }
	// throw new
	// SessionExpiredException("Your session is expired or kicked out");
	// }

}
