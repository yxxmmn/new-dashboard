package com.dashboard.resource;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.EventDateTime;

import org.apache.commons.lang3.RandomStringUtils;
import org.joda.time.Days;
import org.mortbay.log.Log;
import org.sql2o.Sql2oException;

import com.dashboard.command.*;
import com.dashboard.dao.*;

import bean.CalendarSample;
import bean.JodaDateHandler;
import bean.EmailHandler;

@Path("/resource")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class DashboardResource {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/login/")
	public Response login(AllEmployees e) {

		String loginID = e.getLoginID();
		String password = e.getPassword();

		List<EmployeeID> rList = new AuthenticateLogin(loginID, password).execute();
		GenericEntity<List<EmployeeID>> ety = new GenericEntity<List<EmployeeID>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/appointmentsForTheDay/")
	public Response appointments() {

		JodaDateHandler ds = new JodaDateHandler();
		String s1 = ds.todayDate();
		String e1 = ds.endOfToday();

		List<Appointment> rList = new GetAppointmentsForTheDay(s1, e1).execute();
		GenericEntity<List<Appointment>> ety = new GenericEntity<List<Appointment>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/allAppointments/")
	public Response allAppointments() {

		List<AllAppointments> rList = new GetAllAppointments().execute();
		GenericEntity<List<AllAppointments>> ety = new GenericEntity<List<AllAppointments>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}
	
	@GET
	@Path("/getRFMSingleValues/")
	public Response rfmGetRFMSingleValues() {

		//Add in clientID here to call rfmGetDetailsByID() and populate 3 fields for RFM
		//Sort these logics...
		//Call RFM_Logic to return fields
		//Note: Need to call Single/Company based on clientID !!! But, ArrayList is constantly generated again every API call.
		
		//Should i create new class for the dateBecomeCustomer?
		
		//Get RFM values from single_DB in List
		List<RFM> rList = new RFMGetRFMSingleValues().execute();
		GenericEntity<List<RFM>> ety = new GenericEntity<List<RFM>>(rList) {

		};

//		System.out.println(rList.get(0).getFrequency()); // 5
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getRFMCompanyValues/")
	public Response rfmGetRFMCompanyValues() {

		//Get RFM values from company_DB in List
		List<RFM> rList = new RFMGetRFMCompanyValues().execute();
		GenericEntity<List<RFM>> ety = new GenericEntity<List<RFM>>(rList) {

		};

		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}
	
	@GET
	@Path("/getRFMDetailsByID/{clientID}")
	public Response rfmGetDetailsByID(@PathParam("clientID") final int clientID) {

		//Get RFM values from company_DB in List
		List<RFMGetDetailsByIDClass> rList = new RFMGetDetailsByID(clientID).execute();
//		GenericEntity<List<RFMGetDetailsByIDClass>> ety = new GenericEntity<List<RFMGetDetailsByIDClass>>(rList) {
//
//		};
		int totalBottles = rList.get(0).getTotalBottles();
		
		//Checks if clientID given is a customer
		if(totalBottles!=0) {
			int totalInvoiceAmount = rList.get(0).getTotalInvoiceAmount(); 
			
			String clientCategory = rList.get(0).getClientCategory();
			JodaDateHandler jdh = new JodaDateHandler();
			int recencyValue = jdh.daysBeforeNowLastDateOrdered(rList) ; 
			int numberOfDaysCust = jdh.daysBeforeNowDateBecomeCust(rList) ; 
			double noOfMthCust = (double)numberOfDaysCust/30; // Assumption: 30 days every month
			String clientName = rList.get(0).getClientName();
			String empInCharge = rList.get(0).getEmpInCharge();
			String sakeOrderID = rList.get(0).getSakeOrderID();
			String lastDateOrdered = rList.get(0).getLastDateOrdered().toString();
			List<RFM> rfmList;
			
			//Retrieve the relevant RFM values/threshold for Single/Company (Assumption: not single means company)
			if(clientCategory.equals("Single")) {
				System.out.println("Running RFM for Single");
				rfmList = new RFMGetRFMSingleValues().execute();
			} else {
				System.out.println("Running RFM for Company");
				rfmList = new RFMGetRFMCompanyValues().execute();
			}
	
			//Sorts Logic @ RFMLogic class and return the String
			RFMLogic rfmLogic = new RFMLogic();
			String stringResult = rfmLogic.doSort(rfmList, recencyValue , totalBottles/noOfMthCust,totalInvoiceAmount); //need to divide totalBottles by mthOfbecomeCus.
			String customerRFMCategory = rfmLogic.sortCategory(stringResult);
			
			System.out.println("recencyValue: "+recencyValue);
			System.out.println("totalBottles/noOfMthCust: "+totalBottles/noOfMthCust);
			System.out.println("noOfMthCust: "+noOfMthCust);
			System.out.println("totalInvoiceAmount: "+totalInvoiceAmount);
			System.out.println("stringResult: "+stringResult);
			System.out.println("customerRFMCategory: "+customerRFMCategory);
			
			RFMReturnFormat returnFormat = new RFMReturnFormat(clientName,clientCategory,empInCharge,sakeOrderID,lastDateOrdered,recencyValue+"",totalBottles+"",totalInvoiceAmount+"",customerRFMCategory);
			//ArrayList<RFMReturnFormat> rfmArrayList = new ArrayList<RFMReturnFormat>();
			//rfmArrayList.add(returnFormat);
		    
			//Problem: ArrayList is created here for returnFormatList, but creating GenericEntity 'ety' requires a List type, else it prompts the error:
			//SEVERE: MessageBodyWriter not found for media type=application/json;charset=utf-8, type=class java.util.ArrayList, 
			//genericType=java.util.List<com.dashboard.dao.RFMReturnFormat>.

			ArrayList<RFMReturnFormat> returnFormatList = new ArrayList<RFMReturnFormat>();
			returnFormatList.add(returnFormat);
			GenericEntity<ArrayList<RFMReturnFormat>> ety = new GenericEntity<ArrayList<RFMReturnFormat>>(returnFormatList) {

			};	

			
			return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();
		} else {
			//Handle Exception that clientID given is not a client
			System.out.println("ClientID: "+clientID+ " is not a Customer.");
		}
		return null;
	}

	@GET
	@Path("/getAppointmentByAppointmentID/{aid}")
	public Response getAppointmentByAppointmentID(@PathParam("aid") final String aid) {

		List<AllAppointments> rList = new GetAppointmentByAppointmentID(aid).execute();
		GenericEntity<List<AllAppointments>> ety = new GenericEntity<List<AllAppointments>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/allAppointmentsByStatus/{appointmentStatusID}")
	public Response allAppointmentsByStatus(@PathParam("appointmentStatusID") final int appointmentStatusID) {

		List<AllAppointments> rList = new GetAllApppointmentsByStatus(appointmentStatusID).execute();
		GenericEntity<List<AllAppointments>> ety = new GenericEntity<List<AllAppointments>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/allAppointmentsByEmpID/{eID}")
	public Response allAppointmentsByEmpID(@PathParam("eID") final int eID) {

		List<AllAppointments> rList = new GetAllAppointmentsByEmployee(eID).execute();
		GenericEntity<List<AllAppointments>> ety = new GenericEntity<List<AllAppointments>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/allAppointmentsByClientID/{cID}")
	public Response allAppointmentsByClientID(@PathParam("cID") final int cID) {

		List<AllAppointments> rList = new GetAllAppointmentsByClient(cID).execute();
		GenericEntity<List<AllAppointments>> ety = new GenericEntity<List<AllAppointments>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/appointmentStatusDropdown/")
	public Response appointmentStatusDropdown() {

		List<AppointmentStatusDropdown> rList = new GetAppointmentStatusDropdown().execute();
		GenericEntity<List<AppointmentStatusDropdown>> ety = new GenericEntity<List<AppointmentStatusDropdown>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/appointmentGetInvoiceByAppointmentID/{appointmentID}")
	public Response appointmentGetInvoiceByAppointmentID(@PathParam("appointmentID") final String appointmentID) {

		List<InvoiceAmount> rList = new GetInvoiceAmountForAppointment(appointmentID).execute();
		GenericEntity<List<InvoiceAmount>> ety = new GenericEntity<List<InvoiceAmount>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/appointmentGetInvoiceIDByAppointmentID/{appointmentID}")
	public Response appointmentGetInvoiceIDByAppointmentID(@PathParam("appointmentID") final String appointmentID) {

		List<InvoiceIDFromAppointment> rList = new GetInvoiceIDFromAppointment(appointmentID).execute();
		GenericEntity<List<InvoiceIDFromAppointment>> ety = new GenericEntity<List<InvoiceIDFromAppointment>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/appointmentGetShortlistedSakeByAppointmentID/{appointmentID}")
	public Response appointmentGetShortlistedSakeByAppointmentID(
			@PathParam("appointmentID") final String appointmentID) {

		List<AppointmentShortlistedSake> rList = new GetAppointmentShortlistedSake(appointmentID).execute();
		GenericEntity<List<AppointmentShortlistedSake>> ety = new GenericEntity<List<AppointmentShortlistedSake>>(
				rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/appointmentGetListOfPurchase/{invoiceID}")
	public Response appointmentGetListOfPurchase(@PathParam("invoiceID") final int invoiceID) {

		List<ListOfPurchase> rList = new GetListOfPurchase(invoiceID).execute();
		GenericEntity<List<ListOfPurchase>> ety = new GenericEntity<List<ListOfPurchase>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/bestSellersForTheMonth/")
	public Response bestSellers() {

		JodaDateHandler ds = new JodaDateHandler();
		String e1 = ds.todayDate();
		String s1 = ds.oneMonthAgo(e1);

		List<BestSeller> rList = new GetBestSellersForTheMonth(s1, e1).execute();
		GenericEntity<List<BestSeller>> ety = new GenericEntity<List<BestSeller>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/bestSellersByCategory/{s}/{e}/{cat}")
	public Response bestSellersByCategory(@PathParam("s") final String s, @PathParam("e") final String e,
			@PathParam("cat") final String cat) {

		List<BestSeller> rList = new GetBestSellersByCategory(s, e, cat).execute();
		GenericEntity<List<BestSeller>> ety = new GenericEntity<List<BestSeller>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/topSalespersonForTheMonth/")
	public Response topSalespersonForTheMonth() {

		JodaDateHandler ds = new JodaDateHandler();
		String e1 = ds.todayDate();
		String s1 = ds.oneMonthAgo(e1);

		List<TopXEmployee> rList = new GetTopXEmployee(s1, e1).execute();
		GenericEntity<List<TopXEmployee>> ety = new GenericEntity<List<TopXEmployee>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/mostImprovedSalespersonForTheMonth/")
	public Response mostImprovedSalespersonForTheMonth() {

		JodaDateHandler ds = new JodaDateHandler();
		String e1 = ds.todayDate();
		String s1 = ds.oneMonthAgo(e1);
		String s2 = ds.oneMonthAgo(s1);
		String e2 = s1;

		List<MostImprovedEmployee> rList = new GetXMostImprovedEmployees(s1, e1, s2, e2).execute();
		GenericEntity<List<MostImprovedEmployee>> ety = new GenericEntity<List<MostImprovedEmployee>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/invoiceComparisonWithPastMonth/")
	public Response invoiceComparisonWithPastMonth() {

		JodaDateHandler ds = new JodaDateHandler();
		String e1 = ds.todayDate();
		String s1 = ds.oneMonthAgo(e1);
		String s2 = ds.oneMonthAgo(s1);
		String e2 = s1;

		List<InvoiceAmountWithComparison> rList = new GetInvoiceComparisonWithPastMonth(s1, e1, s2, e2).execute();
		GenericEntity<List<InvoiceAmountWithComparison>> ety = new GenericEntity<List<InvoiceAmountWithComparison>>(
				rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/contactsComparisonWithPastMonth/")
	public Response leadsComparisonWithPastMonth() {

		JodaDateHandler ds = new JodaDateHandler();
		String e1 = ds.todayDate();
		String s1 = ds.oneMonthAgo(e1);
		String s2 = ds.oneMonthAgo(s1);
		String e2 = s1;

		List<NewLeadsWithComparison> rList = new GetNewLeadsWithComparisonWIthPastMonth(s1, e1, s2, e2).execute();
		GenericEntity<List<NewLeadsWithComparison>> ety = new GenericEntity<List<NewLeadsWithComparison>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/appointmentsComparisonWithPastMonth/")
	public Response appointmentsComparisonWithPastMonth() {

		JodaDateHandler ds = new JodaDateHandler();
		String e1 = ds.todayDate();
		String s1 = ds.oneMonthAgo(e1);
		String s2 = ds.oneMonthAgo(s1);
		String e2 = s1;

		List<AppointmentsWithComparison> rList = new GetAppointmentsComparisonWithPastMonth(s1, e1, s2, e2).execute();
		GenericEntity<List<AppointmentsWithComparison>> ety = new GenericEntity<List<AppointmentsWithComparison>>(
				rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/prospectsComparisonWithPastMonth/")
	public Response prospectsComparisonWithPastMonth() {
		JodaDateHandler ds = new JodaDateHandler();
		String e1 = ds.todayDate();
		String s1 = ds.oneMonthAgo(e1);
		String s2 = ds.oneMonthAgo(s1);
		String e2 = s1;

		List<ProspectsWithComparison> rList = new GetProspectsComparisonWithPastMonth(s1, e1, s2, e2).execute();
		GenericEntity<List<ProspectsWithComparison>> ety = new GenericEntity<List<ProspectsWithComparison>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/customersComparisonWithPastMonth/")
	public Response customersComparisonWithPastMonth() {

		JodaDateHandler ds = new JodaDateHandler();
		String e1 = ds.todayDate();
		String s1 = ds.oneMonthAgo(e1);
		String s2 = ds.oneMonthAgo(s1);
		String e2 = s1;

		List<CustomersWithComparison> rList = new GetCustomersComparisonWithPastMonth(s1, e1, s2, e2).execute();
		GenericEntity<List<CustomersWithComparison>> ety = new GenericEntity<List<CustomersWithComparison>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/numberOfInvoiceComparisonWithPastMonth/")
	public Response numberOfInvoiceComparisonWithPastMonth() {

		JodaDateHandler ds = new JodaDateHandler();
		String e1 = ds.todayDate();
		String s1 = ds.oneMonthAgo(e1);
		String s2 = ds.oneMonthAgo(s1);
		String e2 = s1;

		List<NumberOfInvoiceWithComparison> rList = new GetNumberOfInvoiceComparisonWithPastMonth(s1, e1, s2, e2)
				.execute();
		GenericEntity<List<NumberOfInvoiceWithComparison>> ety = new GenericEntity<List<NumberOfInvoiceWithComparison>>(
				rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/salesBreakdownForMonth/")
	public Response salesBreakdownForMonth() {

		JodaDateHandler ds = new JodaDateHandler();
		String e1 = ds.todayDate();
		String s1 = ds.oneMonthAgo(e1);

		List<SalesBreakdownForMonth> rList = new GetSalesBreakdownForMonth(s1, e1).execute();
		GenericEntity<List<SalesBreakdownForMonth>> ety = new GenericEntity<List<SalesBreakdownForMonth>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/ordersBreakdownForMonth/")
	public Response ordersBreakdownForMonth() {

		JodaDateHandler ds = new JodaDateHandler();
		String e1 = ds.todayDate();
		String s1 = ds.oneMonthAgo(e1);

		List<OrdersBreakdownForMonth> rList = new GetOrdersBreakdownForMonth(s1, e1).execute();
		GenericEntity<List<OrdersBreakdownForMonth>> ety = new GenericEntity<List<OrdersBreakdownForMonth>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllEmployees/")
	public Response getAllEmployees() {

		List<AllEmployees> rList = new GetAllEmployees().execute();
		GenericEntity<List<AllEmployees>> ety = new GenericEntity<List<AllEmployees>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getEmployeeByID/{empID}")
	public Response ordersBreakdownForMonth(@PathParam("empID") final int empID) {

		List<AllEmployees> rList = new GetEmployeeByID(empID).execute();
		GenericEntity<List<AllEmployees>> ety = new GenericEntity<List<AllEmployees>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getEventByID/{eventID}")
	public Response getEventByID(@PathParam("eventID") final String eventID) {

		List<Events> rList = new GetEventByID(eventID).execute();
		GenericEntity<List<Events>> ety = new GenericEntity<List<Events>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getClientByID/{custID}")
	public Response getClientByID(@PathParam("custID") final int customerID) {

		List<AllClients> rList = new GetClientByID(customerID).execute();
		GenericEntity<List<AllClients>> ety = new GenericEntity<List<AllClients>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getSakeByID/{sakeID}")
	public Response getSakeByID(@PathParam("sakeID") final int sakeID) {

		List<Sake> rList = new GetSakeByID(sakeID).execute();
		GenericEntity<List<Sake>> ety = new GenericEntity<List<Sake>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllActiveEmployees/")
	public Response getAllActiveEmployees() {

		List<AllEmployees> rList = new GetAllActiveEmployees().execute();
		GenericEntity<List<AllEmployees>> ety = new GenericEntity<List<AllEmployees>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllInactiveEmployees/")
	public Response getAllInactiveEmployees() {

		List<AllEmployees> rList = new GetAllInactiveEmployees().execute();
		GenericEntity<List<AllEmployees>> ety = new GenericEntity<List<AllEmployees>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getCustomerCategories/")
	public Response getCustomerCategories() {

		List<AllCustomerCategories> rList = new GetAllCustomerCategories().execute();
		GenericEntity<List<AllCustomerCategories>> ety = new GenericEntity<List<AllCustomerCategories>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getEmployeePositions/")
	public Response getEmployeePositions() {

		List<EmployeePosition> rList = new GetEmployeePositions().execute();
		GenericEntity<List<EmployeePosition>> ety = new GenericEntity<List<EmployeePosition>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllSakeBrands/")
	public Response getSakeBrands() {

		List<AllSakeBrand> rList = new GetAllSakeBrand().execute();
		GenericEntity<List<AllSakeBrand>> ety = new GenericEntity<List<AllSakeBrand>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getEmployeeDropdown/")
	public Response getEmployeeDropdown() {

		List<EmployeeDropdown> rList = new GetEmployeeDropdown().execute();
		GenericEntity<List<EmployeeDropdown>> ety = new GenericEntity<List<EmployeeDropdown>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getEmployeePostionDropdown/")
	public Response getEmployeePostionDropdown() {

		List<EmployeePositionDropdown> rList = new GetEmployeePositionDropdown().execute();
		GenericEntity<List<EmployeePositionDropdown>> ety = new GenericEntity<List<EmployeePositionDropdown>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllClientCategories/")
	public Response getAllClientCategories() {

		List<AllClientCategories> rList = new GetAllClientCategories().execute();
		GenericEntity<List<AllClientCategories>> ety = new GenericEntity<List<AllClientCategories>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getClientTypeDropdown/")
	public Response getClientTypeDropdown() {

		List<ClientTypeDropdown> rList = new GetClientTypeDropdown().execute();
		GenericEntity<List<ClientTypeDropdown>> ety = new GenericEntity<List<ClientTypeDropdown>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllClientDropdown/")
	public Response getAllClientDropdown() {

		List<AllClients> rList = new GetAllClientsDropdown().execute();
		GenericEntity<List<AllClients>> ety = new GenericEntity<List<AllClients>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllContacts/")
	public Response getAllContacts() {

		List<AllClients> rList = new GetAllContacts().execute();
		GenericEntity<List<AllClients>> ety = new GenericEntity<List<AllClients>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET

	@Path("/getAllProspects/")
	public Response getAllProspects() {

		List<AllClients> rList = new GetAllProspects().execute();
		GenericEntity<List<AllClients>> ety = new GenericEntity<List<AllClients>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllCustomers/")
	public Response getAllCustomers() {

		List<AllClients> rList = new GetAllCustomers().execute();
		GenericEntity<List<AllClients>> ety = new GenericEntity<List<AllClients>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllContactsByCategory/{customerCategory}")
	public Response getAllContactsByCategory(@PathParam("customerCategory") final int customerCategory) {

		List<AllClients> rList = new GetAllContactsByCategory(customerCategory).execute();
		GenericEntity<List<AllClients>> ety = new GenericEntity<List<AllClients>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllProspectsByCategory/{customerCategory}")
	public Response getAllProspectsByCategory(@PathParam("customerCategory") final int customerCategory) {

		List<AllClients> rList = new GetAllProsepctsByCategory(customerCategory).execute();
		GenericEntity<List<AllClients>> ety = new GenericEntity<List<AllClients>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllCustomersByCategory/{customerCategory}")
	public Response getAllCustomersByCategory(@PathParam("customerCategory") final int customerCategory) {

		List<AllClients> rList = new GetAllCustomersByCategory(customerCategory).execute();
		GenericEntity<List<AllClients>> ety = new GenericEntity<List<AllClients>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllSake/")
	public Response getAllSake() {

		List<Sake> rList = new GetAllSake().execute();
		GenericEntity<List<Sake>> ety = new GenericEntity<List<Sake>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllSakeByBrand/{brand}")
	public Response getAllSake(@PathParam("brand") final String brand) {

		List<Sake> rList = new GetAllSakeByBrand(brand).execute();
		GenericEntity<List<Sake>> ety = new GenericEntity<List<Sake>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllSakePerformanceBreakdown/")
	public Response getAllSakePerformanceBreakdown() {

		JodaDateHandler ds = new JodaDateHandler();
		String e1 = ds.todayDate();
		String s1 = ds.twoYearsAgo(e1);

		List<SakePerformance> rList = new GetAllSakePerformanceBreakdown().execute();
		GenericEntity<List<SakePerformance>> ety = new GenericEntity<List<SakePerformance>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getSakePerformanceBreakdownByClientCategory/")
	public Response getSakePerformanceBreakdownByClientCategory() {

		JodaDateHandler ds = new JodaDateHandler();
		String e1 = ds.todayDate();
		String s1 = ds.twoYearsAgo(e1);

		List<SakePerformance> rList = new GetSakePerformanceBreakdownByClientCategory().execute();
		GenericEntity<List<SakePerformance>> ety = new GenericEntity<List<SakePerformance>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllSakePerformanceBreakdownByBrand/{sbrand}")
	public Response getAllSakePerformanceBreakdownByBrand(@PathParam("sbrand") final String sbrand) {

		JodaDateHandler ds = new JodaDateHandler();
		String e1 = ds.todayDate();
		String s1 = ds.twoYearsAgo(e1);

		List<SakePerformance> rList = new GetAllSakePerformanceBreakdownByBrand(sbrand).execute();
		GenericEntity<List<SakePerformance>> ety = new GenericEntity<List<SakePerformance>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getSakePerformanceBreakdownByClientCategoryByBrand/{sbrand}")
	public Response getSakePerformanceBreakdownByClientCategoryByBrand(@PathParam("sbrand") final String sbrand) {

		JodaDateHandler ds = new JodaDateHandler();
		String e1 = ds.todayDate();
		String s1 = ds.twoYearsAgo(e1);

		List<SakePerformance> rList = new GetSakePerformanceBreakdownByClientCategoryByBrand(sbrand).execute();
		GenericEntity<List<SakePerformance>> ety = new GenericEntity<List<SakePerformance>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getSakeGradeDropdown/")
	public Response getSakeGrade() {

		List<SakeGradeDropdown> rList = new GetSakeGradeDropdown().execute();
		GenericEntity<List<SakeGradeDropdown>> ety = new GenericEntity<List<SakeGradeDropdown>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getSakeBrandDropdown/")
	public Response getSakeBrand() {

		List<SakeBrandDropdown> rList = new GetSakeBrandDropdown().execute();
		GenericEntity<List<SakeBrandDropdown>> ety = new GenericEntity<List<SakeBrandDropdown>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllAppointmentsWithInvoice/")
	public Response getAllAppointmentsWithInvoice() {

		List<AppointmentWithInvoice> rList = new GetAllAppointmentsWithInvoice().execute();
		GenericEntity<List<AppointmentWithInvoice>> ety = new GenericEntity<List<AppointmentWithInvoice>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllAppointmentsWithInvoiceByEmployeeAndCustomer/{eID}/{cID}")
	public Response getAllAppointmentsWithInvoiceByEmployeeAndCustomer(@PathParam("eID") final int eID,
			@PathParam("cID") final int cID) {

		List<AppointmentWithInvoice> rList = new GetAppointmentByEmployeeAndCustomerWithInvoice(eID, cID).execute();
		GenericEntity<List<AppointmentWithInvoice>> ety = new GenericEntity<List<AppointmentWithInvoice>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllInvoice/")
	public Response getAllInvoice() {

		List<AllInvoice> rList = new GetAllInvoice().execute();
		GenericEntity<List<AllInvoice>> ety = new GenericEntity<List<AllInvoice>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllOpenInvoice/")
	public Response getAllOpenInvoice() {

		List<AllInvoice> rList = new GetAllOpenInvoice().execute();
		GenericEntity<List<AllInvoice>> ety = new GenericEntity<List<AllInvoice>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllClosedInvoice/")
	public Response getAllClosedInvoice() {

		List<AllInvoice> rList = new GetAllClosedInvoice().execute();
		GenericEntity<List<AllInvoice>> ety = new GenericEntity<List<AllInvoice>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

@GET
	@Path("/getAllInvoiceDetails/{invoiceID}")
	public Response getAllInvoiceDetails(@PathParam("invoiceID") final int invoiceID) {

		List<InvoiceInformation> rList = new GetInvoiceInformation(invoiceID).execute();
		GenericEntity<List<InvoiceInformation>> ety = new GenericEntity<List<InvoiceInformation>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/validateLoginID/{loginID}")
	public Response validateLoginID(@PathParam("loginID") final String loginID) {

		List<LoginIDValidator> rList = new ValidateLoginID(loginID).execute();
		GenericEntity<List<LoginIDValidator>> ety = new GenericEntity<List<LoginIDValidator>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getAllSakeOfSpecificBrand/{cat}")
	public Response getAllSakeOfSpecificBrand(@PathParam("cat") final String cat) {

		List<Sake> rList = new GetAllSakeOfSpecificBrand(cat).execute();
		GenericEntity<List<Sake>> ety = new GenericEntity<List<Sake>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/overviewInvoiceByCategories/")
	public Response threeMonthsInvoiceByCategories() {

		JodaDateHandler ds = new JodaDateHandler();
		String e1 = ds.todayDate();
		String s1 = ds.twoYearsAgo(e1);

		List<OverviewInvoiceGraph> rList = new GetOverviewInvoiceGraph(s1, e1).execute();
		GenericEntity<List<OverviewInvoiceGraph>> ety = new GenericEntity<List<OverviewInvoiceGraph>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/employeeOverallPerformanceReport/{s}/{e}")
	public Response employeeOverallPerformanceReport(@PathParam("s") final String s, @PathParam("e") final String e) {

		List<EmployeeOverallPerformanceReport> rList = new GetEmployeeOverallPerformanceReport(s, e).execute();
		GenericEntity<List<EmployeeOverallPerformanceReport>> ety = new GenericEntity<List<EmployeeOverallPerformanceReport>>(
				rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/employeeIndividualPerformanceReport/{s}/{e}/{eid}")
	public Response employeeIndividualPerformanceReport(@PathParam("s") final String s, @PathParam("e") final String e,
			@PathParam("eid") final int eid) {

		List<EmployeeIndividualPerformanceReport> rList = new GetEmployeeIndividualPerformanceReport(s, e, eid)
				.execute();
		GenericEntity<List<EmployeeIndividualPerformanceReport>> ety = new GenericEntity<List<EmployeeIndividualPerformanceReport>>(
				rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/customersListOfAnEmployee/{s}/{e}/{eid}")
	public Response customersListOfAnEmployee(@PathParam("s") final String s, @PathParam("e") final String e,
			@PathParam("eid") final int eid) {

		List<CustomersListOfAnEmployee> rList = new GetCustomersListOfEmployee(s, e, eid).execute();
		GenericEntity<List<CustomersListOfAnEmployee>> ety = new GenericEntity<List<CustomersListOfAnEmployee>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/employeeIndividualInvoiceGraph/{eid}")
	public Response employeeIndividualInvoiceGraph(@PathParam("eid") final int eid) {

		JodaDateHandler ds = new JodaDateHandler();
		String e = ds.todayDate();
		String s = ds.twoYearsAgo(e);

		List<InvoiceGraph> rList = new GetInvoiceGraphForIndividualEmployee(eid, s, e).execute();
		GenericEntity<List<InvoiceGraph>> ety = new GenericEntity<List<InvoiceGraph>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/employeeNonInvoiceGraph/{eid}")
	public Response employeeNonInvoiceGraph(@PathParam("eid") final int eid) {

		JodaDateHandler ds = new JodaDateHandler();
		String e = ds.todayDate();
		String s = ds.twoYearsAgo(e);

		List<NonInvoiceGraph> rList = new GetAllNonInvoiceGraphForIndividualEmployee(eid, s, e).execute();
		GenericEntity<List<NonInvoiceGraph>> ety = new GenericEntity<List<NonInvoiceGraph>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/employeeIndividualAppointmentGraph/{eid}")
	public Response employeeIndividualAppointmentGraph(@PathParam("eid") final int eid) {

		JodaDateHandler ds = new JodaDateHandler();
		String e = ds.todayDate();
		String s = ds.twoYearsAgo(e);

		List<NonInvoiceGraph> rList = new GetAppointmentsGraphForIndividualEmployee(eid, s, e).execute();
		GenericEntity<List<NonInvoiceGraph>> ety = new GenericEntity<List<NonInvoiceGraph>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/employeeIndividualContactGraph/{eid}")
	public Response employeeIndividualContactGraph(@PathParam("eid") final int eid) {

		JodaDateHandler ds = new JodaDateHandler();
		String e = ds.todayDate();
		String s = ds.twoYearsAgo(e);

		List<NonInvoiceGraph> rList = new GetContactGraphForIndividualEmployee(eid, s, e).execute();
		GenericEntity<List<NonInvoiceGraph>> ety = new GenericEntity<List<NonInvoiceGraph>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/employeeIndividualProspectGraph/{eid}")
	public Response employeeIndividualProspectGraph(@PathParam("eid") final int eid) {

		JodaDateHandler ds = new JodaDateHandler();
		String e = ds.todayDate();
		String s = ds.twoYearsAgo(e);

		List<NonInvoiceGraph> rList = new GetProspectGraphForIndividualEmployee(eid, s, e).execute();
		GenericEntity<List<NonInvoiceGraph>> ety = new GenericEntity<List<NonInvoiceGraph>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/employeeIndividualCustomerGraph/{eid}")
	public Response employeeIndividualCustomerGraph(@PathParam("eid") final int eid) {

		JodaDateHandler ds = new JodaDateHandler();
		String e = ds.todayDate();
		String s = ds.twoYearsAgo(e);

		List<NonInvoiceGraph> rList = new GetCustomerGraphForIndividualEmployee(eid, s, e).execute();
		GenericEntity<List<NonInvoiceGraph>> ety = new GenericEntity<List<NonInvoiceGraph>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/customerOverallStatsRFMReport/{s}/{e}")
	public Response customerOverallStatsRFMReport(@PathParam("s") final String s, @PathParam("e") final String e) {

		List<CustomerOverallStatsRFMReport> rList = new GetCustomerOverallStatsRFMReport(s, e).execute();
		GenericEntity<List<CustomerOverallStatsRFMReport>> ety = new GenericEntity<List<CustomerOverallStatsRFMReport>>(
				rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/customerOverallStatsRFMReportByCategory/{s}/{e}/{cat}")
	public Response customerOverallStatsRFMReportByCategory(@PathParam("s") final String s,
			@PathParam("e") final String e, @PathParam("cat") final String cat) {

		List<CustomerOverallStatsRFMReport> rList = new GetCustomerOverallStatsRFMReportByCategory(s, e, cat).execute();
		GenericEntity<List<CustomerOverallStatsRFMReport>> ety = new GenericEntity<List<CustomerOverallStatsRFMReport>>(
				rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/customerIndividualRFM/{s}/{e}/{cID}")
	public Response customerIndividualRFM(@PathParam("s") final String s, @PathParam("e") final String e,
			@PathParam("cID") final int cID) {

		List<CustomerIndividualFRM> rList = new GetCustomerIndividualFRM(s, e, cID).execute();
		GenericEntity<List<CustomerIndividualFRM>> ety = new GenericEntity<List<CustomerIndividualFRM>>(rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/customerIndividualBestSellingSake/{s}/{e}/{cID}")
	public Response customerIndividualBestSellingSake(@PathParam("s") final String s, @PathParam("e") final String e,
			@PathParam("cID") final int cID) {

		List<CustomerIndividualBestSellingSake> rList = new GetCustomerIndividualBestSellingSake(s, e, cID).execute();
		GenericEntity<List<CustomerIndividualBestSellingSake>> ety = new GenericEntity<List<CustomerIndividualBestSellingSake>>(
				rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/customerIndividualShortlist/{cID}")
	public Response customerIndividualShortlist(@PathParam("cID") final int cID) {

		List<CustomerIndividualShortlist> rList = new GetCustomerIndividualShortlist(cID).execute();
		GenericEntity<List<CustomerIndividualShortlist>> ety = new GenericEntity<List<CustomerIndividualShortlist>>(
				rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getCustomerIndividualSixMonthInvoice/{s1}/{e1}/{s2}/{e2}/{s3}/{e3}/{s4}/{e4}/{s5}/{e5}/{s6}/{e6}/{cid}")
	public Response getCustomerIndividualSixMonthInvoice(@PathParam("s1") final String s1,
			@PathParam("e1") final String e1, @PathParam("s2") final String s2, @PathParam("e2") final String e2,
			@PathParam("s3") final String s3, @PathParam("e3") final String e3, @PathParam("s4") final String s4,
			@PathParam("e4") final String e4, @PathParam("s5") final String s5, @PathParam("e5") final String e5,
			@PathParam("s6") final String s6, @PathParam("e6") final String e6, @PathParam("cid") final int cid) {

		List<CustomerIndividualSixMonthInvoice> rList = new GetCustomerIndividualSixMonthInvoice(s1, e1, s2, e2, s3, e3,
				s4, e4, s5, e5, s6, e6, cid).execute();
		GenericEntity<List<CustomerIndividualSixMonthInvoice>> ety = new GenericEntity<List<CustomerIndividualSixMonthInvoice>>(
				rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getCustomerIndividualThreeMonthInvoice/{s1}/{e1}/{s2}/{e2}/{s3}/{e3}/{cid}")
	public Response getCustomerIndividualThreeMonthInvoice(@PathParam("s1") final String s1,
			@PathParam("e1") final String e1, @PathParam("s2") final String s2, @PathParam("e2") final String e2,
			@PathParam("s3") final String s3, @PathParam("e3") final String e3, @PathParam("cid") final int cid) {

		List<CustomerIndividualThreeMonthInvoice> rList = new GetCustomerIndividualthreeMonthInvoice(s1, e1, s2, e2, s3,
				e3, cid).execute();
		GenericEntity<List<CustomerIndividualThreeMonthInvoice>> ety = new GenericEntity<List<CustomerIndividualThreeMonthInvoice>>(
				rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getOverallCustomerThreeMonthInvoice/{s1}/{e1}/{s2}/{e2}/{s3}/{e3}")
	public Response getOverallCustomerThreeMonthInvoice(@PathParam("s1") final String s1,
			@PathParam("e1") final String e1, @PathParam("s2") final String s2, @PathParam("e2") final String e2,
			@PathParam("s3") final String s3, @PathParam("e3") final String e3) {

		List<OverallCustomerThreeMonthInvoice> rList = new GetOverallCustomerThreeMonthInvoice(s1, e1, s2, e2, s3, e3)
				.execute();
		GenericEntity<List<OverallCustomerThreeMonthInvoice>> ety = new GenericEntity<List<OverallCustomerThreeMonthInvoice>>(
				rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getOverallCustomerThreeMonthInvoiceByCategory/{s1}/{e1}/{s2}/{e2}/{s3}/{e3}/{cat}")
	public Response getOverallCustomerThreeMonthInvoiceByCategory(@PathParam("s1") final String s1,
			@PathParam("e1") final String e1, @PathParam("s2") final String s2, @PathParam("e2") final String e2,
			@PathParam("s3") final String s3, @PathParam("e3") final String e3, @PathParam("cat") final String cat) {

		List<OverallCustomerThreeMonthInvoice> rList = new GetOverallCustomerThreeMonthInvoiceByCategory(s1, e1, s2, e2,
				s3, e3, cat).execute();
		GenericEntity<List<OverallCustomerThreeMonthInvoice>> ety = new GenericEntity<List<OverallCustomerThreeMonthInvoice>>(
				rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getOverallCustomerSixMonthInvoice/{s1}/{e1}/{s2}/{e2}/{s3}/{e3}/{s4}/{e4}/{s5}/{e5}/{s6}/{e6}")
	public Response getOverallCustomerSixMonthInvoice(@PathParam("s1") final String s1,
			@PathParam("e1") final String e1, @PathParam("s2") final String s2, @PathParam("e2") final String e2,
			@PathParam("s3") final String s3, @PathParam("e3") final String e3, @PathParam("s4") final String s4,
			@PathParam("e4") final String e4, @PathParam("s5") final String s5, @PathParam("e5") final String e5,
			@PathParam("s6") final String s6, @PathParam("e6") final String e6) {

		List<OverallCustomerSixMonthInvoice> rList = new GetOverallCustomerSixMonthInvoice(s1, e1, s2, e2, s3, e3, s4,
				e4, s5, e5, s6, e6).execute();
		GenericEntity<List<OverallCustomerSixMonthInvoice>> ety = new GenericEntity<List<OverallCustomerSixMonthInvoice>>(
				rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@GET
	@Path("/getOverallCustomerSixMonthInvoiceByCategory/{s1}/{e1}/{s2}/{e2}/{s3}/{e3}/{s4}/{e4}/{s5}/{e5}/{s6}/{e6}/{cat}")
	public Response getOverallCustomerThreeMonthInvoiceByCategory(@PathParam("s1") final String s1,
			@PathParam("e1") final String e1, @PathParam("s2") final String s2, @PathParam("e2") final String e2,
			@PathParam("s3") final String s3, @PathParam("e3") final String e3, @PathParam("s4") final String s4,
			@PathParam("e4") final String e4, @PathParam("s5") final String s5, @PathParam("e5") final String e5,
			@PathParam("s6") final String s6, @PathParam("e6") final String e6, @PathParam("cat") final String cat) {

		List<OverallCustomerSixMonthInvoice> rList = new GetOverallCustomerSixMonthInvoiceByCategory(s1, e1, s2, e2, s3,
				e3, s4, e4, s5, e5, s6, e6, cat).execute();
		GenericEntity<List<OverallCustomerSixMonthInvoice>> ety = new GenericEntity<List<OverallCustomerSixMonthInvoice>>(
				rList) {

		};
		return Response.status(Status.OK).entity(ety).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@POST
	@Path("/createEmployee/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createEmployee(AllEmployees employee) {

		String loginID = employee.getLoginID();
		String password = employee.getPassword();
		String empContactNumber = employee.getEmpContactNumber();
		String empName = employee.getEmpName();
		String empDOB = employee.getEmpDOB();
		String empAddress = employee.getEmpAddress();
		String empDateJoined = employee.getEmpDateJoined();
		int empPosition = employee.getEmpPosition();
		String empEmail = employee.getEmpEmail();
		int empStatus = 1;

		AllEmployees e = new CreateEmployee(loginID, password, empContactNumber, empName, empDOB, empAddress,
				empDateJoined, empEmail, empPosition, empStatus).execute();
		
		CalendarSample cs = new CalendarSample();
		
		if (e != null) {
			cs.authorizeUser(empEmail);
		} else {
			Log.warn("Creation of appointment unsuccessful.");
		}

		return Response.status(Status.OK).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@POST
	@Path("/createEvent/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createEvent(Events e) {

		String eventName = e.getEventName();
		String eventStart = e.getEventStart();
		String eventDescription = e.getEventDescription();
		String eventLocation = e.getEventLocation();
		String eventEnd = e.getEventEnd();

		String resourceID = "";
		resourceID = eventStart + "" + eventEnd;
		resourceID = resourceID.toLowerCase();
		resourceID = resourceID.replaceAll(" ", "");
		resourceID = resourceID.replaceAll("-", "");
		resourceID = resourceID.replaceAll(":", "");

		String eventDateGoogleStart = eventStart.replace(' ', 'T');
		String eventDateGoogleEnd = eventEnd.replace(' ', 'T');

		DateTime dt = DateTime.parseRfc3339(eventDateGoogleStart + "+08:00");
		DateTime de = DateTime.parseRfc3339(eventDateGoogleEnd + "+08:00");

		EventDateTime start = new EventDateTime().setDateTime(dt).setTimeZone("Asia/Singapore");
		EventDateTime end = new EventDateTime().setDateTime(de).setTimeZone("Asia/Singapore");

		Events event = new CreateEvent(resourceID, eventName, eventStart, eventEnd, eventLocation, eventDescription)
				.execute();

		CalendarSample cs = new CalendarSample();
		if (event != null) {
			cs.createEvent(resourceID, eventName, start, end, eventDescription, eventLocation);
		} else {
			Log.warn("Creation of appointment unsuccessful.");
		}

		return Response.status(Status.OK).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/createContact/")
	public Response createContact(AllClients ac) {

		int empID = 0;
		String clientName = ac.getClientName();
		String parentCompany = "";
		if (ac.getParentCompany() != null) {
			parentCompany = ac.getParentCompany();
		}
		String clientContactNumber = ac.getClientContactNumber();

		String clientEmail = ac.getClientEmail();
		String clientType = "Contact";
		int clientCategoryID = ac.getClientCategoryID();
		String clientBillingAddress = "";
		if (ac.getClientBillingAddress() != null) {
			clientBillingAddress = ac.getClientBillingAddress();
		}
		String clientShippingAddress = "";
		if (ac.getClientShippingAddress() != null) {
			clientShippingAddress = ac.getClientShippingAddress();
		}
		String POC1Name = ac.getPOC1Name();
		String POC1ContactNumber = ac.getPOC1ContactNumber();
		String POC1Email = ac.getPOC1Email();
		String POC2Name = "";
		if (ac.getPOC2Name() != null) {
			POC2Name = ac.getPOC2Name();
		}
		String POC2ContactNumber = "";
		if (ac.getPOC2ContactNumber() != null) {
			POC2ContactNumber = ac.getPOC2ContactNumber();
		}
		String POC2Email = "";
		if (ac.getPOC2Email() != null) {
			POC2Email = ac.getPOC2Email();
		}
		String clientDateAdded = ac.getClientDateAdded();

		AllClients c = new CreateContact(empID, clientName, parentCompany, clientContactNumber, clientEmail, clientType,
				clientCategoryID, clientBillingAddress, clientShippingAddress, POC1Name, POC1ContactNumber, POC1Email,
				POC2Name, POC2ContactNumber, POC2Email, clientDateAdded).execute();

		return Response.status(Status.OK).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/createSake/")
	public Response createSake(Sake sake) {

		String sakeBrand = sake.getSakeBrand();
		if (sakeBrand == null) {
			sakeBrand = "";
		}
		String sakeName = sake.getSakeName();
		String sakeGrade = sake.getSakeGrade();
		if (sakeGrade == null) {
			sakeGrade = "";
		}

		String sakeDescription = sake.getSakeDescription();
		if (sakeDescription == null) {
			sakeDescription = "";
		}
		String skuNumber = sake.getSkuNumber(); // cannot be null
		double sakeRetailPrice = sake.getSakeRetailPrice();
		if (sakeRetailPrice == 0.0) {
			sakeRetailPrice = 0.0;
		}
		double sakeTradePrice = sake.getSakeTradePrice();
		if (sakeTradePrice == 0.0) {
			sakeTradePrice = 0.0;
		}
		double sakeWholesalePrice = sake.getSakeWholesalePrice();
		if (sakeWholesalePrice == 0.0) {
			sakeWholesalePrice = 0.0;
		}
		int sakeCapacity = sake.getSakeCapacity();
		if (sakeCapacity == 0) {
			sakeCapacity = 0;
		}
		int sakePolish = sake.getSakePolish();
		if (sakePolish == 0) {
			sakePolish = 0;
		}
		String sakeRice = sake.getSakeRice();
		if (sakeRice == null) {
			sakeRice = "";
		}
		int sakeAcidity = sake.getSakeAcidity();
		if (sakeAcidity == 0) {
			sakeAcidity = 0;
		}
		double sakeAlcoholContent = sake.getSakeAlcoholContent();
		if (sakeAlcoholContent == 0.0) {
			sakeAlcoholContent = 0.0;
		}
		int sakeSMV = sake.getSakeSMV();
		if (sakeSMV == 0) {
			sakeSMV = 0;
		}
		String dataURI = sake.getDataURI();
		if (dataURI == null) {
			dataURI = "";
		}

		Sake newSake = new CreateSake(sakeBrand, sakeName, sakeGrade, sakeDescription, skuNumber, sakeRetailPrice,
				sakeTradePrice, sakeWholesalePrice, sakeCapacity, sakePolish, sakeRice, sakeAcidity, sakeAlcoholContent,
				sakeSMV, dataURI).execute();

		return Response.status(Status.OK).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	// * @POST
	// *
	// * @Consumes(MediaType.APPLICATION_JSON)
	// *
	// * @Path("/createSake/") public Response createSake(Sake sake) {
	// *
	// * String skuNumber = sake.getSkuNumber(); String sakeDescription =
	// * sake.getSakeDescription(); String sakePhoto = sake.getSakePhoto();
	// String
	// * sakeBrand = sake.getSakeBrand(); String sakeName = sake.getSakeName();
	// * double retailPrice = sake.getRetailPrice(); double wholesalePrice =
	// * sake.getWholesalePrice(); double tradePrice = sake.getTradePrice(); int
	// * grade = sake.getGrade();
	// *
	// * Sake s = new CreateSake(skuNumber, sakeDescription, sakePhoto,
	// sakeBrand,
	// * sakeName, retailPrice, wholesalePrice, tradePrice, grade).execute();
	// *
	// * return Response.status(Status.OK).header("Access-Control-Allow-Origin",
	// * "*") .header("Access-Control-Allow-Methods",
	// * "GET, POST, DELETE, PUT").allow("OPTIONS").build();
	// *
	// * }

	@POST

	@Consumes(MediaType.APPLICATION_JSON)

	@Path("/createAppointment/")
	public Response createAppointment(Appointment appointment) throws Exception {
		// String characters = "abcdefghijklmnopqrstuvwxyz0123456789";
		String resourceID = "";
		// String appointmentID = RandomStringUtils.random(8, characters);
		int empID = appointment.getEmpID();
		int clientID = appointment.getClientID();
		String appointmentLocation = appointment.getAppointmentLocation();
		String appointmentDate = appointment.getAppointmentDate();
		// System.out.println("clientID: "+ clientID);
		// System.out.println("empID: "+ empID);
		// System.out.println("appointmentLocation: "+ appointmentLocation);
		// System.out.println("appointmentDate: "+ appointmentDate);
		resourceID = empID + "" + clientID + appointmentDate;
		resourceID = resourceID.toLowerCase();
		resourceID = resourceID.replaceAll(" ", "");
		resourceID = resourceID.replaceAll("-", "");
		resourceID = resourceID.replaceAll(":", "");

		// System.out.println(resourceID);

		String notes = appointment.getAppointmentNotes();
		int appointmentStatusID = 1;
		String empName = appointment.getEmpName();
		String clientName = appointment.getClientName();

		String appointmentDateGoogle = appointmentDate.replace(' ', 'T');

		DateTime dt = DateTime.parseRfc3339(appointmentDateGoogle + "+08:00");

		String description = "Meeting with " + empName + " .";
		String summary = "Appointment by: " + clientName;
		EventDateTime start = new EventDateTime().setDateTime(dt).setTimeZone("Asia/Singapore");

		long dteValue = dt.getValue();
		dteValue = dteValue + 3600000;
		DateTime dte = new DateTime(dteValue);
		EventDateTime end = new EventDateTime().setDateTime(dte).setTimeZone("Asia/Singapore");

		CalendarSample cs = new CalendarSample();
		try {
			Appointment a = new CreateAppointment(resourceID, empID, clientID, appointmentLocation, appointmentDate,
					notes, appointmentStatusID).execute();

			cs.createAppointment("primary", start, end, description, appointmentLocation, summary, resourceID);

		} catch (Sql2oException e) {
//			System.out.println("Error caught and thrown");
			throw new Exception("Duplicated Entry for salesID " + empID + " with clientID " + clientID +" on "+ appointmentDate);
//			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("General Exception!");

			// System.out.println("Error caught and thrown");
			throw new Exception(
					"Duplicated Entry for salesID " + empID + " with clientID " + clientID + " on " + appointmentDate);
		}

		return Response.status(Status.OK).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@PUT
	@Path("/updateEmployee/")
	public Response updateEmployee(AllEmployees employee) {
		String loginID = employee.getLoginID();
		String password = employee.getPassword();
		String empContactNumber = employee.getEmpContactNumber();
		String empName = employee.getEmpName();
		String empDOB = employee.getEmpDOB();
		String empAddress = employee.getEmpAddress();
		String empDateJoined = employee.getEmpDateJoined();
		int empPosition = employee.getEmpPosition();
		String empEmail = employee.getEmpEmail();
		int empStatus = employee.getEmpStatus();
		int empID = employee.getEmpID();

		AllEmployees e = new UpdateEmployee(loginID, password, empContactNumber, empName, empDOB, empAddress,
				empDateJoined, empEmail, empPosition, empStatus, empID).execute();

		return Response.status(Status.OK).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@PUT
	@Path("/updateClient/")
	public Response updateClient(AllClients ac) {
		int empID = ac.getEmpID();
		int clientID = ac.getClientID();
		String clientName = ac.getClientName();
		String parentCompany = ac.getParentCompany();
		String clientContactNumber = ac.getClientContactNumber();
		String clientEmail = ac.getClientEmail();
		int clientCategoryID = ac.getClientCategoryID();
		String POC1Name = ac.getPOC1Name();
		String POC1ContactNumber = ac.getPOC1ContactNumber();
		String POC1Email = ac.getPOC1Email();
		String POC2Name = ac.getPOC2Name();
		String POC2ContactNumber = ac.getPOC2ContactNumber();
		String POC2Email = ac.getPOC2Email();
		String clientShippingAddress = ac.getClientShippingAddress();
		String clientBillingAddress = ac.getClientBillingAddress();

		AllClients a = new UpdateClientDetails(clientID, empID, clientName, parentCompany, clientContactNumber,
				clientEmail, clientCategoryID, clientBillingAddress, clientShippingAddress, POC1Name, POC1ContactNumber,
				POC1Email, POC2Name, POC2ContactNumber, POC2Email).execute();

		return Response.status(Status.OK).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@PUT
	@Path("/updateEvent/")
	public Response updateEvent(Events e) {
		String eventID = e.getEventID();
		String eventName = e.getEventName();
		String eventLocation = e.getEventLocation();
		String eventStart = e.getEventStart();
		String eventEnd = e.getEventEnd();
		String eventDescription = e.getEventDescription();

		Events event = new UpdateEvent(eventID, eventName, eventStart, eventEnd, eventLocation, eventDescription)
				.execute();

		return Response.status(Status.OK).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@PUT

	@Path("/updateSake/")
	public Response updateSake(Sake sake) {

		int sakeID = sake.getSakeID();
		String sakeBrand = sake.getSakeBrand();
		String sakeName = sake.getSakeName();
		String sakeGrade = sake.getSakeGrade();
		String sakeDescription = sake.getSakeDescription();
		String skuNumber = sake.getSkuNumber();
		double sakeRetailPrice = sake.getSakeRetailPrice();
		double sakeTradePrice = sake.getSakeTradePrice();
		double sakeWholesalePrice = sake.getSakeWholesalePrice();
		int sakeCapacity = sake.getSakeCapacity();
		int sakePolish = sake.getSakePolish();
		String sakeRice = sake.getSakeRice();
		int sakeAcidity = sake.getSakeAcidity();
		double sakeAlcoholContent = sake.getSakeAlcoholContent();
		int sakeSMV = sake.getSakeSMV();
		String dataURI = sake.getDataURI();

		Sake s = new UpdateSake(sakeID, sakeBrand, sakeName, sakeGrade, sakeDescription, skuNumber, sakeRetailPrice,
				sakeTradePrice, sakeWholesalePrice, sakeCapacity, sakePolish, sakeRice, sakeAcidity, sakeAlcoholContent,
				sakeSMV, dataURI).execute();

		return Response.status(Status.OK).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@PUT
	@Path("/systemResetPassword/")
	public Response systemResetPassword(PasswordReset pr) {

		String loginID = pr.getLoginID();
		String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		String password = RandomStringUtils.random(15, characters);
		String employeeEmail = pr.getEmployeeEmail();
		EmailHandler EmailHandler = new EmailHandler();
		EmailHandler.sendEmail(employeeEmail, password);
		PasswordReset e = new ResetPassword(loginID, password, employeeEmail).execute();

		return Response.status(Status.OK).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

	@PUT
	@Path("/changePassword/")
	public Response changePassword(PasswordReset pr) {

		String password = pr.getPassword();
		int empID = pr.getEmpID();
		PasswordReset e = new ChangePassword(empID, password).execute();

		return Response.status(Status.OK).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").allow("OPTIONS").build();

	}

}
