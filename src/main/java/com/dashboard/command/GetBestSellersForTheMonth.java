package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.BestSeller;

public class GetBestSellersForTheMonth extends Command2<List<BestSeller>> {
	String s;
	String e;

	public GetBestSellersForTheMonth(String startDate, String endDate) {
		super(null);
		this.s = startDate;
		this.e = endDate;
	}

	@Override
	public List<BestSeller> run(Connection conn) {
		String query = "call OverviewGetBestSellers(:s, :e)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e", this.e)
				.executeAndFetch(BestSeller.class);
		return result;
	}

}
