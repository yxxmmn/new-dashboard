package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllEmployees;

public class GetAllInactiveEmployees extends Command2<List<AllEmployees>> {

	public GetAllInactiveEmployees() {
		super(null);

	}

	@Override
	public List<AllEmployees> run(Connection conn) {
		String query = "call EmpGetAllInactiveEmployees()";
		result = conn.createQuery(query).executeAndFetch(AllEmployees.class);
		return result;
	}


}
