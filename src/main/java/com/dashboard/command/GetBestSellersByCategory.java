package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.BestSeller;

public class GetBestSellersByCategory extends Command2<List<BestSeller>> {
	String s;
	String e;
	String cat;

	public GetBestSellersByCategory(String startDate, String endDate, String cat) {
		super(null);
		this.s = startDate;
		this.e = endDate;
		this.cat = cat;
	}

	@Override
	public List<BestSeller> run(Connection conn) {
		String query = "call getBestSellerByCustomerCategory(:s, :e, :cat)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e", this.e)
				.addParameter("cat", this.cat).executeAndFetch(BestSeller.class);
		return result;
	}

}
