package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllClientCategories;

public class GetAllClientCategories extends Command2<List<AllClientCategories>> {

	public GetAllClientCategories() {
		super(null);

	}

	@Override
	public List<AllClientCategories> run(Connection conn) {
		String query = "call MiscGetAllClientCategories()";
		result = conn.createQuery(query).executeAndFetch(AllClientCategories.class);
		return result;
	}

}
