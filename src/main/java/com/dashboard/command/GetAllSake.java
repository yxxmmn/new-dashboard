package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.Sake;

public class GetAllSake extends Command2<List<Sake>> {

	public GetAllSake() {
		super(null);

	}

	@Override
	public List<Sake> run(Connection conn) {
		String query = "call SakeGetAllSake()";
		result = conn.createQuery(query).executeAndFetch(Sake.class);
		return result;
	}

}
