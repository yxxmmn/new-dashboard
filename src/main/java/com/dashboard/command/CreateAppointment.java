package com.dashboard.command;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.Appointment;

public class CreateAppointment extends Command2<Appointment> {

	String appointmentID;
	int empID;
	int clientID;
	String appointmentLocation;
	String appointmentDate;
	String appointmentNotes;
	int appointmentStatusID;

	public CreateAppointment(String appointmentID, int empID, int clientID, String appointmentLocation,
			String appointmentDate, String appointmentNotes, int appointmentStatusID) {
		super(null);
		this.appointmentID = appointmentID;
		this.empID = empID;
		this.clientID = clientID;
		this.appointmentLocation = appointmentLocation;
		this.appointmentDate = appointmentDate;
		this.appointmentNotes = appointmentNotes;
		this.appointmentStatusID = appointmentStatusID;
	}

	public Appointment run(Connection conn) {
		String query = "call AppointmentAddAppointment(:appointmentID, :empID, :clientID, :appointmentDate, "
				+ ":appointmentNotes, :appointmentLocation, :appointmentStatusID)";
		Appointment a = new Appointment(appointmentID, empID, clientID, appointmentDate, appointmentNotes,
				appointmentLocation, appointmentStatusID);
		conn.createQuery(query).addParameter("appointmentID", this.appointmentID).addParameter("empID", this.empID)
				.addParameter("clientID", this.clientID).addParameter("appointmentDate", this.appointmentDate)
				.addParameter("appointmentNotes", this.appointmentNotes)
				.addParameter("appointmentLocation", this.appointmentLocation)
				.addParameter("appointmentStatusID", this.appointmentStatusID).executeUpdate();
		return a;
	}
}
