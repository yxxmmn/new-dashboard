package com.dashboard.command;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllProspects;

public class UpdateProspect extends Command2<AllProspects> {
	int customerID;
	int empID;
	String name;
	String customerCategory;
	String billingAddress;
	String shippingAddress;
	String customerEmail;
	int contactnumber;

	public UpdateProspect(int customerID, int empID, String contactName, String customerCategory, String billingAddress,
			String shippingAddress, String customerEmail, int contactnumber) {
		super(null);
		this.customerID = customerID;
		this.empID = empID;
		this.name = contactName;
		this.customerCategory = customerCategory;
		this.billingAddress = billingAddress;
		this.shippingAddress = shippingAddress;
		this.customerEmail = customerCategory;
		this.contactnumber = contactnumber;
	}

	@Override
	public AllProspects run(Connection conn) {
		String query = "call updateProspect(mCustomerID, :mEmpID, :mContactName, :mCustomerCategory, :mBillingAddress, :mShippingAddress, :mCustomerEmail, :mContactnumber)";
		AllProspects c = new AllProspects(customerID, empID, name, customerCategory, billingAddress,
				shippingAddress, customerEmail, contactnumber);
		
		conn.createQuery(query).addParameter("mCustomerID", this.customerID).addParameter("mEmpID", this.empID)
				.addParameter("mContactName", this.name).addParameter("mCustomerCategory", this.customerCategory)
				.addParameter("mBillingAddress", this.billingAddress)
				.addParameter("mShippingAddress", this.shippingAddress)
				.addParameter("mCustomerEmail", this.customerEmail).addParameter("mContactnumber", this.contactnumber)
				.executeUpdate();
		return c;
	}

}
