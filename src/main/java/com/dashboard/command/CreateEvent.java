package com.dashboard.command;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.Events;

public class CreateEvent extends Command2<Events> {

	String eventID;
	String eventName;
	String eventLocation;
	String eventStart;
	String eventDescription;
	String eventEnd;

	public CreateEvent(String eventID, String eventName, String eventStart, String eventEnd, String eventLocation,
			String eventDescription) {

		super(null);
		this.eventID = eventID;
		this.eventName = eventName;
		this.eventStart = eventStart;
		this.eventEnd = eventEnd;
		this.eventLocation = eventLocation;
		this.eventDescription = eventDescription;

	}

	public Events run(Connection conn) {
		String query = "call CalendarCreateEvent(:eventID, :eventName,:eventStart, :eventEnd, :eventLocation,"
				+ ":eventDescription)";
		Events s = new Events(eventID, eventName, eventStart, eventEnd, eventLocation, eventDescription);
		conn.createQuery(query)
		.addParameter("eventID", this.eventID)
		.addParameter("eventName", this.eventName)
		.addParameter("eventStart", this.eventStart)
				.addParameter("eventEnd", this.eventEnd).addParameter("eventLocation", this.eventLocation)
				.addParameter("eventDescription", this.eventDescription).executeUpdate();
		return s;
	}

}
