package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.SakePerformance;

public class GetAllSakePerformanceBreakdown extends Command2<List<SakePerformance>> {

	public GetAllSakePerformanceBreakdown() {
		super(null);

	}

	@Override
	public List<SakePerformance> run(Connection conn) {
		String query = "call SakeGetSakePerformanceBreakdown()";
		result = conn.createQuery(query).executeAndFetch(SakePerformance.class);
		return result;
	}

}
