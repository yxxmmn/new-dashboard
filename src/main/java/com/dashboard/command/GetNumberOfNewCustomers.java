package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.NumberOfNewCustomers;

public class GetNumberOfNewCustomers extends Command2<List<NumberOfNewCustomers>> {
	String s;
	String e;

	public GetNumberOfNewCustomers(String startDate, String endDate) {
		super(null);
		this.s = startDate;
		this.e = endDate;
	}

	@Override
	public List<NumberOfNewCustomers> run(Connection conn) {
		String query = "call getNumberOfNewCustomers(:s, :e)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e", this.e)
				.executeAndFetch(NumberOfNewCustomers.class);
		return result;
	}

}
