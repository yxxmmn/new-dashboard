package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllInvoice;

public class GetAllInvoice extends Command2<List<AllInvoice>> {

	public GetAllInvoice() {
		super(null);

	}

	@Override
	public List<AllInvoice> run(Connection conn) {
		String query = "call BillingGetAllInvoice()";
		result = conn.createQuery(query).executeAndFetch(AllInvoice.class);
		return result;
	}

}
