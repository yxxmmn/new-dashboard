package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllCustomerCategories;

public class GetAllCustomerCategories extends Command2<List<AllCustomerCategories>> {

	public GetAllCustomerCategories() {
		super(null);

	}

	@Override
	public List<AllCustomerCategories> run(Connection conn) {
		String query = "call getAllCustomerCategory()";
		result = conn.createQuery(query).executeAndFetch(AllCustomerCategories.class);
		return result;
	}
}
