package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.ListOfPurchase;

public class GetListOfPurchase extends Command2<List<ListOfPurchase>> {
	int invoiceID;

	public GetListOfPurchase(int invoiceID) {
		super(null);
		this.invoiceID = invoiceID;
	}

	@Override
	public List<ListOfPurchase> run(Connection conn) {
		String query = "call AppointmentGetListOfPurchase(:invoiceID)";
		result = conn.createQuery(query).addParameter("invoiceID", this.invoiceID)
				.executeAndFetch(ListOfPurchase.class);
		return result;
	}

}
