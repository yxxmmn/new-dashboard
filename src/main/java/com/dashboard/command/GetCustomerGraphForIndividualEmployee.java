package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.NonInvoiceGraph;

public class GetCustomerGraphForIndividualEmployee extends Command2<List<NonInvoiceGraph>> {
	int empID;
	String s;
	String e;

	public GetCustomerGraphForIndividualEmployee(int empID, String s, String e) {
		super(null);
		this.empID = empID;
		this.s = s;
		this.e = e;
	}

	@Override
	public List<NonInvoiceGraph> run(Connection conn) {
		String query = "call EmpGetIndividualEmployeeCustomerGraph(:eid,:s,:e)";
		result = conn.createQuery(query).addParameter("eid", this.empID).addParameter("s", this.s)
				.addParameter("e", this.e).executeAndFetch(NonInvoiceGraph.class);
		return result;
	}

}
