package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllEmployees;

public class GetEmployeeByID extends Command2<List<AllEmployees>> {
	int empID;

	public GetEmployeeByID(int empID) {
		super(null);
		this.empID = empID;
	}

	@Override
	public List<AllEmployees> run(Connection conn) {
		String query = "call EmpGetEmployeeByID(:empID)";
		result = conn.createQuery(query).addParameter("empID", this.empID).executeAndFetch(AllEmployees.class);
		return result;
	}

}
