package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.TopXEmployee;

public class GetTopXEmployee extends Command2<List<TopXEmployee>> {
	String s;
	String e;

	public GetTopXEmployee(String startDate, String endDate) {
		super(null);
		this.s = startDate;
		this.e = endDate;

	}

	@Override
	public List<TopXEmployee> run(Connection conn) {
		String query = "call OverviewGetTopSalesperson(:s, :e)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e", this.e)
				.executeAndFetch(TopXEmployee.class);
		return result;
	}
}
