package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.Events;

public class GetEventByID extends Command2<List<Events>> {
	String eventID;

	public GetEventByID(String eventID) {
		super(null);
		this.eventID = eventID;
	}

	@Override
	public List<Events> run(Connection conn) {
		String query = "call CalendarGetEventByID(:eventID)";
		result = conn.createQuery(query).addParameter("eventID", this.eventID).executeAndFetch(Events.class);
		return result;
	}

}
