package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.ClientTypeDropdown;

public class GetClientTypeDropdown extends Command2<List<ClientTypeDropdown>> {

	public GetClientTypeDropdown() {
		super(null);

	}

	@Override
	public List<ClientTypeDropdown> run(Connection conn) {
		String query = "call ClientGetDropdownListForClientType()";
		result = conn.createQuery(query).executeAndFetch(ClientTypeDropdown.class);
		return result;
	}

}
