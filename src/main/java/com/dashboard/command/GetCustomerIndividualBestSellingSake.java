package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.CustomerIndividualBestSellingSake;

public class GetCustomerIndividualBestSellingSake extends Command2<List<CustomerIndividualBestSellingSake>> {
	String s;
	String e;
	int cID;

	public GetCustomerIndividualBestSellingSake(String startDate, String endDate, int cID) {
		super(null);
		this.s = startDate;
		this.e = endDate;
		this.cID = cID;
	}

	@Override
	public List<CustomerIndividualBestSellingSake> run(Connection conn) {
		String query = "call getCustomerIndividualBestSellingSake(:s, :e, :cID)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e", this.e)
				.addParameter("cID", this.cID).executeAndFetch(CustomerIndividualBestSellingSake.class);
		return result;
	}

}
