package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.EmployeeIndividualPerformanceReport;

public class GetEmployeeIndividualPerformanceReport extends Command2<List<EmployeeIndividualPerformanceReport>> {
	String s;
	String e;
	int eid;

	public GetEmployeeIndividualPerformanceReport(String startDate, String endDate, int eid) {
		super(null);
		this.s = startDate;
		this.e = endDate;
		this.eid = eid;
	}

	@Override
	public List<EmployeeIndividualPerformanceReport> run(Connection conn) {
		String query = "call EmpGetIndividualEmployeeReport(:s, :e, :eid)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e", this.e).addParameter("eid", this.eid)
				.executeAndFetch(EmployeeIndividualPerformanceReport.class);
		return result;
	}

}
