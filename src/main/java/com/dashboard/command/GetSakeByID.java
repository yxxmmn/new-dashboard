package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.Sake;

public class GetSakeByID extends Command2<List<Sake>> {
	int sakeID;

	public GetSakeByID(int sakeID) {
		super(null);
		this.sakeID = sakeID;

	}

	@Override
	public List<Sake> run(Connection conn) {
		String query = "call SakeGetSakeByID(:sakeID)";
		result = conn.createQuery(query).addParameter("sakeID", this.sakeID).executeAndFetch(Sake.class);
		return result;
	}

}
