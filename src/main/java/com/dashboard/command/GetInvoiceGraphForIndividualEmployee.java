package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.InvoiceGraph;

public class GetInvoiceGraphForIndividualEmployee extends Command2<List<InvoiceGraph>> {
	int empID;
	String s;
	String e;

	public GetInvoiceGraphForIndividualEmployee(int empID, String s, String e) {
		super(null);
		this.empID = empID;
		this.s = s;
		this.e = e;
	}

	@Override
	public List<InvoiceGraph> run(Connection conn) {
		String query = "call EmpGetIndividualEmployeeInvoiceGraph(:eid,:s,:e)";
		result = conn.createQuery(query).addParameter("eid", this.empID).addParameter("s", this.s)
				.addParameter("e", this.e).executeAndFetch(InvoiceGraph.class);
		return result;
	}

}
