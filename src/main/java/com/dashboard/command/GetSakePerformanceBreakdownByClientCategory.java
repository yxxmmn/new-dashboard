package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.SakePerformance;

public class GetSakePerformanceBreakdownByClientCategory extends Command2<List<SakePerformance>> {

	public GetSakePerformanceBreakdownByClientCategory() {
		super(null);

	}

	@Override
	public List<SakePerformance> run(Connection conn) {
		String query = "call SakeGetSakePerformanceBreakdownByClientCategory()";
		result = conn.createQuery(query).executeAndFetch(SakePerformance.class);
		return result;
	}

}
