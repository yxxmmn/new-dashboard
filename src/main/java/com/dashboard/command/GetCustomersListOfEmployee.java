package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.CustomersListOfAnEmployee;

public class GetCustomersListOfEmployee extends Command2<List<CustomersListOfAnEmployee>> {
	String s;
	String e;
	int eid;

	public GetCustomersListOfEmployee(String startDate, String endDate, int eid) {
		super(null);
		this.s = startDate;
		this.e = endDate;
		this.eid = eid;
	}

	@Override
	public List<CustomersListOfAnEmployee> run(Connection conn) {
		String query = "call EmpGetIndividualEmployeeListOfCustomers(:s, :e, :eid)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e", this.e).addParameter("eid", this.eid)
				.executeAndFetch(CustomersListOfAnEmployee.class);
		return result;
	}


}
