package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AppointmentWithInvoice;

public class GetAppointmentByEmployeeAndCustomerWithInvoice extends Command2<List<AppointmentWithInvoice>> {

	int eID;
	int cID;

	public GetAppointmentByEmployeeAndCustomerWithInvoice(int eID, int cID) {
		super(null);
		this.eID = eID;
		this.cID = cID;

	}

	@Override
	public List<AppointmentWithInvoice> run(Connection conn) {
		String query = "call getAppointmentByEmployeeAndCustomer(:cID, :eID)";
		result = conn.createQuery(query).addParameter("cID", this.cID).addParameter("eID", this.eID)
				.executeAndFetch(AppointmentWithInvoice.class);
		return result;
	}
}
