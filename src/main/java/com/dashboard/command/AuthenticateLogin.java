package com.dashboard.command;

import java.sql.SQLException;
import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.EmployeeID;

public class AuthenticateLogin extends Command2<List<EmployeeID>> {

	String loginID;
	String password;

	public AuthenticateLogin(String loginID, String password) {
		super(null);
		// TODO Auto-generated constructor stub
		this.loginID = loginID;
		this.password = password;
	}

	@Override
	public List<EmployeeID> run(Connection conn) {
		String query = "call LoginAuthentication(:loginID, :password)";
		result = conn.createQuery(query).addParameter("loginID", this.loginID).addParameter("password", this.password)
				.executeAndFetch(EmployeeID.class);
		return result;
	}

}
