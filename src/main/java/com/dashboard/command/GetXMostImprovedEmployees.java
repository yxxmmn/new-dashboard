package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.MostImprovedEmployee;

public class GetXMostImprovedEmployees extends Command2<List<MostImprovedEmployee>> {

	String s1;
	String e1;
	String s2;
	String e2;

	public GetXMostImprovedEmployees(String startDate1, String endDate1, String startDate2, String endDate2) {
		super(null);
		this.s1 = startDate1;
		this.e1 = endDate1;
		this.s2 = startDate2;
		this.e2 = endDate2;
	}

	@Override
	public List<MostImprovedEmployee> run(Connection conn) {
		String query = "call OverviewGetMostImprovedSalesperson(:s1, :e1, :s2, :e2)";
		result = conn.createQuery(query).addParameter("s1", this.s1).addParameter("e1", this.e1)
				.addParameter("s2", this.s2).addParameter("e2", this.e2).executeAndFetch(MostImprovedEmployee.class);
		return result;
	}
}
