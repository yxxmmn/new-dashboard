package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.InvoiceInformation;

public class GetInvoiceInformation extends Command2<List<InvoiceInformation>> {
	int invoiceID;

	public GetInvoiceInformation(int invoiceID) {
		super(null);
		this.invoiceID = invoiceID;
	}

	@Override
	public List<InvoiceInformation> run(Connection conn) {
		String query = "call BillingGetInvoiceInformation(:invoiceID)";
		result = conn.createQuery(query).addParameter("invoiceID", this.invoiceID)
				.executeAndFetch(InvoiceInformation.class);
		return result;
	}

}
