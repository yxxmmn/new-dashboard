package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.CustomerIndividualFRM;

public class GetCustomerIndividualFRM extends Command2<List<CustomerIndividualFRM>> {
	String s;
	String e;
	int cID;

	public GetCustomerIndividualFRM(String startDate, String endDate, int cID) {
		super(null);
		this.s = startDate;
		this.e = endDate;
		this.cID = cID;
	}

	@Override
	public List<CustomerIndividualFRM> run(Connection conn) {
		String query = "call getCustomerIndividualRFM(:s, :e, :cID)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e", this.e)
				.addParameter("cID", this.cID).executeAndFetch(CustomerIndividualFRM.class);
		return result;
	}

}
