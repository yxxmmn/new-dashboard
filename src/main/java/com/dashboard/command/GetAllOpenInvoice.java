package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllInvoice;

public class GetAllOpenInvoice extends Command2<List<AllInvoice>> {

	public GetAllOpenInvoice() {
		super(null);

	}

	@Override
	public List<AllInvoice> run(Connection conn) {
		String query = "call BillingGetAllOpenInvoice()";
		result = conn.createQuery(query).executeAndFetch(AllInvoice.class);
		return result;
	}

}
