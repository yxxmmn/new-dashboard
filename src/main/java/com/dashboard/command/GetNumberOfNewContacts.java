package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.NumberOfNewContacts;

public class GetNumberOfNewContacts extends Command2<List<NumberOfNewContacts>> {
	String s;
	String e;

	public GetNumberOfNewContacts(String startDate, String endDate) {
		super(null);
		this.s = startDate;
		this.e = endDate;
	}

	@Override
	public List<NumberOfNewContacts> run(Connection conn) {
		String query = "call getNumberOfNewContacts(:s, :e)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e", this.e)
				.executeAndFetch(NumberOfNewContacts.class);
		return result;
	}
}
