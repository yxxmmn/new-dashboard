package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllClients;

public class GetClientByID extends Command2<List<AllClients>> {
	int customerID;

	public GetClientByID(int customerID) {
		super(null);
		this.customerID = customerID;
	}

	@Override
	public List<AllClients> run(Connection conn) {
		String query = "call ClientGetIndividualClient(:cID)";
		result = conn.createQuery(query).addParameter("cID", customerID).executeAndFetch(AllClients.class);
		return result;
	}

}
