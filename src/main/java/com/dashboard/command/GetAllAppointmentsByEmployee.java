package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllAppointments;

public class GetAllAppointmentsByEmployee extends Command2<List<AllAppointments>> {
	int empID;

	public GetAllAppointmentsByEmployee(int empID) {
		super(null);
		this.empID = empID;

	}

	@Override
	public List<AllAppointments> run(Connection conn) {
		String query = "call EmpGetListOfAppointments(:eid)";
		result = conn.createQuery(query).addParameter("eid", empID).executeAndFetch(AllAppointments.class);
		return result;
	}

}
