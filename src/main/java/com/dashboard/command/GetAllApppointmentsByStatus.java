package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllAppointments;

public class GetAllApppointmentsByStatus extends Command2<List<AllAppointments>> {
	int appointmentStatusID;

	public GetAllApppointmentsByStatus(int appointmentStatusID) {
		super(null);
		this.appointmentStatusID = appointmentStatusID;
	}

	@Override
	public List<AllAppointments> run(Connection conn) {
		String query = "call AppointmentGetAllAppointmentsByID(:appStatusID)";
		result = conn.createQuery(query).addParameter("appStatusID", appointmentStatusID)
				.executeAndFetch(AllAppointments.class);
		return result;
	}

}
