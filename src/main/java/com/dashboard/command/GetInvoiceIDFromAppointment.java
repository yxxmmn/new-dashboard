package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.InvoiceIDFromAppointment;

public class GetInvoiceIDFromAppointment extends Command2<List<InvoiceIDFromAppointment>> {
	String appointmentID;

	public GetInvoiceIDFromAppointment(String appointmentID) {
		super(null);
		this.appointmentID = appointmentID;
	}

	@Override
	public List<InvoiceIDFromAppointment> run(Connection conn) {
		String query = "call AppointmentGetInvoiceID(:appointmentID)";
		result = conn.createQuery(query).addParameter("appointmentID", this.appointmentID)
				.executeAndFetch(InvoiceIDFromAppointment.class);
		return result;
	}

}
