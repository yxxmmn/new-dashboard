package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllAppointments;

public class GetAllAppointments extends Command2<List<AllAppointments>> {

	public GetAllAppointments() {
		super(null);

	}

	@Override
	public List<AllAppointments> run(Connection conn) {
		String query = "call AppointmentGetAllAppointments()";
		result = conn.createQuery(query).executeAndFetch(AllAppointments.class);
		return result;
	}

}
