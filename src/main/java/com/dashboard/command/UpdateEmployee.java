package com.dashboard.command;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllEmployees;

public class UpdateEmployee extends Command2<AllEmployees> {

	String loginID;
	String password;
	String empContactNumber;
	String empName;
	String empDOB;
	String empAddress;
	String empDateJoined;
	String empEmail;
	int empPosition;
	int empStatus;
	int empID;

	public UpdateEmployee(String loginID, String password, String empContactNumber, String empName, String empDOB,
			String empAddress, String empDateJoined, String empEmail, int empPosition, int empStatus, int empID) {

		super(null);
		this.loginID = loginID;
		this.password = password;
		this.empContactNumber = empContactNumber;
		this.empName = empName;
		this.empDOB = empDOB;
		this.empAddress = empAddress;
		this.empDateJoined = empDateJoined;
		this.empPosition = empPosition;
		this.empEmail = empEmail;
		this.empStatus = empStatus;
		this.empID = empID;
	}

	@Override
	public AllEmployees run(Connection conn) {

		String query = "call EmpUpdateEmployee(:mloginID, :mpassword, :mempName, :mempContactNumber,  :mempDOB, :mempAddress, :mempDateJoined, :mempEmail, :mempStatus,  :mempPosition, :mempID)";

		AllEmployees e = new AllEmployees(loginID, password, empContactNumber, empName, empDOB, empAddress,
				empDateJoined, empEmail, empPosition, empStatus);

		conn.createQuery(query).addParameter("mloginID", this.loginID).addParameter("mpassword", this.password)
				.addParameter("mempName", this.empName).addParameter("mempContactNumber", this.empContactNumber)
				.addParameter("mempDOB", this.empDOB).addParameter("mempAddress", this.empAddress)
				.addParameter("mempDateJoined", this.empDateJoined).addParameter("mempEmail", this.empEmail)
				.addParameter("mempStatus", this.empStatus).addParameter("mempPosition", this.empPosition)
				.addParameter("mempID", this.empID).executeUpdate();
		return e;
	}

}
