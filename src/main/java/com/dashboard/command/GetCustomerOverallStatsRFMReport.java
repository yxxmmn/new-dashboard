package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.CustomerOverallStatsRFMReport;

public class GetCustomerOverallStatsRFMReport extends Command2<List<CustomerOverallStatsRFMReport>> {
	String s;
	String e;

	public GetCustomerOverallStatsRFMReport(String startDate, String endDate) {
		super(null);
		this.s = startDate;
		this.e = endDate;
	}

	@Override
	public List<CustomerOverallStatsRFMReport> run(Connection conn) {
		String query = "call getOverallCustomerStatsReportRFMTable(:s, :e)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e", this.e)
				.executeAndFetch(CustomerOverallStatsRFMReport.class);
		return result;
	}

}
