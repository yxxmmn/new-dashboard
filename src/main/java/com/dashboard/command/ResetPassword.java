package com.dashboard.command;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.PasswordReset;

public class ResetPassword extends Command2<PasswordReset> {

	int empID;
	String password;
	String loginID;
	String employeeEmail;

	public ResetPassword(String loginID, String password, String employeeEmail) {
		super(null);
		// TODO Auto-generated constructor stub
		this.employeeEmail = employeeEmail;
		this.password = password;
		this.loginID = loginID;
	}

	@Override
	public PasswordReset run(Connection conn) {
		String query = "call resetPasswordRandom(:employeeEmail, :loginID,:password)";
		PasswordReset pr = new PasswordReset(loginID, employeeEmail);
		conn.createQuery(query).addParameter("employeeEmail", this.employeeEmail).addParameter("loginID", this.loginID)
				.addParameter("password", this.password).executeUpdate();
		return pr;
	}

}
