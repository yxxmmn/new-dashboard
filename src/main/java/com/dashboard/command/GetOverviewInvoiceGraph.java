package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.OverviewInvoiceGraph;

public class GetOverviewInvoiceGraph extends Command2<List<OverviewInvoiceGraph>> {
	String s;
	String e;

	public GetOverviewInvoiceGraph(String startDate, String endDate) {
		super(null);
		this.s = startDate;
		this.e = endDate;
	}

	@Override
	public List<OverviewInvoiceGraph> run(Connection conn) {
		String query = "call OverviewGetInvoiceGraphByCategory(:s, :e)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e", this.e)
				.executeAndFetch(OverviewInvoiceGraph.class);
		return result;
	}

}
