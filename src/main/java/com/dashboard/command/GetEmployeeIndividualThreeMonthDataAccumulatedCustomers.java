package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.EmployeeIndividualThreeMonthDataAccumulatedCustomers;

public class GetEmployeeIndividualThreeMonthDataAccumulatedCustomers
		extends Command2<List<EmployeeIndividualThreeMonthDataAccumulatedCustomers>> {

	String s;
	String e1;
	String e2;
	String e3;
	int eid;

	public GetEmployeeIndividualThreeMonthDataAccumulatedCustomers(String startDate1, String endDate1, String endDate2,
			String endDate3, int eid) {
		super(null);
		this.s = startDate1;
		this.e1 = endDate1;
		this.e2 = endDate2;
		this.e3 = endDate3;
		this.eid = eid;

	}

	@Override
	public List<EmployeeIndividualThreeMonthDataAccumulatedCustomers> run(Connection conn) {
		String query = "call getIndividualEmployeeThreeMonthAccumulatedCustomers(:s, :e1, :e2, :e3, :eid)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e1", this.e1)
				.addParameter("e2", this.e2).addParameter("e3", this.e3).addParameter("eid", this.eid)
				.executeAndFetch(EmployeeIndividualThreeMonthDataAccumulatedCustomers.class);
		return result;
	}

}
