package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.EmployeePositionDropdown;

public class GetEmployeePositionDropdown extends Command2<List<EmployeePositionDropdown>> {

	public GetEmployeePositionDropdown() {
		super(null);

	}

	@Override
	public List<EmployeePositionDropdown> run(Connection conn) {
		String query = "call EmpGetDropdownListForEmployeePosition()";
		result = conn.createQuery(query).executeAndFetch(EmployeePositionDropdown.class);
		return result;
	}

}
