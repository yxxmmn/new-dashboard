package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllEmployees;

public class GetAllActiveEmployees extends Command2<List<AllEmployees>> {

	public GetAllActiveEmployees() {
		super(null);

	}

	@Override
	public List<AllEmployees> run(Connection conn) {
		String query = "call EmpGetAllActiveEmployees()";
		result = conn.createQuery(query).executeAndFetch(AllEmployees.class);
		return result;
	}


}
