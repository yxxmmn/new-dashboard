package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.SakeBrandDropdown;

public class GetSakeBrandDropdown extends Command2<List<SakeBrandDropdown>> {

	public GetSakeBrandDropdown() {
		super(null);

	}

	@Override
	public List<SakeBrandDropdown> run(Connection conn) {
		String query = "call SakeGetDropdownListForSakeBrand()";
		result = conn.createQuery(query).executeAndFetch(SakeBrandDropdown.class);
		return result;
	}

}
