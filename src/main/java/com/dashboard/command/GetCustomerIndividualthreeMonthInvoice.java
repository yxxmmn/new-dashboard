package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.CustomerIndividualThreeMonthInvoice;

public class GetCustomerIndividualthreeMonthInvoice extends Command2<List<CustomerIndividualThreeMonthInvoice>> {

	String s1;
	String e1;
	String s2;
	String e2;
	String s3;
	String e3;

	int cid;

	public GetCustomerIndividualthreeMonthInvoice(String startDate1, String endDate1, String startDate2,
			String endDate2, String startDate3, String endDate3, int cid) {
		super(null);
		this.s1 = startDate1;
		this.e1 = endDate1;
		this.s2 = startDate2;
		this.e2 = endDate2;
		this.s3 = startDate3;
		this.e3 = endDate3;
		this.cid = cid;

	}

	@Override
	public List<CustomerIndividualThreeMonthInvoice> run(Connection conn) {
		String query = "call getCustomerIndividualThreeMonthInvoice(:s1, :e1, :s2, :e2, :s3, :e3, :cid)";
		result = conn.createQuery(query).addParameter("s1", this.s1).addParameter("e1", this.e1)
				.addParameter("s2", this.s2).addParameter("e2", this.e2).addParameter("s3", this.s3)
				.addParameter("e3", this.e3).addParameter("cid", this.cid)
				.executeAndFetch(CustomerIndividualThreeMonthInvoice.class);
		return result;
	}

}
