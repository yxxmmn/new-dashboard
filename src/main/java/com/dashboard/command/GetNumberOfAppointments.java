package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.NumberOfAppointments;

public class GetNumberOfAppointments extends Command2<List<NumberOfAppointments>> {
	String s;
	String e;

	public GetNumberOfAppointments(String startDate, String endDate) {
		super(null);
		this.s = startDate;
		this.e = endDate;
	}

	@Override
	public List<NumberOfAppointments> run(Connection conn) {
		String query = "call getNumberOfAppointments(:s, :e)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e", this.e)
				.executeAndFetch(NumberOfAppointments.class);
		return result;
	}
}
