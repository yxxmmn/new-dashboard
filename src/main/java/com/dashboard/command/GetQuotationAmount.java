package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.QuotationAmount;

public class GetQuotationAmount extends Command2<List<QuotationAmount>> {
	String s;
	String e;

	public GetQuotationAmount(String startDate, String endDate) {
		super(null);
		this.s = startDate;
		this.e = endDate;
	}

	@Override
	public List<QuotationAmount> run(Connection conn) {
		String query = "call getQuotationAmount(:s, :e)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e", this.e)
				.executeAndFetch(QuotationAmount.class);
		return result;
	}

}
