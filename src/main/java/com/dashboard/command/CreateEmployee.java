package com.dashboard.command;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllEmployees;

public class CreateEmployee extends Command2<AllEmployees> {
	String loginID;
	String password;
	String empContactNumber;
	String empName;
	String empDOB;
	String empAddress;
	String empDateJoined;
	int empPosition;
	String empEmail;
	int empStatus;

	public CreateEmployee(String loginID, String password, String empContactNumber, String empName, String empDOB,
			String empAddress, String empDateJoined, String empEmail, int empPosition, int empStatus) {

		super(null);
		this.loginID = loginID;
		this.password = password;
		this.empContactNumber = empContactNumber;
		this.empName = empName;
		this.empDOB = empDOB;
		this.empAddress = empAddress;
		this.empDateJoined = empDateJoined;
		this.empPosition = empPosition;
		this.empEmail = empEmail;
		this.empStatus = empStatus;
	}

	public AllEmployees run(Connection conn) {
		String query = "call EmpAddEmployee(:mloginID, :mpassword,:mempName, :mempContactNumber, :mempDOB, :mempAddress, :mempDateJoined, :mempEmail, :mempStatus, :mempPosition)";

		AllEmployees e = new AllEmployees(loginID, password, empContactNumber, empName, empDOB, empAddress,
				empDateJoined, empEmail, empStatus, empPosition);

		conn.createQuery(query).addParameter("mloginID", this.loginID).addParameter("mpassword", this.password)
				.addParameter("mempName", this.empName).addParameter("mempContactNumber", this.empContactNumber)
				.addParameter("mempDOB", this.empDOB).addParameter("mempAddress", this.empAddress)
				.addParameter("mempDateJoined", this.empDateJoined).addParameter("mempEmail", this.empEmail)
				.addParameter("mempStatus", this.empStatus).addParameter("mempPosition", this.empPosition)
				.executeUpdate();
		return e;
	}

}
