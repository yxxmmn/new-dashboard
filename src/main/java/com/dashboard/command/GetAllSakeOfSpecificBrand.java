package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.Sake;

public class GetAllSakeOfSpecificBrand extends Command2<List<Sake>> {

	String cat;

	public GetAllSakeOfSpecificBrand(String cat) {
		super(null);
		this.cat = cat;

	}

	@Override
	public List<Sake> run(Connection conn) {
		String query = "call getAllSakeOfSpecificBrand(:cat)";
		result = conn.createQuery(query).addParameter("cat", this.cat).executeAndFetch(Sake.class);
		return result;
	}

}
