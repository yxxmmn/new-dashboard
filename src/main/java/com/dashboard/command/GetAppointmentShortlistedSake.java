package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AppointmentShortlistedSake;

public class GetAppointmentShortlistedSake extends Command2<List<AppointmentShortlistedSake>> {
	String appointmentID;

	public GetAppointmentShortlistedSake(String appointmentID) {
		super(null);
		this.appointmentID = appointmentID;
	}

	@Override
	public List<AppointmentShortlistedSake> run(Connection conn) {
		String query = "call AppointmentGetShorlistedSake(:appointmentID)";
		result = conn.createQuery(query).addParameter("appointmentID", this.appointmentID)
				.executeAndFetch(AppointmentShortlistedSake.class);
		return result;
	}

}
