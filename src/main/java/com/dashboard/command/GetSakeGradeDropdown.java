package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.SakeGradeDropdown;

public class GetSakeGradeDropdown extends Command2<List<SakeGradeDropdown>> {

	public GetSakeGradeDropdown() {
		super(null);

	}

	@Override
	public List<SakeGradeDropdown> run(Connection conn) {
		String query = "call SakeGetDropdownListForSakeGrade()";
		result = conn.createQuery(query).executeAndFetch(SakeGradeDropdown.class);
		return result;
	}

}
