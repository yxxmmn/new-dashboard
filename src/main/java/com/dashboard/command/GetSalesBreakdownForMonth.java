package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.SalesBreakdownForMonth;

public class GetSalesBreakdownForMonth extends Command2<List<SalesBreakdownForMonth>> {

	String s1;
	String e1;

	public GetSalesBreakdownForMonth(String startDate1, String endDate1) {
		super(null);
		this.s1 = startDate1;
		this.e1 = endDate1;

	}

	@Override
	public List<SalesBreakdownForMonth> run(Connection conn) {
		String query = "call OverviewGetInvoiceBreakdownByCategories(:s1, :e1)";
		result = conn.createQuery(query).addParameter("s1", this.s1).addParameter("e1", this.e1)
				.executeAndFetch(SalesBreakdownForMonth.class);
		return result;
	}

}
