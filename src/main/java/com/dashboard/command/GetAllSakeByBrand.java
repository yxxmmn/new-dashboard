package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.Sake;

public class GetAllSakeByBrand extends Command2<List<Sake>> {
	String brand;

	public GetAllSakeByBrand(String brand) {
		super(null);
		this.brand = brand;
	}

	@Override
	public List<Sake> run(Connection conn) {
		String query = "call SakeGetAllSakeByBrand(:brand)";
		result = conn.createQuery(query).addParameter("brand", brand).executeAndFetch(Sake.class);
		return result;
	}

}
