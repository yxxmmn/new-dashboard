package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllClients;

public class GetAllContactsByCategory extends Command2<List<AllClients>> {
	int customerCategory;

	public GetAllContactsByCategory(int customerCategory) {
		super(null);
		this.customerCategory = customerCategory;
	}

	@Override
	public List<AllClients> run(Connection conn) {
		String query = "call ClientGetAllContactsByCategory(:customerCategory)";
		result = conn.createQuery(query).addParameter("customerCategory", customerCategory)
				.executeAndFetch(AllClients.class);
		return result;
	}

}
