package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.OrdersBreakdownForMonth;

public class GetOrdersBreakdownForMonth extends Command2<List<OrdersBreakdownForMonth>> {

	String s1;
	String e1;

	public GetOrdersBreakdownForMonth(String startDate1, String endDate1) {
		super(null);
		this.s1 = startDate1;
		this.e1 = endDate1;

	}

	@Override
	public List<OrdersBreakdownForMonth> run(Connection conn) {
		String query = "call OverviewGetOrdersBreakdownByCategories(:s1, :e1)";
		result = conn.createQuery(query).addParameter("s1", this.s1).addParameter("e1", this.e1)
				.executeAndFetch(OrdersBreakdownForMonth.class);
		return result;
	}

}
