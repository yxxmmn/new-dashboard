package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllClients;

public class GetAllContacts extends Command2<List<AllClients>> {

	public GetAllContacts() {
		super(null);

	}

	@Override
	public List<AllClients> run(Connection conn) {
		String query = "call ClientGetAllContacts()";
		result = conn.createQuery(query).executeAndFetch(AllClients.class);
		return result;
	}

}
