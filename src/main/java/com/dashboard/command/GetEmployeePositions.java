package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.EmployeePosition;

public class GetEmployeePositions extends Command2<List<EmployeePosition>> {

	public GetEmployeePositions() {
		super(null);

	}

	@Override
	public List<EmployeePosition> run(Connection conn) {
		String query = "call getEmployeePositions()";
		result = conn.createQuery(query).executeAndFetch(EmployeePosition.class);
		return result;
	}

}
