package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.EmployeeIndividualSixMonthDataAccumulatedCustomers;

public class GetEmployeeIndividualSixMonthDataAccumulatedCustomers
		extends Command2<List<EmployeeIndividualSixMonthDataAccumulatedCustomers>> {

	String s;
	String e1;
	String e2;
	String e3;
	String e4;
	String e5;
	String e6;
	int eid;

	public GetEmployeeIndividualSixMonthDataAccumulatedCustomers(String startDate1, String endDate1, String endDate2,
			String endDate3, String endDate4, String endDate5, String endDate6, int eid) {
		super(null);
		this.s = startDate1;
		this.e1 = endDate1;
		this.e2 = endDate2;
		this.e3 = endDate3;
		this.e4 = endDate4;
		this.e5 = endDate5;
		this.e6 = endDate6;
		this.eid = eid;

	}

	@Override
	public List<EmployeeIndividualSixMonthDataAccumulatedCustomers> run(Connection conn) {
		String query = "call getIndividualEmployeeSixMonthAccumulatedCustomers(:s, :e1, :e2, :e3, :e4, :e5, :e6, :eid)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e1", this.e1)
				.addParameter("e2", this.e2).addParameter("e3", this.e3).addParameter("e4", this.e4)
				.addParameter("e5", this.e5).addParameter("e6", this.e6).addParameter("eid", this.eid)
				.executeAndFetch(EmployeeIndividualSixMonthDataAccumulatedCustomers.class);
		return result;
	}

}
