package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.SakePerformance;

public class GetAllSakePerformanceBreakdownByBrand extends Command2<List<SakePerformance>> {
	String sakeBrand;

	public GetAllSakePerformanceBreakdownByBrand(String sakeBrand) {
		super(null);
		this.sakeBrand = sakeBrand;
	}

	@Override
	public List<SakePerformance> run(Connection conn) {
		String query = "call SakeGetSakePerformanceBreakdownByBrand(:sakeBrand)";
		result = conn.createQuery(query).addParameter("sakeBrand", sakeBrand).executeAndFetch(SakePerformance.class);
		return result;
	}

}
