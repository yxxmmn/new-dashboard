package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllSakeBrand;

public class GetAllSakeBrand extends Command2<List<AllSakeBrand>> {

	public GetAllSakeBrand() {
		super(null);

	}

	@Override
	public List<AllSakeBrand> run(Connection conn) {
		String query = "call getAllSakeBrand()";
		result = conn.createQuery(query).executeAndFetch(AllSakeBrand.class);
		return result;
	}

}
