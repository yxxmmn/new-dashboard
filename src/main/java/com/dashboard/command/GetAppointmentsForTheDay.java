package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.Appointment;

public class GetAppointmentsForTheDay extends Command2<List<Appointment>> {
	String s;
	String e;

	public GetAppointmentsForTheDay(String startDate, String endDate) {
		super(null);
		this.s = startDate;
		this.e = endDate;
	}

	@Override
	public List<Appointment> run(Connection conn) {
		String query = "call OverviewGetAppointmentsForTheDay(:s, :e)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e", this.e)
				.executeAndFetch(Appointment.class);
		return result;
	}
}
