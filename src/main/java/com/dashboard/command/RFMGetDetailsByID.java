package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.RFMGetDetailsByIDClass;

public class RFMGetDetailsByID extends Command2<List<RFMGetDetailsByIDClass>> {
	int clientID;
	
	public RFMGetDetailsByID(int clientID) {
		super(null);
		this.clientID = clientID;
	}

	@Override
	public List<RFMGetDetailsByIDClass> run(Connection conn) {
		String query = "call RFMGetRFMInfoByID(:clientID)";
		result = conn.createQuery(query).addParameter("clientID", this.clientID).executeAndFetch(RFMGetDetailsByIDClass.class);
		return result;
	}

}