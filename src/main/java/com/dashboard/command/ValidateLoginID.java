package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.LoginIDValidator;

public class ValidateLoginID extends Command2<List<LoginIDValidator>> {

	String loginID;

	public ValidateLoginID(String loginID) {
		super(null);
		this.loginID = loginID;

	}

	@Override
	public List<LoginIDValidator> run(Connection conn) {
		String query = "call EmpValidateLoginID(:loginID)";
		result = conn.createQuery(query).addParameter("loginID", this.loginID).executeAndFetch(LoginIDValidator.class);
		return result;
	}

}
