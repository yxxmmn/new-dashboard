package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllAppointments;

public class GetAppointmentByAppointmentID extends Command2<List<AllAppointments>> {
	String appointmentID;

	public GetAppointmentByAppointmentID(String appointmentID) {
		super(null);
		this.appointmentID = appointmentID;

	}

	@Override
	public List<AllAppointments> run(Connection conn) {
		String query = "call AppointmentGetAppointmentByID(:appointmentID)";
		result = conn.createQuery(query).addParameter("appointmentID", appointmentID).executeAndFetch(AllAppointments.class);
		return result;
	}

}
