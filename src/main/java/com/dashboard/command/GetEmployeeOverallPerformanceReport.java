package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.EmployeeOverallPerformanceReport;

public class GetEmployeeOverallPerformanceReport extends Command2<List<EmployeeOverallPerformanceReport>> {
	String s;
	String e;

	public GetEmployeeOverallPerformanceReport(String startDate, String endDate) {
		super(null);
		this.s = startDate;
		this.e = endDate;
	}

	@Override
	public List<EmployeeOverallPerformanceReport> run(Connection conn) {
		String query = "call EmpGetOverallEmployeeReport(:s, :e)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e", this.e)
				.executeAndFetch(EmployeeOverallPerformanceReport.class);
		return result;
	}

}
