package com.dashboard.command;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllClients;

public class CreateContact extends Command2<AllClients> {

	int empID;
	String clientName;
	String parentCompany;
	String clientContactNumber;
	String clientEmail;
	String clientType;
	int clientCategoryID;
	String clientBillingAddress;
	String clientShippingAddress;
	String POC1Name;
	String POC1ContactNumber;
	String POC1Email;
	String POC2Name;
	String POC2ContactNumber;
	String POC2Email;
	String clientDateAdded;

	public CreateContact(int empID, String clientName, String parentCompany, String clientContactNumber,
			String clientEmail, String clientType, int clientCategoryID, String clientBillingAddress,
			String clientShippingAddress, String POC1Name, String POC1ContactNumber, String POC1Email, String POC2Name,
			String POC2ContactNumber, String POC2Email, String clientDateAdded) {

		super(null);
		this.empID = empID;
		this.clientName = clientName;
		this.parentCompany = parentCompany;
		this.clientContactNumber = clientContactNumber;
		this.clientEmail = clientEmail;
		this.clientType = clientType;
		this.clientCategoryID = clientCategoryID;
		this.clientBillingAddress = clientBillingAddress;
		this.clientShippingAddress = clientShippingAddress;
		this.POC1Name = POC1Name;
		this.POC1ContactNumber = POC1ContactNumber;
		this.POC1Email = POC1Email;
		this.POC2Name = POC2Name;
		this.POC2ContactNumber = POC2ContactNumber;
		this.POC2Email = POC2Email;
		this.clientDateAdded = clientDateAdded;
	}

	public AllClients run(Connection conn) {
		String query = "call ClientAddNewContact(:mempID, :mclientName, :mparentCompany, :mclientContactNumber, :mclientEmail, "
				+ ":mclientType, :mclientCategoryID, :mclientBillingAddress, :mclientShippingAddress, "
				+ ":mPOC1Name, :mPOC1ContactNumber, :mPOC1Email, :mPOC2Name, "
				+ ":mPOC2ContactNumber, :mPOC2Email, :mclientDateAdded)";

		AllClients c = new AllClients(empID, clientName, parentCompany, clientContactNumber, clientEmail, clientType,
				clientCategoryID, clientBillingAddress, clientShippingAddress, POC1Name, POC1ContactNumber, POC1Email,
				POC2Name, POC2ContactNumber, POC2Email, clientDateAdded);

		conn.createQuery(query).addParameter("mempID", this.empID).addParameter("mclientName", this.clientName)
				.addParameter("mparentCompany", this.parentCompany)
				.addParameter("mclientContactNumber", this.clientContactNumber)
				.addParameter("mclientEmail", this.clientEmail).addParameter("mclientType", this.clientType)
				.addParameter("mclientCategoryID", this.clientCategoryID).addParameter("mPOC1Name", this.POC1Name)
				.addParameter("mclientBillingAddress", this.clientBillingAddress)
				.addParameter("mclientShippingAddress", this.clientShippingAddress)
				.addParameter("mPOC1ContactNumber", this.POC1ContactNumber).addParameter("mPOC1Email", this.POC1Email)
				.addParameter("mPOC2Name", this.POC2Name).addParameter("mPOC2ContactNumber", this.POC2ContactNumber)
				.addParameter("mPOC2Email", this.POC2Email).addParameter("mclientDateAdded", this.clientDateAdded)
				.executeUpdate();
		return c;
	}

}
