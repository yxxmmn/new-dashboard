package com.dashboard.command;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllClients;

public class UpdateClientDetails extends Command2<AllClients> {
	int clientID;
	int empID;
	String clientName;
	String parentCompany;
	String clientContactNumber;
	String clientEmail;
	int clientCategoryID;
	String clientBillingAddress;
	String clientShippingAddress;
	String POC1Name;
	String POC1ContactNumber;
	String POC1Email;
	String POC2Name;
	String POC2ContactNumber;
	String POC2Email;

	public UpdateClientDetails(int clientID, int empID, String clientName, String parentCompany,
			String clientContactNumber, String clientEmail, int clientCategoryID, String clientBillingAddress,
			String clientShippingAddress, String POC1Name, String POC1ContactNumber, String POC1Email, String POC2Name,
			String POC2ContactNumber, String POC2Email) {
		super(null);
		this.clientID = clientID;
		this.empID = empID;
		this.clientName = clientName;
		this.parentCompany = parentCompany;
		this.clientContactNumber = clientContactNumber;
		this.clientEmail = clientEmail;
		this.clientCategoryID = clientCategoryID;
		this.clientBillingAddress = clientBillingAddress;
		this.clientShippingAddress = clientShippingAddress;
		this.POC1Name = POC1Name;
		this.POC1ContactNumber = POC1ContactNumber;
		this.POC1Email = POC1Email;
		this.POC2Name = POC2Name;
		this.POC2ContactNumber = POC2ContactNumber;
		this.POC2Email = POC2Email;
	}

	@Override
	public AllClients run(Connection conn) {
		String query = "call ClientUpdateClient(:clientID, :empID, :clientName, :parentCompany, :clientContactNumber, "
				+ ":clientEmail, :clientCategoryID, :clientBillingAddress, :clientShippingAddress, "
				+ ":POC1Name, :POC1ContactNumber, :POC1Email,:POC2Name, :POC2ContactNumber, :POC2Email)";
		AllClients c = new AllClients(clientID, empID, clientName, parentCompany, clientContactNumber, clientEmail,
				clientCategoryID, clientBillingAddress, clientShippingAddress, POC1Name, POC1ContactNumber, POC1Email,
				POC2Name, POC2ContactNumber, POC2Email);

		conn.createQuery(query).addParameter("clientID", this.clientID).addParameter("empID", this.empID)
				.addParameter("clientName", this.clientName).addParameter("parentCompany", this.parentCompany)
				.addParameter("clientContactNumber", this.clientContactNumber)
				.addParameter("clientEmail", this.clientEmail).addParameter("clientCategoryID", this.clientCategoryID)
				.addParameter("clientBillingAddress", this.clientBillingAddress)
				.addParameter("clientShippingAddress", this.clientShippingAddress)
				.addParameter("POC1Name", this.POC1Name).addParameter("POC1ContactNumber", this.POC1ContactNumber)
				.addParameter("POC1Email", this.POC1Email).addParameter("POC2Name", this.POC2Name)
				.addParameter("POC2ContactNumber", this.POC2ContactNumber).addParameter("POC2Email", this.POC2Email)
				.executeUpdate();
		return c;
	}

}
