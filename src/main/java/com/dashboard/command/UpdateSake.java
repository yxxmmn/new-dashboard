package com.dashboard.command;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.Sake;

public class UpdateSake extends Command2<Sake> {

	String sakeBrand;
	String sakeName;
	String sakeGrade;
	String sakeImage;
	String sakeDescription;
	String skuNumber;
	double sakeRetailPrice;
	double sakeTradePrice;
	double sakeWholesalePrice;
	int sakeCapacity;
	int sakePolish;
	String sakeRice;
	int sakeAcidity;
	double sakeAlcoholContent;
	int sakeSMV;
	int sakeStock;
	String dataURI;
	int sakeID;

	public UpdateSake(int sakeID, String sakeBrand, String sakeName, String sakeGrade, String sakeDescription,
			String skuNumber, double sakeRetailPrice, double sakeTradePrice, double sakeWholesalePrice,
			int sakeCapacity, int sakePolish, String sakeRice, int sakeAcidity, double sakeAlcoholContent, int sakeSMV,
			String dataURI) {
		super(null);
		this.sakeID = sakeID;
		this.sakeBrand = sakeBrand;
		this.sakeName = sakeName;
		this.sakeGrade = sakeGrade;
		this.sakeDescription = sakeDescription;
		this.skuNumber = skuNumber;
		this.sakeRetailPrice = sakeRetailPrice;
		this.sakeTradePrice = sakeTradePrice;
		this.sakeWholesalePrice = sakeWholesalePrice;
		this.sakeCapacity = sakeCapacity;
		this.sakePolish = sakePolish;
		this.sakeRice = sakeRice;
		this.sakeAcidity = sakeAcidity;
		this.sakeAlcoholContent = sakeAlcoholContent;
		this.sakeSMV = sakeSMV;
		this.dataURI = dataURI;
	}

	@Override
	public Sake run(Connection conn) {
		String query = "call SakeUpdateSake(:sakeID, :msakeBrand,:msakeName,:msakeGrade,"
				+ ":msakeDescription,:mskuNumber,:msakeRetailPrice,:msakeTradePrice,:msakeStock, :msakeWholesalePrice,:msakeCapacity,:msakePolish,"
				+ ":msakeRice,:msakeAcidity,:msakeAlcoholContent,:msakeSMV,:mdataURI)";
		Sake s = new Sake(sakeBrand, sakeName, sakeGrade, sakeDescription, skuNumber, sakeRetailPrice, sakeTradePrice,
				sakeStock, sakeWholesalePrice, sakeCapacity, sakePolish, sakeRice, sakeAcidity, sakeAlcoholContent,
				sakeSMV, dataURI);
		conn.createQuery(query).addParameter("sakeID", this.sakeID).addParameter("msakeBrand", this.sakeBrand)
				.addParameter("msakeName", this.sakeName).addParameter("msakeGrade", this.sakeGrade)
				.addParameter("msakeDescription", this.sakeDescription).addParameter("mskuNumber", this.skuNumber)
				.addParameter("msakeRetailPrice", this.sakeRetailPrice)
				.addParameter("msakeTradePrice", this.sakeTradePrice).addParameter("msakeStock", this.sakeStock)
				.addParameter("msakeWholesalePrice", this.sakeWholesalePrice)
				.addParameter("msakeCapacity", this.sakeCapacity).addParameter("msakePolish", this.sakePolish)
				.addParameter("msakeRice", this.sakeRice).addParameter("msakeAcidity", this.sakeAcidity)
				.addParameter("msakeAlcoholContent", this.sakeAlcoholContent).addParameter("msakeSMV", this.sakeSMV)
				.addParameter("mdataURI", this.dataURI).executeUpdate();
		return s;
	}

}
