package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.NewContactsForTopX;

public class GetNumberOfNewContactsForTopX extends Command2<List<NewContactsForTopX>>{
	String s;
	String e;
	String eID;
	int x;

	public GetNumberOfNewContactsForTopX(String startDate, String endDate, String empID, int x) {
		super(null);
		this.s = startDate;
		this.e = endDate;
		this.eID = empID;
		this.x = x;
	}

	@Override
	public List<NewContactsForTopX> run(Connection conn) {
		String query = "call getNumberOfNewContactsForTopX(:s, :e, :eID, :x)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e", this.e).addParameter("eID", this.eID).addParameter("x", this.x)
				.executeAndFetch(NewContactsForTopX.class);
		return result;
	}
}
