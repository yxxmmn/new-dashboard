package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AppointmentWithInvoice;

public class GetAllAppointmentsWithInvoice extends Command2<List<AppointmentWithInvoice>> {

	public GetAllAppointmentsWithInvoice() {
		super(null);

	}

	@Override
	public List<AppointmentWithInvoice> run(Connection conn) {
		String query = "call getAllAppointments()";
		result = conn.createQuery(query).executeAndFetch(AppointmentWithInvoice.class);
		return result;
	}

}
