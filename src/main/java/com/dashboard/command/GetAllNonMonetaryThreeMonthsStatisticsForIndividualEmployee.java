package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllNonMonetaryThreeMonthsStatisticsForIndividualEmployee;

public class GetAllNonMonetaryThreeMonthsStatisticsForIndividualEmployee
		extends Command2<List<AllNonMonetaryThreeMonthsStatisticsForIndividualEmployee>> {

	String s1;
	String e1;
	String s2;
	String e2;
	String s3;
	String e3;

	int eid;

	public GetAllNonMonetaryThreeMonthsStatisticsForIndividualEmployee(String startDate1, String endDate1,
			String startDate2, String endDate2, String startDate3, String endDate3, int eid) {
		super(null);
		this.s1 = startDate1;
		this.e1 = endDate1;
		this.s2 = startDate2;
		this.e2 = endDate2;
		this.s3 = startDate3;
		this.e3 = endDate3;

		this.eid = eid;

	}

	@Override
	public List<AllNonMonetaryThreeMonthsStatisticsForIndividualEmployee> run(Connection conn) {
		String query = "call getAllNonMonetaryThreeMonthsStatisticsForIndividualEmployee(:s1, :e1, :s2, :e2, :s3, :e3, :eid)";
		result = conn.createQuery(query).addParameter("s1", this.s1).addParameter("e1", this.e1)
				.addParameter("s2", this.s2).addParameter("e2", this.e2).addParameter("s3", this.s3)
				.addParameter("e3", this.e3).addParameter("eid", this.eid)
				.executeAndFetch(AllNonMonetaryThreeMonthsStatisticsForIndividualEmployee.class);
		return result;
	}

}
