package com.dashboard.command;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.Events;

public class UpdateEvent extends Command2<Events> {
	String eventName;
	String eventLocation;
	String eventStart;
	String eventDescription;
	String eventEnd;
	String eventID;

	public UpdateEvent(String eventID, String eventName, String eventLocation, String eventStart,
			String eventDescription, String eventEnd) {
		super(null);
		this.eventID = eventID;
		this.eventName = eventName;
		this.eventLocation = eventLocation;
		this.eventStart = eventStart;
		this.eventDescription = eventDescription;
		this.eventEnd = eventEnd;
	}

	@Override
	public Events run(Connection conn) {
		String query = "call CalendarUpdateEvent(:eventID, :eventName,  :eventStart, :eventEnd,:eventLocation, :eventDescription)";
		Events e = new Events(eventID, eventName, eventStart, eventEnd, eventLocation, eventDescription);

		conn.createQuery(query).addParameter("eventID", this.eventID).addParameter("eventName", this.eventName)
				.addParameter("eventStart", this.eventStart).addParameter("eventEnd", this.eventEnd)
				.addParameter("eventLocation", this.eventLocation)
				.addParameter("eventDescription", this.eventDescription).executeUpdate();
		return e;
	}

}
