package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.CustomerOverallStatsRFMReport;

public class GetCustomerOverallStatsRFMReportByCategory extends Command2<List<CustomerOverallStatsRFMReport>> {
	String s;
	String e;
	String cat;

	public GetCustomerOverallStatsRFMReportByCategory(String startDate, String endDate, String category) {
		super(null);
		this.s = startDate;
		this.e = endDate;
		this.cat = category;
	}

	@Override
	public List<CustomerOverallStatsRFMReport> run(Connection conn) {
		String query = "call getOverallCustomerStatsReportRFMTableByCategory(:s, :e, :cat)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e", this.e)
				.addParameter("cat", this.cat).executeAndFetch(CustomerOverallStatsRFMReport.class);
		return result;
	}

}