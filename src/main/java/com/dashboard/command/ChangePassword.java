package com.dashboard.command;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.PasswordReset;

public class ChangePassword extends Command2<PasswordReset> {

	int empID;
	String password;

	public ChangePassword(int empID, String password) {
		super(null);
		// TODO Auto-generated constructor stub
		this.password = password;
		this.empID = empID;
	}

	@Override
	public PasswordReset run(Connection conn) {
		String query = "call setNewPassword(:empID, :password)";
		PasswordReset pr = new PasswordReset(empID, password);
		conn.createQuery(query).addParameter("empID", this.empID).addParameter("password", this.password)
				.executeUpdate();
		return pr;
	}

}
