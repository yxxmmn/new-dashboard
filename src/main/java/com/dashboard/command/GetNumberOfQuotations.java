package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.NumberOfQuotations;

public class GetNumberOfQuotations extends Command2<List<NumberOfQuotations>> {
	String s;
	String e;

	public GetNumberOfQuotations(String startDate, String endDate) {
		super(null);
		this.s = startDate;
		this.e = endDate;
	}

	@Override
	public List<NumberOfQuotations> run(Connection conn) {
		String query = "call getNumberOfQuotations(:s, :e)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e", this.e)
				.executeAndFetch(NumberOfQuotations.class);
		return result;
	}

}
