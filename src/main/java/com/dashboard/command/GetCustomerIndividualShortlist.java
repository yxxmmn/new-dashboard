package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.CustomerIndividualShortlist;

public class GetCustomerIndividualShortlist extends Command2<List<CustomerIndividualShortlist>> {
	int cID;

	public GetCustomerIndividualShortlist(int cID) {
		super(null);

		this.cID = cID;
	}

	@Override
	public List<CustomerIndividualShortlist> run(Connection conn) {
		String query = "call getCustomerIndividualSakeShortlist(:cID)";
		result = conn.createQuery(query).addParameter("cID", this.cID)
				.executeAndFetch(CustomerIndividualShortlist.class);
		return result;
	}

}
