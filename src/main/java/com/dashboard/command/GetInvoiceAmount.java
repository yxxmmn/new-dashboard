package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.InvoiceAmount;

public class GetInvoiceAmount extends Command2<List<InvoiceAmount>> {
	String s;
	String e;

	public GetInvoiceAmount(String startDate, String endDate) {
		super(null);
		this.s = startDate;
		this.e = endDate;
	}

	@Override
	public List<InvoiceAmount> run(Connection conn) {
		String query = "call getInvoiceAmount(:s, :e)";
		result = conn.createQuery(query).addParameter("s", this.s).addParameter("e", this.e)
				.executeAndFetch(InvoiceAmount.class);
		return result;
	}


}
