package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllAppointments;

public class GetAllAppointmentsByClient extends Command2<List<AllAppointments>> {
	int clientID;

	public GetAllAppointmentsByClient(int clientID) {
		super(null);
		this.clientID = clientID;

	}

	@Override
	public List<AllAppointments> run(Connection conn) {
		String query = "call ClientGetListOfAppointments(:cid)";
		result = conn.createQuery(query).addParameter("cid", clientID).executeAndFetch(AllAppointments.class);
		return result;
	}

}
