package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.InvoiceAmount;

public class GetInvoiceAmountForAppointment extends Command2<List<InvoiceAmount>> {
	String appointmentID;

	public GetInvoiceAmountForAppointment(String appointmentID) {
		super(null);
		this.appointmentID = appointmentID;
	}

	@Override
	public List<InvoiceAmount> run(Connection conn) {
		String query = "call AppointmentGetInvoiceByAppointment(:appointmentID)";
		result = conn.createQuery(query).addParameter("appointmentID", this.appointmentID)
				.executeAndFetch(InvoiceAmount.class);
		return result;
	}

}
