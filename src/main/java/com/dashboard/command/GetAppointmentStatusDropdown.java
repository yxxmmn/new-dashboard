package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AppointmentStatusDropdown;

public class GetAppointmentStatusDropdown extends Command2<List<AppointmentStatusDropdown>> {

	public GetAppointmentStatusDropdown() {
		super(null);

	}

	@Override
	public List<AppointmentStatusDropdown> run(Connection conn) {
		String query = "call MiscGetAllAppointmentStatus()";
		result = conn.createQuery(query).executeAndFetch(AppointmentStatusDropdown.class);
		return result;
	}

}
