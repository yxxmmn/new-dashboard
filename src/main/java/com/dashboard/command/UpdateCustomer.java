package com.dashboard.command;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.AllCustomers;;

public class UpdateCustomer extends Command2<AllCustomers> {
	int customerID;
	int empID;
	String name;
	String customerCategory;
	String billingAddress;
	String shippingAddress;
	String customerEmail;
	int contactnumber;

	public UpdateCustomer(int customerID, int empID, String contactName, String customerCategory, String billingAddress,
			String shippingAddress, String customerEmail, int contactnumber) {
		super(null);
		this.customerID = customerID;
		this.empID = empID;
		this.name = contactName;
		this.customerCategory = customerCategory;
		this.billingAddress = billingAddress;
		this.shippingAddress = shippingAddress;
		this.customerEmail = customerCategory;
		this.contactnumber = contactnumber;
	}

	@Override
	public AllCustomers run(Connection conn) {
		String query = "call updateCustomer(:customerID, :empID, :contactName, :customerCategory, :billingAddress, :shippingAddress, :customerEmail, :contactnumber)";
		AllCustomers c = new AllCustomers(customerID, empID, name, customerCategory, billingAddress,
				shippingAddress, customerEmail, contactnumber);
		conn.createQuery(query).addParameter("customerID", this.customerID).addParameter("empID", this.empID)
				.addParameter("contactName", this.name).addParameter("customerCategory", this.customerCategory)
				.addParameter("billingAddress", this.billingAddress)
				.addParameter("shippingAddress", this.shippingAddress).addParameter("customerEmail", this.customerEmail)
				.addParameter("contactnumber", this.contactnumber).executeUpdate();
		return c;
	}

}
