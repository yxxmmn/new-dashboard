package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.EmployeeIndividualSixMonthDataNewCustomer;

public class GetEmployeeIndividualSixMonthDataNewCustomer
		extends Command2<List<EmployeeIndividualSixMonthDataNewCustomer>> {

	String s1;
	String e1;
	String s2;
	String e2;
	String s3;
	String e3;
	String s4;
	String e4;
	String s5;
	String e5;
	String s6;
	String e6;
	int eid;

	public GetEmployeeIndividualSixMonthDataNewCustomer(String startDate1, String endDate1, String startDate2,
			String endDate2, String startDate3, String endDate3, String startDate4, String endDate4, String startDate5,
			String endDate5, String startDate6, String endDate6, int eid) {
		super(null);
		this.s1 = startDate1;
		this.e1 = endDate1;
		this.s2 = startDate2;
		this.e2 = endDate2;
		this.s3 = startDate3;
		this.e3 = endDate3;
		this.s4 = startDate4;
		this.e4 = endDate4;
		this.s5 = startDate5;
		this.e5 = endDate5;
		this.s6 = startDate6;
		this.e6 = endDate6;
		this.eid = eid;

	}

	@Override
	public List<EmployeeIndividualSixMonthDataNewCustomer> run(Connection conn) {
		String query = "call getIndividualEmployeeSixMonthNewCustomers(:s1, :e1, :s2, :e2, :s3, :e3,:s4, :e4, :s5, :e5, :s6, :e6, :eid)";
		result = conn.createQuery(query).addParameter("s1", this.s1).addParameter("e1", this.e1)
				.addParameter("s2", this.s2).addParameter("e2", this.e2).addParameter("s3", this.s3)
				.addParameter("e3", this.e3).addParameter("s4", this.s4).addParameter("e4", this.e4)
				.addParameter("s5", this.s5).addParameter("e5", this.e5).addParameter("s6", this.s6)
				.addParameter("e6", this.e6).addParameter("eid", this.eid)
				.executeAndFetch(EmployeeIndividualSixMonthDataNewCustomer.class);
		return result;
	}

}
