package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.RFM;
import com.dashboard.dao.Sake;

public class RFMGetRFMCompanyValues extends Command2<List<RFM>> {
	
	public RFMGetRFMCompanyValues() {
		super(null);
	}

	@Override
	public List<RFM> run(Connection conn) {
		String query = "call RFMGetRFMCompanyValues()";
		result = conn.createQuery(query).executeAndFetch(RFM.class);
		return result;
	}

}
