package com.dashboard.command;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.controller.header.Command2;
import com.dashboard.dao.EmployeeDropdown;

public class GetEmployeeDropdown extends Command2<List<EmployeeDropdown>> {

	public GetEmployeeDropdown() {
		super(null);

	}

	@Override
	public List<EmployeeDropdown> run(Connection conn) {
		String query = "call EmpGetDropdownListForEmployee()";
		result = conn.createQuery(query).executeAndFetch(EmployeeDropdown.class);
		return result;
	}

}
