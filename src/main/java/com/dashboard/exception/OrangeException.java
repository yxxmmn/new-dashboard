package com.dashboard.exception;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OrangeException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6134466703639224120L;

	public OrangeException(String message) {
		super(new Exception(message));
	}
}
