package com.dashboard.exception;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GreenException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 496760310299776737L;

	public GreenException(String message) {
		super(message);
	}

}
