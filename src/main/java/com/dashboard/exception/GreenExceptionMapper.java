package com.dashboard.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class GreenExceptionMapper implements ExceptionMapper<GreenException> {

	@Override
	public Response toResponse(GreenException exception) {
		ErrorMessage errorMessage = new ErrorMessage(exception.getMessage(), 406);
		System.out.println("thrown 406 : " + errorMessage.getErrorMessage());

		return Response.status(Status.NOT_ACCEPTABLE).entity(errorMessage).build();
	}
}
