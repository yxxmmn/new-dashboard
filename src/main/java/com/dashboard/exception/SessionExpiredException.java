package com.dashboard.exception;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SessionExpiredException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 760716927578046267L;

	public SessionExpiredException(String message) {
		super(new Exception(message));
	}
}
