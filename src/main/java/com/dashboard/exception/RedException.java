package com.dashboard.exception;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RedException extends RuntimeException{


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2436825405374214878L;

	public RedException(String message) {
		super(message);
	}

}
