package com.dashboard.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
@Provider
public class OrangeExceptionMapper implements ExceptionMapper<OrangeException>{

	@Override
	public Response toResponse(OrangeException exception) {
		ErrorMessage errorMessage = new ErrorMessage(exception.getMessage(), 503);
		System.out.println("thrown 503 : "+ errorMessage.getErrorMessage());

		return Response.status(Status.SERVICE_UNAVAILABLE).entity(errorMessage).build();
	}
}
