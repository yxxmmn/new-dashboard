package com.dashboard.exception.external;

public class DataNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5931331153588735436L;

	public DataNotFoundException(String message) {
		super(message);
	}

}
