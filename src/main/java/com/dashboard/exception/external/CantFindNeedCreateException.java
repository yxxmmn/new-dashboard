package com.dashboard.exception.external;

public class CantFindNeedCreateException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2685849688030873582L;

	public CantFindNeedCreateException(String message) {
		super(message);
	}

}
