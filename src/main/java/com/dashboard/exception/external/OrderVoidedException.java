package com.dashboard.exception.external;

public class OrderVoidedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6905837530455804383L;

	public OrderVoidedException(Exception ex) {
		super(ex);
	}

	public OrderVoidedException(String message) {
		super(new Exception(message));
	}

}
