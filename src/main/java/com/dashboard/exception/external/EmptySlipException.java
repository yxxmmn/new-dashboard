package com.dashboard.exception.external;

public class EmptySlipException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -774236658621690153L;

	public EmptySlipException(String message) {
		super(message);
	}
}
