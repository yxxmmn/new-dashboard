package com.dashboard.exception.external;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.dashboard.exception.ErrorMessage;

@Provider
public class OrderVoidedExceptionMapper implements ExceptionMapper<OrderVoidedException> {
	@Override
	public Response toResponse(OrderVoidedException ex) {
		ErrorMessage errorMessage = new ErrorMessage(ex.getMessage(), 307);
		System.out.println("thrown 307 :"+errorMessage.getErrorMessage());
		return Response.status(Status.TEMPORARY_REDIRECT).entity(errorMessage).build();
	}

}
