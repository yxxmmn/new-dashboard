package com.dashboard.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
@Provider
public class YellowExceptionMapper implements ExceptionMapper<YellowException>{

	@Override
	public Response toResponse(YellowException exception) {
		ErrorMessage errorMessage = new ErrorMessage(exception.getMessage(), 204);
		System.out.println("thrown 204 : "+ errorMessage.getErrorMessage());

		return Response.status(Status.NO_CONTENT).entity(errorMessage).build();
	}
}
