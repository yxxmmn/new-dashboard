package com.dashboard.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
@Provider
public class RedExceptionMapper implements ExceptionMapper<RedException>{

	@Override
	public Response toResponse(RedException exception) {
		ErrorMessage errorMessage = new ErrorMessage(exception.getMessage(), 410);
		System.out.println("thrown 410 : "+ errorMessage.getErrorMessage());

		return Response.status(Status.GONE).entity(errorMessage).build();
	}

}
