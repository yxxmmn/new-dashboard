package com.dashboard.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class SessionExceptionMapper implements ExceptionMapper<SessionExpiredException> {

	@Override
	public Response toResponse(SessionExpiredException exception) {
		ErrorMessage errorMessage = new ErrorMessage(exception.getMessage(), 504);
		System.out.println("thrown 504 : " + errorMessage.getErrorMessage());

		return Response.status(Status.GATEWAY_TIMEOUT).entity(errorMessage).build();
	}
}
