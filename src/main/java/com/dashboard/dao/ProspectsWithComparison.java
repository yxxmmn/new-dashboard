package com.dashboard.dao;

public class ProspectsWithComparison {
int numberOfNewProspectsThisMonth;
int numberOfNewProspectsLastMonth;
	double percentage;

	public int getNumberOfNewProspectsThisMonth() {
		return numberOfNewProspectsThisMonth;
	}

	public void setNumberOfNewProspectsThisMonth(int numberOfNewProspectsThisMonth) {
		this.numberOfNewProspectsThisMonth = numberOfNewProspectsThisMonth;
	}

	public int getNumberOfNewProspectsLastMonth() {
		return numberOfNewProspectsLastMonth;
	}

	public void setNumberOfNewProspectsLastMonth(int numberOfNewProspectsLastMonth) {
		this.numberOfNewProspectsLastMonth = numberOfNewProspectsLastMonth;
	}

	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}
}
