package com.dashboard.dao;

public class EmployeePosition {
	String position;

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
}
