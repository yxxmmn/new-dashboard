package com.dashboard.dao;

public class NewLeadsWithComparison {
	int thisMonthLeads;
	int pastMonthLeads;
	double percentage;

	public int getThisMonthLeads() {
		return thisMonthLeads;
	}

	public void setThisMonthLeads(int thisMonthLeads) {
		this.thisMonthLeads = thisMonthLeads;
	}

	public int getPastMonthLeads() {
		return pastMonthLeads;
	}

	public void setPastMonthLeads(int pastMonthLeads) {
		this.pastMonthLeads = pastMonthLeads;
	}

	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}
}
