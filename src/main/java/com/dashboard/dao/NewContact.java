package com.dashboard.dao;

public class NewContact {
	int empID;
	String billingAddress;
	String name;
	String shippingAddress;
	String customerType;
	String customerCategory;
	String dateAdded;
	String customerEmail;
	String contactNumber;

	public NewContact() {
	}

	public NewContact(int empID, String billingAddress, String name, String shippingAddress, String customerType,
			String customerCategory, String dateAdded, String customerEmail, String contactNumber) {
		super();
		// TODO Auto-generated constructor stub
		this.empID = empID;
		this.billingAddress = billingAddress;
		this.name = name;
		this.shippingAddress = shippingAddress;
		this.customerType = customerType;
		this.customerCategory = customerCategory;
		this.dateAdded = dateAdded;
		this.customerEmail = customerEmail;
		this.contactNumber = contactNumber;
	}

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getCustomerCategory() {
		return customerCategory;
	}

	public void setCustomerCategory(String customerCategory) {
		this.customerCategory = customerCategory;
	}

	public String getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(String dateAdded) {
		this.dateAdded = dateAdded;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
}
