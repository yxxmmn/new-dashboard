package com.dashboard.dao;

public class NumberOfQuotations {
	int numberOfQuotations;

	public int getNumberOfQuotations() {
		return numberOfQuotations;
	}

	public void setNumberOfQuotations(int numberOfQuotations) {
		this.numberOfQuotations = numberOfQuotations;
	}

}
