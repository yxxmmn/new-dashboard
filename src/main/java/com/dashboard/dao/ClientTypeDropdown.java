package com.dashboard.dao;

public class ClientTypeDropdown {
	String clientType;

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
}
