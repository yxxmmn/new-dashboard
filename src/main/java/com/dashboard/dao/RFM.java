package com.dashboard.dao;

public class RFM {

	int tier;
	int recency;
	int frequency;
	int monetary;
	
	public int getTier() {
		return tier;
	}
	public int getRecency() {
		return recency;
	}
	public int getFrequency() {
		return frequency;
	}
	public int getMonetary() {
		return monetary;
	}
	
	public void setTier(int tier) {
		this.tier = tier;
	}
	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	public void setRecency(int recency) {
		this.recency = recency;
	}
	public void setMonetary(int monetary) {
		this.monetary = monetary;
	}

}
