package com.dashboard.dao;

public class AppointmentStatusDropdown {
	int appointmentStatusID;
	String appointmentStatus;

	public int getAppointmentStatusID() {
		return appointmentStatusID;
	}

	public void setAppointmentStatusID(int appointmentStatusID) {
		this.appointmentStatusID = appointmentStatusID;
	}

	public String getAppointmentStatus() {
		return appointmentStatus;
	}

	public void setAppointmentStatus(String appointmentStatus) {
		this.appointmentStatus = appointmentStatus;
	}

}
