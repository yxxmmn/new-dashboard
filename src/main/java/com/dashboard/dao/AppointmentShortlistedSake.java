package com.dashboard.dao;

public class AppointmentShortlistedSake {
	int sakeID;
	String sakeName;
	boolean tasteStatus;
	String appointmentID;

	public int getSakeID() {
		return sakeID;
	}

	public void setSakeID(int sakeID) {
		this.sakeID = sakeID;
	}

	public String getSakeName() {
		return sakeName;
	}

	public void setSakeName(String sakeName) {
		this.sakeName = sakeName;
	}

	public boolean isTasteStatus() {
		return tasteStatus;
	}

	public void setTasteStatus(boolean tasteStatus) {
		this.tasteStatus = tasteStatus;
	}

	public String getAppointmentID() {
		return appointmentID;
	}

	public void setAppointmentID(String appointmentID) {
		this.appointmentID = appointmentID;
	}

}
