package com.dashboard.dao;

import java.util.ArrayList;
import java.util.List;

public class RFMLogic {

	int tier;
	int recency;
	int frequency;
	int monetary;
	
	public int getTier() {
		return tier;
	}
	public int getRecency() {
		return recency;
	}
	public int getFrequency() {
		return frequency;
	}
	public int getMonetary() {
		return monetary;
	}
	
	public void setTier(int tier) {
		this.tier = tier;
	}
	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	public void setRecency(int recency) {
		this.recency = recency;
	}
	public void setMonetary(int monetary) {
		this.monetary = monetary;
	}
	
	public String doSort(List<RFM> rfmList,int recency, double frequency, int monetary) {

		//Sort populate Lists of threshold taken from RFM DBs
		ArrayList<Integer> recencyValues = new ArrayList<Integer>();
		ArrayList<Integer> frequencyValues = new ArrayList<Integer>();
		ArrayList<Integer> monetaryValues = new ArrayList<Integer>();
		
		for(RFM r:rfmList) {
			recencyValues.add(r.getRecency());
			frequencyValues.add(r.getFrequency());
			monetaryValues.add(r.getMonetary());
		}
		
		String recencyResult = this.recencyLogic(recency, recencyValues);
		String frequencyResult = this.frequencyLogic(frequency, frequencyValues);
		String monetaryResult = this.monetaryLogic(monetary, monetaryValues);
		
		return recencyResult + frequencyResult + monetaryResult;
	}
	
	public String sortCategory(String stringResult) {
		
		switch(stringResult) {
			case "111":
				return "Best Customer";
			case "311":
				return "Almost Lost";
			case "411":
				return "Lost Customer";
			case "444":
				return "Lost Cheap Customer";
		}
		
		if (stringResult.charAt(1) == '1' && stringResult.charAt(2) == '1') {
			return "Loyal and Big Spender";
		} else if(stringResult.charAt(1) == '1') {
			return "Loyal Customer";
		} else if(stringResult.charAt(2) == '1') {
			return "Big Spender";
		}
			
		return stringResult;
	}
	
	
	
	public String recencyLogic(int recency, ArrayList<Integer> recencyList) {
		int t1 = recencyList.get(0);
		int t2 = recencyList.get(1);
		int t3 = recencyList.get(2);
		int t4 = recencyList.get(3);
		
		// 1 meaning they purchased most recently.
		if(recency<=t1) {
			return "1";
		} else if(recency<=t2) {
			return "2";
		} else if(recency<=t3) {
			return "3";
		} else if(recency<=t4) {
			return "4";
		} 
		
		return null;
	}

	public String frequencyLogic(double frequency, ArrayList<Integer> frequencyList) {
		int t1 = frequencyList.get(0);
		int t2 = frequencyList.get(1);
		int t3 = frequencyList.get(2);
//		int t4 = frequencyList.get(3);
		
		// 1 meaning they buy the most bottle/mth
		if(frequency>=t1) {
			return "1";
		} else if(frequency>=t2) {
			return "2";
		} else if(frequency>=t3) {
			return "3";
		} else {
			return "4";
		}
		
	}
	
	public String monetaryLogic(int monetary, ArrayList<Integer> monetaryList) {
		int t1 = monetaryList.get(0);
		int t2 = monetaryList.get(1);
		int t3 = monetaryList.get(2);
//		int t4 = monetaryList.get(3);
		
		// 1 meaning they spent the most.
		if(monetary>=t1) {
			return "1";
		} else if(monetary>=t2) {
			return "2";
		} else if(monetary>=t3) {
			return "3";
		} else {
			return "4";
		}
		
	}
}
