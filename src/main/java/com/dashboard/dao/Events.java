package com.dashboard.dao;

public class Events {
	String eventName;
	String eventID;
	String eventLocation;
	String eventStart;
	String eventDescription;
	String eventEnd;

	public Events() {
	}

	public Events(String eventID, String eventName, String eventStart, String eventEnd, String eventLocation, String eventDescription) {
		this.eventID = eventID;
		this.eventName = eventName;
		this.eventStart = eventStart;
		this.eventEnd = eventEnd;
		this.eventLocation = eventLocation;
		this.eventDescription = eventDescription;
	}
	
	public Events( String eventName, String eventStart, String eventEnd, String eventLocation, String eventDescription) {
		this.eventName = eventName;
		this.eventStart = eventStart;
		this.eventEnd = eventEnd;
		this.eventLocation = eventLocation;
		this.eventDescription = eventDescription;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventID() {
		return eventID;
	}

	public void setEventID(String eventID) {
		this.eventID = eventID;
	}

	public String getEventLocation() {
		return eventLocation;
	}

	public void setEventLocation(String eventLocation) {
		this.eventLocation = eventLocation;
	}

	public String getEventStart() {
		return eventStart;
	}

	public void setEventStart(String eventStart) {
		this.eventStart = eventStart;
	}

	public String getEventEnd() {
		return eventEnd;
	}

	public void setEventEnd(String eventEnd) {
		this.eventEnd = eventEnd;
	}

	public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

}
