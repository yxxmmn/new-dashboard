package com.dashboard.dao;

public class MostImprovedEmployee {
	String empName;
	String empID;
	double invoiceAmountTwoMonthsAgo;
	double invoiceAmountLastMonth;
	double percentageIncrease;

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpID() {
		return empID;
	}

	public void setEmpID(String empID) {
		this.empID = empID;
	}

	public double getInvoiceAmountTwoMonthsAgo() {
		return invoiceAmountTwoMonthsAgo;
	}

	public void setInvoiceAmountTwoMonthsAgo(double invoiceAmountTwoMonthsAgo) {
		this.invoiceAmountTwoMonthsAgo = invoiceAmountTwoMonthsAgo;
	}

	public double getInvoiceAmountLastMonth() {
		return invoiceAmountLastMonth;
	}

	public void setInvoiceAmountLastMonth(double invoiceAmountLastMonth) {
		this.invoiceAmountLastMonth = invoiceAmountLastMonth;
	}

	public double getPercentageIncrease() {
		return percentageIncrease;
	}

	public void setPercentageIncrease(double percentageIncrease) {
		this.percentageIncrease = percentageIncrease;
	}

}
