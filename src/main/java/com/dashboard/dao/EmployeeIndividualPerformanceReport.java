package com.dashboard.dao;

public class EmployeeIndividualPerformanceReport {
	int empID;
	String empName;
	double invoiceAmountMonth;
	double averageInvoice;
	double quotationAmountMonth;
	int noOfAppointmentstMonth;
	int noOfContactsMonth;
	int noOfProspectsMonth;
	int noOfCustomerstMonth;

	public double getAverageInvoice() {
		return averageInvoice;
	}

	public void setAverageInvoice(double averageInvoice) {
		this.averageInvoice = averageInvoice;
	}

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public double getInvoiceAmountMonth() {
		return invoiceAmountMonth;
	}

	public void setInvoiceAmountMonth(double invoiceAmountMonth) {
		this.invoiceAmountMonth = invoiceAmountMonth;
	}

	public double getQuotationAmountMonth() {
		return quotationAmountMonth;
	}

	public void setQuotationAmountMonth(double quotationAmountMonth) {
		this.quotationAmountMonth = quotationAmountMonth;
	}

	public int getNoOfAppointmentstMonth() {
		return noOfAppointmentstMonth;
	}

	public void setNoOfAppointmentstMonth(int noOfAppointmentstMonth) {
		this.noOfAppointmentstMonth = noOfAppointmentstMonth;
	}

	public int getNoOfContactsMonth() {
		return noOfContactsMonth;
	}

	public void setNoOfContactsMonth(int noOfContactsMonth) {
		this.noOfContactsMonth = noOfContactsMonth;
	}

	public int getNoOfProspectsMonth() {
		return noOfProspectsMonth;
	}

	public void setNoOfProspectsMonth(int noOfProspectsMonth) {
		this.noOfProspectsMonth = noOfProspectsMonth;
	}

	public int getNoOfCustomerstMonth() {
		return noOfCustomerstMonth;
	}

	public void setNoOfCustomerstMonth(int noOfCustomerstMonth) {
		this.noOfCustomerstMonth = noOfCustomerstMonth;
	}

}
