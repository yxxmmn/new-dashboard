package com.dashboard.dao;

public class AllNonMonetaryThreeMonthsStatisticsForIndividualEmployee {
	int empID;

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	int thisMonthYear;
	int thisMonth;
	int lastMonthYear;
	int lastMonth;
	int twoMonthsAgoYear;
	int twoMonthsAgo;

	int thisMonthAppointments;
	int lastMonthAppointments;
	int twoMonthsAgoAppointments;

	int thisMonthNewContacts;
	int lastMonthNewContacts;
	int twoMonthsAgoNewContacts;

	int thisMonthNewProspects;
	int lastMonthNewProspects;
	int twoMonthsAgoNewProspects;

	int thisMonthNewCustomers;
	int lastMonthNewCustomers;
	int twoMonthsAgoNewCustomers;

	public int getThisMonthYear() {
		return thisMonthYear;
	}

	public void setThisMonthYear(int thisMonthYear) {
		this.thisMonthYear = thisMonthYear;
	}

	public int getThisMonth() {
		return thisMonth;
	}

	public void setThisMonth(int thisMonth) {
		this.thisMonth = thisMonth;
	}

	public int getLastMonthYear() {
		return lastMonthYear;
	}

	public void setLastMonthYear(int lastMonthYear) {
		this.lastMonthYear = lastMonthYear;
	}

	public int getLastMonth() {
		return lastMonth;
	}

	public void setLastMonth(int lastMonth) {
		this.lastMonth = lastMonth;
	}

	public int getTwoMonthsAgoYear() {
		return twoMonthsAgoYear;
	}

	public void setTwoMonthsAgoYear(int twoMonthsAgoYear) {
		this.twoMonthsAgoYear = twoMonthsAgoYear;
	}

	public int getTwoMonthsAgo() {
		return twoMonthsAgo;
	}

	public void setTwoMonthsAgo(int twoMonthsAgo) {
		this.twoMonthsAgo = twoMonthsAgo;
	}

	public int getThisMonthAppointments() {
		return thisMonthAppointments;
	}

	public void setThisMonthAppointments(int thisMonthAppointments) {
		this.thisMonthAppointments = thisMonthAppointments;
	}

	public int getLastMonthAppointments() {
		return lastMonthAppointments;
	}

	public void setLastMonthAppointments(int lastMonthAppointments) {
		this.lastMonthAppointments = lastMonthAppointments;
	}

	public int getTwoMonthsAgoAppointments() {
		return twoMonthsAgoAppointments;
	}

	public void setTwoMonthsAgoAppointments(int twoMonthsAgoAppointments) {
		this.twoMonthsAgoAppointments = twoMonthsAgoAppointments;
	}

	public int getThisMonthNewContacts() {
		return thisMonthNewContacts;
	}

	public void setThisMonthNewContacts(int thisMonthNewContacts) {
		this.thisMonthNewContacts = thisMonthNewContacts;
	}

	public int getLastMonthNewContacts() {
		return lastMonthNewContacts;
	}

	public void setLastMonthNewContacts(int lastMonthNewContacts) {
		this.lastMonthNewContacts = lastMonthNewContacts;
	}

	public int getTwoMonthsAgoNewContacts() {
		return twoMonthsAgoNewContacts;
	}

	public void setTwoMonthsAgoNewContacts(int twoMonthsAgoNewContacts) {
		this.twoMonthsAgoNewContacts = twoMonthsAgoNewContacts;
	}

	public int getThisMonthNewProspects() {
		return thisMonthNewProspects;
	}

	public void setThisMonthNewProspects(int thisMonthNewProspects) {
		this.thisMonthNewProspects = thisMonthNewProspects;
	}

	public int getLastMonthNewProspects() {
		return lastMonthNewProspects;
	}

	public void setLastMonthNewProspects(int lastMonthNewProspects) {
		this.lastMonthNewProspects = lastMonthNewProspects;
	}

	public int getTwoMonthsAgoNewProspects() {
		return twoMonthsAgoNewProspects;
	}

	public void setTwoMonthsAgoNewProspects(int twoMonthsAgoNewProspects) {
		this.twoMonthsAgoNewProspects = twoMonthsAgoNewProspects;
	}

	public int getThisMonthNewCustomers() {
		return thisMonthNewCustomers;
	}

	public void setThisMonthNewCustomers(int thisMonthNewCustomers) {
		this.thisMonthNewCustomers = thisMonthNewCustomers;
	}

	public int getLastMonthNewCustomers() {
		return lastMonthNewCustomers;
	}

	public void setLastMonthNewCustomers(int lastMonthNewCustomers) {
		this.lastMonthNewCustomers = lastMonthNewCustomers;
	}

	public int getTwoMonthsAgoNewCustomers() {
		return twoMonthsAgoNewCustomers;
	}

	public void setTwoMonthsAgoNewCustomers(int twoMonthsAgoNewCustomers) {
		this.twoMonthsAgoNewCustomers = twoMonthsAgoNewCustomers;
	}
}
