package com.dashboard.dao;

public class AllClientCategories {
	int clientCategoryID;
	String clientCategory;

	public int getClientCategoryID() {
		return clientCategoryID;
	}

	public void setClientCategoryID(int clientCategoryID) {
		this.clientCategoryID = clientCategoryID;
	}

	public String getClientCategory() {
		return clientCategory;
	}

	public void setClientCategory(String clientCategory) {
		this.clientCategory = clientCategory;
	}

}
