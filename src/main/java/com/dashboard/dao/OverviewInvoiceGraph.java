package com.dashboard.dao;

public class OverviewInvoiceGraph {
	String Month;
	double Japanese;
	double NonJapanese;
	double Hotel;
	double BarsAndCafe;
	double Single;
	double Events;

	public String getMonth() {
		return Month;
	}

	public void setMonth(String month) {
		Month = month;
	}

	public double getJapanese() {
		return Japanese;
	}

	public void setJapanese(double japanese) {
		Japanese = japanese;
	}

	public double getNonJapanese() {
		return NonJapanese;
	}

	public void setNonJapanese(double nonJapanese) {
		NonJapanese = nonJapanese;
	}

	public double getHotel() {
		return Hotel;
	}

	public void setHotel(double hotel) {
		Hotel = hotel;
	}

	public double getBarsAndCafe() {
		return BarsAndCafe;
	}

	public void setBarsAndCafe(double barsAndCafe) {
		BarsAndCafe = barsAndCafe;
	}

	public double getSingle() {
		return Single;
	}

	public void setSingle(double single) {
		Single = single;
	}

	public double getEvents() {
		return Events;
	}

	public void setEvents(double events) {
		Events = events;
	}

}
