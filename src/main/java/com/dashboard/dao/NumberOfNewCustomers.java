package com.dashboard.dao;

public class NumberOfNewCustomers {
	int numberOfNewCustomers;

	public int getNumberOfNewCustomers() {
		return numberOfNewCustomers;
	}

	public void setNumberOfNewCustomers(int numberOfNewCustomers) {
		this.numberOfNewCustomers = numberOfNewCustomers;
	}

}
