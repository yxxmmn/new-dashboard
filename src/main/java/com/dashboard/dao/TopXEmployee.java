package com.dashboard.dao;

public class TopXEmployee {

	String empName;
	String empID;
	double monthSales;
	double accumulatedSales;

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpID() {
		return empID;
	}

	public void setEmpID(String empID) {
		this.empID = empID;
	}

	public double getMonthSales() {
		return monthSales;
	}

	public void setMonthSales(double monthSales) {
		this.monthSales = monthSales;
	}

	public double getAccumulatedSales() {
		return accumulatedSales;
	}

	public void setAccumulatedSales(double accumulatedSales) {
		this.accumulatedSales = accumulatedSales;
	}

}
