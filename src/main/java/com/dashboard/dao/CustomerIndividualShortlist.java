package com.dashboard.dao;

public class CustomerIndividualShortlist {
	String sakeBrand;
	int sakeID;
	String sakeName;
	int grade;

	public String getSakeBrand() {
		return sakeBrand;
	}

	public void setSakeBrand(String sakeBrand) {
		this.sakeBrand = sakeBrand;
	}

	public int getSakeID() {
		return sakeID;
	}

	public void setSakeID(int sakeID) {
		this.sakeID = sakeID;
	}

	public String getSakeName() {
		return sakeName;
	}

	public void setSakeName(String sakeName) {
		this.sakeName = sakeName;
	}

	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

}
