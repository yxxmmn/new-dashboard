package com.dashboard.dao;

public class CustomerIndividualBestSellingSake {
	int sakeID;
	String sakeName;
	String sakeBrand;
	double totalAmount;

	public int getSakeID() {
		return sakeID;
	}

	public void setSakeID(int sakeID) {
		this.sakeID = sakeID;
	}

	public String getSakeName() {
		return sakeName;
	}

	public void setSakeName(String sakeName) {
		this.sakeName = sakeName;
	}

	public String getSakeBrand() {
		return sakeBrand;
	}

	public void setSakeBrand(String sakeBrand) {
		this.sakeBrand = sakeBrand;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

}
