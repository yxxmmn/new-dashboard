package com.dashboard.dao;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "")
public class RFMReturnFormat {
	
	String clientName;
	String empInCharge;
	String sakeOrderID;
	String lastDateOrdered;
	String daysSinceLastOrdered;
	String totalBottles;
	String totalInvoiceAmount;
	String customerRFMCategory;
	String clientCategory;
	
	public RFMReturnFormat() {
		
	}
	
	public RFMReturnFormat(String clientName,String clientCategory,String empInCharge,String sakeOrderID,String lastDateOrdered,
			String daysSinceLastOrdered,String totalBottles,String totalInvoiceAmount,String customerRFMCategory) {
		this.clientName = clientName;
		this.clientCategory = clientCategory;
		this.empInCharge = empInCharge;
		this.sakeOrderID = sakeOrderID;
		this.lastDateOrdered = lastDateOrdered;
		this.daysSinceLastOrdered = daysSinceLastOrdered;
		this.totalBottles = totalBottles;
		this.totalInvoiceAmount = totalInvoiceAmount;
		this.customerRFMCategory = customerRFMCategory;
	}
	
	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getEmpInCharge() {
		return empInCharge;
	}

	public void setEmpInCharge(String empInCharge) {
		this.empInCharge = empInCharge;
	}

	public String getSakeOrderID() {
		return sakeOrderID;
	}

	public void setSakeOrderID(String sakeOrderID) {
		this.sakeOrderID = sakeOrderID;
	}

	public String getLastDateOrdered() {
		return lastDateOrdered;
	}

	public void setLastDateOrdered(String lastDateOrdered) {
		this.lastDateOrdered = lastDateOrdered;
	}

	public String getDaysSinceLastOrdered() {
		return daysSinceLastOrdered;
	}

	public void setDaysSinceLastOrdered(String daysSinceLastOrdered) {
		this.daysSinceLastOrdered = daysSinceLastOrdered;
	}

	public String getTotalBottles() {
		return totalBottles;
	}

	public void setTotalBottles(String totalBottles) {
		this.totalBottles = totalBottles;
	}

	public String getTotalInvoiceAmount() {
		return totalInvoiceAmount;
	}

	public void setTotalInvoiceAmount(String totalInvoiceAmount) {
		this.totalInvoiceAmount = totalInvoiceAmount;
	}

	public String getCustomerRFMCategory() {
		return customerRFMCategory;
	}

	public void setCustomerRFMCategory(String customerRFMCategory) {
		this.customerRFMCategory = customerRFMCategory;
	}

	public String getClientCategory() {
		return clientCategory;
	}

	public void setClientCategory(String clientCategory) {
		this.clientCategory = clientCategory;
	}

}
