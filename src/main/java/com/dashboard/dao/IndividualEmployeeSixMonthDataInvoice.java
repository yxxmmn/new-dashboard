package com.dashboard.dao;

public class IndividualEmployeeSixMonthDataInvoice {
	int empID;
	double thisMonthInvoice;
	double lastMonthInvoice;
	double twoMonthsAgoInvoice;
	double threeMonthsAgoInvoice;
	double fourMonthsAgoInvoice;
	double fiveMonthsAgoInvoice;
	int thisMonth;
	int thisMonthYear;
	int lastMonth;
	int lastMonthYear;
	int twoMonthsAgo;
	int twoMonthsAgoYear;
	int threeMonthsAgo;
	int threeMonthsAgoYear;
	int fourMonthsAgo;
	int fourMonthsAgoYear;
	int fiveMonthsAgo;
	int fiveMonthsAgoYear;

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public double getThisMonthInvoice() {
		return thisMonthInvoice;
	}

	public void setThisMonthInvoice(double thisMonthInvoice) {
		this.thisMonthInvoice = thisMonthInvoice;
	}

	public double getLastMonthInvoice() {
		return lastMonthInvoice;
	}

	public void setLastMonthInvoice(double lastMonthInvoice) {
		this.lastMonthInvoice = lastMonthInvoice;
	}

	public double getTwoMonthsAgoInvoice() {
		return twoMonthsAgoInvoice;
	}

	public void setTwoMonthsAgoInvoice(double twoMonthsAgoInvoice) {
		this.twoMonthsAgoInvoice = twoMonthsAgoInvoice;
	}

	public double getThreeMonthsAgoInvoice() {
		return threeMonthsAgoInvoice;
	}

	public void setThreeMonthsAgoInvoice(double threeMonthsAgoInvoice) {
		this.threeMonthsAgoInvoice = threeMonthsAgoInvoice;
	}

	public double getFourMonthsAgoInvoice() {
		return fourMonthsAgoInvoice;
	}

	public void setFourMonthsAgoInvoice(double fourMonthsAgoInvoice) {
		this.fourMonthsAgoInvoice = fourMonthsAgoInvoice;
	}

	public double getFiveMonthsAgoInvoice() {
		return fiveMonthsAgoInvoice;
	}

	public void setFiveMonthsAgoInvoice(double fiveMonthsAgoInvoice) {
		this.fiveMonthsAgoInvoice = fiveMonthsAgoInvoice;
	}

	public int getThisMonth() {
		return thisMonth;
	}

	public void setThisMonth(int thisMonth) {
		this.thisMonth = thisMonth;
	}

	public int getThisMonthYear() {
		return thisMonthYear;
	}

	public void setThisMonthYear(int thisMonthYear) {
		this.thisMonthYear = thisMonthYear;
	}

	public int getLastMonth() {
		return lastMonth;
	}

	public void setLastMonth(int lastMonth) {
		this.lastMonth = lastMonth;
	}

	public int getLastMonthYear() {
		return lastMonthYear;
	}

	public void setLastMonthYear(int lastMonthYear) {
		this.lastMonthYear = lastMonthYear;
	}

	public int getTwoMonthsAgo() {
		return twoMonthsAgo;
	}

	public void setTwoMonthsAgo(int twoMonthsAgo) {
		this.twoMonthsAgo = twoMonthsAgo;
	}

	public int getTwoMonthsAgoYear() {
		return twoMonthsAgoYear;
	}

	public void setTwoMonthsAgoYear(int twoMonthsAgoYear) {
		this.twoMonthsAgoYear = twoMonthsAgoYear;
	}

	public int getThreeMonthsAgo() {
		return threeMonthsAgo;
	}

	public void setThreeMonthsAgo(int threeMonthsAgo) {
		this.threeMonthsAgo = threeMonthsAgo;
	}

	public int getThreeMonthsAgoYear() {
		return threeMonthsAgoYear;
	}

	public void setThreeMonthsAgoYear(int threeMonthsAgoYear) {
		this.threeMonthsAgoYear = threeMonthsAgoYear;
	}

	public int getFourMonthsAgo() {
		return fourMonthsAgo;
	}

	public void setFourMonthsAgo(int fourMonthsAgo) {
		this.fourMonthsAgo = fourMonthsAgo;
	}

	public int getFourMonthsAgoYear() {
		return fourMonthsAgoYear;
	}

	public void setFourMonthsAgoYear(int fourMonthsAgoYear) {
		this.fourMonthsAgoYear = fourMonthsAgoYear;
	}

	public int getFiveMonthsAgo() {
		return fiveMonthsAgo;
	}

	public void setFiveMonthsAgo(int fiveMonthsAgo) {
		this.fiveMonthsAgo = fiveMonthsAgo;
	}

	public int getFiveMonthsAgoYear() {
		return fiveMonthsAgoYear;
	}

	public void setFiveMonthsAgoYear(int fiveMonthsAgoYear) {
		this.fiveMonthsAgoYear = fiveMonthsAgoYear;
	}

}
