package com.dashboard.dao;

public class NumberOfAppointments {
	int numberOfAppointments;

	public int getNumberOfAppointments() {
		return numberOfAppointments;
	}

	public void setNumberOfAppointments(int numberOfAppointments) {
		this.numberOfAppointments = numberOfAppointments;
	}

}
