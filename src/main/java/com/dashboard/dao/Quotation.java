package com.dashboard.dao;

public class Quotation {

	int quotationID;
	int customerID;
	int empID;
	String dateIssued;
	double quotationAmount;

	public int getQuotationID() {
		return quotationID;
	}

	public void setQuotationID(int quotationID) {
		this.quotationID = quotationID;
	}

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public String getDateIssued() {
		return dateIssued;
	}

	public void setDateIssued(String dateIssued) {
		this.dateIssued = dateIssued;
	}

	public double getQuotationAmount() {
		return quotationAmount;
	}

	public void setQuotationAmount(double quotationAmount) {
		this.quotationAmount = quotationAmount;
	}

}
