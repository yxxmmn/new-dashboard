package com.dashboard.dao;

import java.sql.Date;

public class RFMGetDetailsByIDClass {

	Date lastDateOrdered;
	int totalBottles;
	int totalInvoiceAmount;
	Date clientDateBecomeCustomer;
	String clientCategory;
	String clientName;
	String empInCharge;
	String sakeOrderID;
	
	//Getters
	public Date getLastDateOrdered() {
		return lastDateOrdered;
	}
	public int getTotalBottles() {
		return totalBottles;
	}
	public int getTotalInvoiceAmount() {
		return totalInvoiceAmount;
	}
	public Date getClientDateBecomeCustomer() {
		return clientDateBecomeCustomer;
	}
	public String getClientCategory() {
		return clientCategory;
	}
	public String getEmpInCharge() {
		return empInCharge;
	}
	public String getSakeOrderID() {
		return sakeOrderID;
	}
	public String getClientName() {
		return clientName;
	}
	
	//Setters
	public void setLastDateOrdered(Date lastDateOrdered) {
		this.lastDateOrdered = lastDateOrdered;
	}
	public void setTotalBottles(int totalBottles) {
		this.totalBottles = totalBottles;
	}
	public void setTotalInvoiceAmount(int totalInvoiceAmount) {
		this.totalInvoiceAmount = totalInvoiceAmount;
	}
	public void setClientDateBecomeCustomer(Date clientDateBecomeCustomer) {
		this.clientDateBecomeCustomer = clientDateBecomeCustomer;
	}
	public void setClientCategory(String clientCategory) {
		this.clientCategory = clientCategory;
	}
	public void setEmpInCharge(String empInCharge) {
		this.empInCharge = empInCharge;
	}
	public void setSakeOrderID(String sakeOrderID) {
		this.sakeOrderID = sakeOrderID;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	
}
