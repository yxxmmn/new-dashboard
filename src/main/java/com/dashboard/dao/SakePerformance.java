package com.dashboard.dao;

public class SakePerformance {
	String sakeName;
	String sakeBrand;
	int quantitySold;
	double sakeSales;
	String clientCategory;

	public String getSakeName() {
		return sakeName;
	}

	public void setSakeName(String sakeName) {
		this.sakeName = sakeName;
	}

	public String getSakeBrand() {
		return sakeBrand;
	}

	public void setSakeBrand(String sakeBrand) {
		this.sakeBrand = sakeBrand;
	}

	public int getQuantitySold() {
		return quantitySold;
	}

	public void setQuantitySold(int quantitySold) {
		this.quantitySold = quantitySold;
	}

	public double getSakeSales() {
		return sakeSales;
	}

	public void setSakeSales(double sakeSales) {
		this.sakeSales = sakeSales;
	}

	public String getClientCategory() {
		return clientCategory;
	}

	public void setClientCategory(String clientCategory) {
		this.clientCategory = clientCategory;
	}

}
