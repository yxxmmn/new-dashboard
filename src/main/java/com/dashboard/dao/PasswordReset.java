package com.dashboard.dao;

public class PasswordReset {
	int empID;
	String password;
	String loginID;
	String employeeEmail;

	public String getEmployeeEmail() {
		return employeeEmail;
	}

	public void setEmployeeEmail(String employeeEmail) {
		this.employeeEmail = employeeEmail;
	}

	public PasswordReset() {
	}

	public PasswordReset(String loginID, String employeeEmail) {
		// TODO Auto-generated constructor stub
		super();
		this.loginID = loginID;
		this.employeeEmail = employeeEmail;
	}

	public PasswordReset(int empID, String password) {
		super();
		this.empID = empID;
		this.password = password;
	}

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLoginID() {
		return loginID;
	}

	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}

}
