package com.dashboard.dao;

public class Sake {

	int sakeID;
	String sakeBrand;
	String sakeName;
	String sakeGrade;
	String sakeImage;
	String sakeDescription;
	String skuNumber;
	double sakeRetailPrice;
	double sakeTradePrice;
	double sakeWholesalePrice;
	int sakeCapacity;
	int sakePolish;
	String sakeRice;
	int sakeAcidity;
	double sakeAlcoholContent;
	int sakeSMV;
	int sakeStock;
	String dataURI;

	public String getDataURI() {
		return dataURI;
	}

	public void setDataURI(String dataURI) {
		this.dataURI = dataURI;
	}

	public int getSakeStock() {
		return sakeStock;
	}

	public void setSakeStock(int sakeStock) {
		this.sakeStock = sakeStock;
	}

	public Sake() {
	}

	public Sake(int sakeID, String sakeBrand, String sakeName, String sakeGrade, String sakeImage,
			String sakeDescription, String skuNumber, double sakeRetailPrice, double sakeTradePrice,
			double sakeWholesalePrice, int sakeCapacity, int sakePolish, String sakeRice, int sakeAcidity,
			double sakeAlcoholContent, int sakeSMV, String dataURI) {
		super();
		this.sakeID = sakeID;
		this.sakeBrand = sakeBrand;
		this.sakeName = sakeName;
		this.sakeGrade = sakeGrade;
		this.sakeImage = sakeImage;
		this.sakeDescription = sakeDescription;
		this.skuNumber = skuNumber;
		this.sakeName = sakeName;
		this.sakeRetailPrice = sakeRetailPrice;
		this.sakeTradePrice = sakeTradePrice;
		this.sakeWholesalePrice = sakeWholesalePrice;
		this.sakeCapacity = sakeCapacity;
		this.sakePolish = sakePolish;
		this.sakeRice = sakeRice;
		this.sakeAcidity = sakeAcidity;
		this.sakeAlcoholContent = sakeAlcoholContent;
		this.sakeSMV = sakeSMV;
		this.dataURI = dataURI;

	}

	public Sake(String sakeBrand, String sakeName, String sakeGrade, String sakeDescription, String skuNumber,
			double sakeRetailPrice, double sakeTradePrice, int sakeStock, double sakeWholesalePrice, int sakeCapacity,
			int sakePolish, String sakeRice, int sakeAcidity, double sakeAlcoholContent, int sakeSMV, String dataURI) {
		super();
		this.sakeBrand = sakeBrand;
		this.sakeName = sakeName;
		this.sakeGrade = sakeGrade;
		this.sakeDescription = sakeDescription;
		this.skuNumber = skuNumber;
		this.sakeName = sakeName;
		this.sakeRetailPrice = sakeRetailPrice;
		this.sakeTradePrice = sakeTradePrice;
		this.sakeWholesalePrice = sakeWholesalePrice;
		this.sakeCapacity = sakeCapacity;
		this.sakePolish = sakePolish;
		this.sakeRice = sakeRice;
		this.sakeAcidity = sakeAcidity;
		this.sakeAlcoholContent = sakeAlcoholContent;
		this.sakeSMV = sakeSMV;
		this.sakeStock = sakeStock;
		this.dataURI = dataURI;
	}

	public int getSakeID() {
		return sakeID;
	}

	public void setSakeID(int sakeID) {
		this.sakeID = sakeID;
	}

	public String getSakeBrand() {
		return sakeBrand;
	}

	public void setSakeBrand(String sakeBrand) {
		this.sakeBrand = sakeBrand;
	}

	public String getSakeName() {
		return sakeName;
	}

	public void setSakeName(String sakeName) {
		this.sakeName = sakeName;
	}

	public String getSakeGrade() {
		return sakeGrade;
	}

	public void setSakeGrade(String sakeGrade) {
		this.sakeGrade = sakeGrade;
	}

	public String getSakeImage() {
		return sakeImage;
	}

	public void setSakeImage(String sakeImage) {
		this.sakeImage = sakeImage;
	}

	public String getSakeDescription() {
		return sakeDescription;
	}

	public void setSakeDescription(String sakeDescription) {
		this.sakeDescription = sakeDescription;
	}

	public String getSkuNumber() {
		return skuNumber;
	}

	public void setSkuNumber(String skuNumber) {
		this.skuNumber = skuNumber;
	}

	public double getSakeRetailPrice() {
		return sakeRetailPrice;
	}

	public void setSakeRetailPrice(double sakeRetailPrice) {
		this.sakeRetailPrice = sakeRetailPrice;
	}

	public double getSakeTradePrice() {
		return sakeTradePrice;
	}

	public void setSakeTradePrice(double sakeTradePrice) {
		this.sakeTradePrice = sakeTradePrice;
	}

	public double getSakeWholesalePrice() {
		return sakeWholesalePrice;
	}

	public void setSakeWholesalePrice(double sakeWholesalePrice) {
		this.sakeWholesalePrice = sakeWholesalePrice;
	}

	public int getSakeCapacity() {
		return sakeCapacity;
	}

	public void setSakeCapacity(int sakeCapacity) {
		this.sakeCapacity = sakeCapacity;
	}

	public int getSakePolish() {
		return sakePolish;
	}

	public void setSakePolish(int sakePolish) {
		this.sakePolish = sakePolish;
	}

	public String getSakeRice() {
		return sakeRice;
	}

	public void setSakeRice(String sakeRice) {
		this.sakeRice = sakeRice;
	}

	public int getSakeAcidity() {
		return sakeAcidity;
	}

	public void setSakeAcidity(int sakeAcidity) {
		this.sakeAcidity = sakeAcidity;
	}

	public double getSakeAlcoholContent() {
		return sakeAlcoholContent;
	}

	public void setSakeAlcoholContent(double sakeAlcoholContent) {
		this.sakeAlcoholContent = sakeAlcoholContent;
	}

	public int getSakeSMV() {
		return sakeSMV;
	}

	public void setSakeSMV(int sakeSMV) {
		this.sakeSMV = sakeSMV;
	}
	

}
