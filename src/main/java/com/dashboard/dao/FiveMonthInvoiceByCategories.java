package com.dashboard.dao;

public class FiveMonthInvoiceByCategories {
	String clientCategory;

	public String getClientCategory() {
		return clientCategory;
	}

	public void setClientCategory(String clientCategory) {
		this.clientCategory = clientCategory;
	}

	double thisMonthInvoice;
	double lastMonthInvoice;
	double twoMonthsAgoInvoice;
	double threeMonthsAgoInvoice;
	double fourMonthsAgoInvoice;
	int thisMonthYear;
	int thisMonth;
	int lastMonthYear;
	int lastMonth;
	int twoMonthsAgoYear;
	int twoMonthsAgo;
	int threeMonthsAgoYear;
	int threeMonthsAgo;
	int fourMonthsAgoYear;
	int fourMonthsAgo;

	public double getThreeMonthsAgoInvoice() {
		return threeMonthsAgoInvoice;
	}

	public void setThreeMonthsAgoInvoice(double threeMonthsAgoInvoice) {
		this.threeMonthsAgoInvoice = threeMonthsAgoInvoice;
	}

	public double getFourMonthsAgoInvoice() {
		return fourMonthsAgoInvoice;
	}

	public void setFourMonthsAgoInvoice(double fourMonthsAgoInvoice) {
		this.fourMonthsAgoInvoice = fourMonthsAgoInvoice;
	}

	public int getThreeMonthsAgoYear() {
		return threeMonthsAgoYear;
	}

	public void setThreeMonthsAgoYear(int threeMonthsAgoYear) {
		this.threeMonthsAgoYear = threeMonthsAgoYear;
	}

	public int getThreeMonthsAgo() {
		return threeMonthsAgo;
	}

	public void setThreeMonthsAgo(int threeMonthsAgo) {
		this.threeMonthsAgo = threeMonthsAgo;
	}

	public int getFourMonthsAgoYear() {
		return fourMonthsAgoYear;
	}

	public void setFourMonthsAgoYear(int fourMonthsAgoYear) {
		this.fourMonthsAgoYear = fourMonthsAgoYear;
	}

	public int getFourMonthsAgo() {
		return fourMonthsAgo;
	}

	public void setFourMonthsAgo(int fourMonthsAgo) {
		this.fourMonthsAgo = fourMonthsAgo;
	}

	public double getThisMonthInvoice() {
		return thisMonthInvoice;
	}

	public void setThisMonthInvoice(double thisMonthInvoice) {
		this.thisMonthInvoice = thisMonthInvoice;
	}

	public double getLastMonthInvoice() {
		return lastMonthInvoice;
	}

	public void setLastMonthInvoice(double lastMonthInvoice) {
		this.lastMonthInvoice = lastMonthInvoice;
	}

	public double getTwoMonthsAgoInvoice() {
		return twoMonthsAgoInvoice;
	}

	public void setTwoMonthsAgoInvoice(double twoMonthsAgoInvoice) {
		this.twoMonthsAgoInvoice = twoMonthsAgoInvoice;
	}

	public int getThisMonthYear() {
		return thisMonthYear;
	}

	public void setThisMonthYear(int thisMonthYear) {
		this.thisMonthYear = thisMonthYear;
	}

	public int getThisMonth() {
		return thisMonth;
	}

	public void setThisMonth(int thisMonth) {
		this.thisMonth = thisMonth;
	}

	public int getLastMonthYear() {
		return lastMonthYear;
	}

	public void setLastMonthYear(int lastMonthYear) {
		this.lastMonthYear = lastMonthYear;
	}

	public int getLastMonth() {
		return lastMonth;
	}

	public void setLastMonth(int lastMonth) {
		this.lastMonth = lastMonth;
	}

	public int getTwoMonthsAgoYear() {
		return twoMonthsAgoYear;
	}

	public void setTwoMonthsAgoYear(int twoMonthsAgoYear) {
		this.twoMonthsAgoYear = twoMonthsAgoYear;
	}

	public int getTwoMonthsAgo() {
		return twoMonthsAgo;
	}

	public void setTwoMonthsAgo(int twoMonthsAgo) {
		this.twoMonthsAgo = twoMonthsAgo;
	}

}
