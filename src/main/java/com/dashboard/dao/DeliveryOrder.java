package com.dashboard.dao;

public class DeliveryOrder {

	int doID;
	int customerID;
	int empID;
	int doStatus;

	public int getDoID() {
		return doID;
	}

	public void setDoID(int doID) {
		this.doID = doID;
	}

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public int getDoStatus() {
		return doStatus;
	}

	public void setDoStatus(int doStatus) {
		this.doStatus = doStatus;
	}

}
