package com.dashboard.dao;

public class IndividualEmployeeThreeMonthDataInvoice {
	int empID;
	double thisMonthInvoice;
	double lastMonthInvoice;
	double twoMonthsAgoInvoice;
	int thisMonth;
	int thisMonthYear;
	int lastMonth;
	int lastMonthYear;
	int twoMonthsAgo;
	int twoMonthsAgoYear;

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public double getThisMonthInvoice() {
		return thisMonthInvoice;
	}

	public void setThisMonthInvoice(double thisMonthInvoice) {
		this.thisMonthInvoice = thisMonthInvoice;
	}

	public double getLastMonthInvoice() {
		return lastMonthInvoice;
	}

	public void setLastMonthInvoice(double lastMonthInvoice) {
		this.lastMonthInvoice = lastMonthInvoice;
	}

	public double getTwoMonthsAgoInvoice() {
		return twoMonthsAgoInvoice;
	}

	public void setTwoMonthsAgoInvoice(double twoMonthsAgoInvoice) {
		this.twoMonthsAgoInvoice = twoMonthsAgoInvoice;
	}

	public int getThisMonth() {
		return thisMonth;
	}

	public void setThisMonth(int thisMonth) {
		this.thisMonth = thisMonth;
	}

	public int getThisMonthYear() {
		return thisMonthYear;
	}

	public void setThisMonthYear(int thisMonthYear) {
		this.thisMonthYear = thisMonthYear;
	}

	public int getLastMonth() {
		return lastMonth;
	}

	public void setLastMonth(int lastMonth) {
		this.lastMonth = lastMonth;
	}

	public int getLastMonthYear() {
		return lastMonthYear;
	}

	public void setLastMonthYear(int lastMonthYear) {
		this.lastMonthYear = lastMonthYear;
	}

	public int getTwoMonthsAgo() {
		return twoMonthsAgo;
	}

	public void setTwoMonthsAgo(int twoMonthsAgo) {
		this.twoMonthsAgo = twoMonthsAgo;
	}

	public int getTwoMonthsAgoYear() {
		return twoMonthsAgoYear;
	}

	public void setTwoMonthsAgoYear(int twoMonthsAgoYear) {
		this.twoMonthsAgoYear = twoMonthsAgoYear;
	}

}
