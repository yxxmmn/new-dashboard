package com.dashboard.dao;

public class AllProspects {
	int customerID;
	String prospectName;
	String customerCategory;
	String empName;
	String billingAddress;
	String shippingAddress;
	String appointmentDate;
	String customerEmail;
	int contactnumber;
	int empID;

	public AllProspects() {
	}

	public AllProspects(int customerID, int empID, String prospectName, String customerCategory, String billingAddress,
			String shippingAddress, String customerEmail, int contactnumber) {
		this.customerID = customerID;
		this.empID = empID;
		this.prospectName = prospectName;
		this.customerCategory = customerCategory;
		this.billingAddress = billingAddress;
		this.shippingAddress = shippingAddress;
		this.customerEmail = customerEmail;
		this.contactnumber = contactnumber;
	}

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public int getContactnumber() {
		return contactnumber;
	}

	public void setContactnumber(int contactnumber) {
		this.contactnumber = contactnumber;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public String getProspectName() {
		return prospectName;
	}

	public void setProspectName(String prospectName) {
		this.prospectName = prospectName;
	}

	public String getCustomerCategory() {
		return customerCategory;
	}

	public void setCustomerCategory(String customerCategory) {
		this.customerCategory = customerCategory;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public String getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

}
