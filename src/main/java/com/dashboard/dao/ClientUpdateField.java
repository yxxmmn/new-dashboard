package com.dashboard.dao;

public class ClientUpdateField {
	int customerID;
	int empID;
	String name;
	String customerCategory;
	String billingAddress;
	String shippingAddress;
	String customerEmail;
	int contactNumber;

	public ClientUpdateField() {
	}

	public ClientUpdateField(int customerID, int empID, String name, String customerCategory, String billingAddress,
			String shippingAddress, String customerEmail, int contactnumber) {
		this.customerID = customerID;
		this.empID = empID;
		this.name = name;
		this.customerCategory = customerCategory;
		this.billingAddress = billingAddress;
		this.shippingAddress = shippingAddress;
		this.customerEmail = customerEmail;
		this.contactNumber = contactnumber;
	}

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCustomerCategory() {
		return customerCategory;
	}

	public void setCustomerCategory(String customerCategory) {
		this.customerCategory = customerCategory;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public int getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(int contactNumber) {
		this.contactNumber = contactNumber;
	}


}
