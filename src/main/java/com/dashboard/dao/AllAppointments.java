package com.dashboard.dao;

public class AllAppointments {
	String appointmentID;
	String appointmentLocation;
	String appointmentDate;
	int clientID;
	String clientName;
	int empID;
	String empName;
	String appointmentNotes;
	String clientType;
	String POC1name;
	String POC1email;
	String POC1ContactNumber;
	String POC2name;
	String POC2email;
	String POC2ContactNumber;
	String appointmentStatus;
	String clientCategory;

	public AllAppointments() {

	}

	public AllAppointments(String appointmentID, String appointmentLocation, String appointmentDate, int clientID,
			String clientName, int empID, String empName, String appointmentNotes, String clientType, String POC1name,
			String POC1email, String POC1ContactNumber, String POC2name, String POC2email, String POC2ContactNumber,
			String appointmentStatus, String clientCategory) {
		super();
		this.appointmentID = appointmentID;
		this.appointmentLocation = appointmentLocation;
		this.appointmentDate = appointmentDate;
		this.clientID = clientID;
		this.clientName = clientName;
		this.empID = empID;
		this.empName = empName;
		this.appointmentNotes = appointmentNotes;
		this.clientType = clientType;
		this.POC1name = POC1name;
		this.POC1email = POC1email;
		this.POC1ContactNumber = POC1ContactNumber;
		this.POC2name = POC2name;
		this.POC2email = POC2email;
		this.POC2ContactNumber = POC2ContactNumber;
		this.appointmentStatus = appointmentStatus;
		this.clientCategory = clientCategory;
	}

	public AllAppointments(int clientID, String clientName, String clientCategory, String appointmentDate,
			String appointmentID) {
		this.appointmentID = appointmentID;
		this.appointmentDate = appointmentDate;
		this.clientID = clientID;
		this.clientName = clientName;
		this.clientCategory = clientCategory;
	}

	public AllAppointments(int empID, String empName, String appointmentDate, String appointmentID) {
		this.appointmentID = appointmentID;
		this.appointmentDate = appointmentDate;
		this.empID = empID;
		this.empName = empName;
	}

	public String getAppointmentID() {
		return appointmentID;
	}

	public void setAppointmentID(String appointmentID) {
		this.appointmentID = appointmentID;
	}

	public String getAppointmentLocation() {
		return appointmentLocation;
	}

	public void setAppointmentLocation(String appointmentLocation) {
		this.appointmentLocation = appointmentLocation;
	}

	public String getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public int getClientID() {
		return clientID;
	}

	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getAppointmentNotes() {
		return appointmentNotes;
	}

	public void setAppointmentNotes(String appointmentNotes) {
		this.appointmentNotes = appointmentNotes;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	public String getPOC1name() {
		return POC1name;
	}

	public void setPOC1name(String pOC1name) {
		POC1name = pOC1name;
	}

	public String getPOC1email() {
		return POC1email;
	}

	public void setPOC1email(String pOC1email) {
		POC1email = pOC1email;
	}

	public String getPOC1ContactNumber() {
		return POC1ContactNumber;
	}

	public void setPOC1ContactNumber(String pOC1ContactNumber) {
		POC1ContactNumber = pOC1ContactNumber;
	}

	public String getPOC2name() {
		return POC2name;
	}

	public void setPOC2name(String pOC2name) {
		POC2name = pOC2name;
	}

	public String getPOC2email() {
		return POC2email;
	}

	public void setPOC2email(String pOC2email) {
		POC2email = pOC2email;
	}

	public String getPOC2ContactNumber() {
		return POC2ContactNumber;
	}

	public void setPOC2ContactNumber(String pOC2ContactNumber) {
		POC2ContactNumber = pOC2ContactNumber;
	}

	public String getAppointmentStatus() {
		return appointmentStatus;
	}

	public void setAppointmentStatus(String appointmentStatus) {
		this.appointmentStatus = appointmentStatus;
	}

	public String getClientCategory() {
		return clientCategory;
	}

	public void setClientCategory(String clientCategory) {
		this.clientCategory = clientCategory;
	}

}
