package com.dashboard.dao;

public class InvoiceGraph {
	String dateInvoiceIssued;
	double invoiceAmount;
	String clientCategory;

	public String getClientCategory() {
		return clientCategory;
	}

	public void setClientCategory(String clientCategory) {
		this.clientCategory = clientCategory;
	}

	public String getDateInvoiceIssued() {
		return dateInvoiceIssued;
	}

	public void setDateInvoiceIssued(String dateInvoiceIssued) {
		this.dateInvoiceIssued = dateInvoiceIssued;
	}

	public double getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
}
