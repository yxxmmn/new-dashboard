package com.dashboard.dao;

public class NumberOfInvoiceWithComparison {
	int numberOfInvoiceThisMonth;
	int numberOfInvoiceLastMonth;
	double percentage;

	public int getNumberOfInvoiceThisMonth() {
		return numberOfInvoiceThisMonth;
	}

	public void setNumberOfInvoiceThisMonth(int numberOfInvoiceThisMonth) {
		this.numberOfInvoiceThisMonth = numberOfInvoiceThisMonth;
	}

	public int getNumberOfInvoiceLastMonth() {
		return numberOfInvoiceLastMonth;
	}

	public void setNumberOfInvoiceLastMonth(int numberOfInvoiceLastMonth) {
		this.numberOfInvoiceLastMonth = numberOfInvoiceLastMonth;
	}

	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}

}
