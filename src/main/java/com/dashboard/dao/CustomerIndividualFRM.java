package com.dashboard.dao;

public class CustomerIndividualFRM {
	String mostRecent;
	int frequency;
	double invoiceAmount;

	public String getMostRecent() {
		return mostRecent;
	}

	public void setMostRecent(String mostRecent) {
		this.mostRecent = mostRecent;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public double getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

}
