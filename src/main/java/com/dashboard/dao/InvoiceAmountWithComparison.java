package com.dashboard.dao;

public class InvoiceAmountWithComparison {
	double thisMonthInvoice;
	double pastMonthInvoice;
	double percentage;

	public double getThisMonthInvoice() {
		return thisMonthInvoice;
	}

	public void setThisMonthInvoice(double thisMonthInvoice) {
		this.thisMonthInvoice = thisMonthInvoice;
	}

	public double getPastMonthInvoice() {
		return pastMonthInvoice;
	}

	public void setPastMonthInvoice(double pastMonthInvoice) {
		this.pastMonthInvoice = pastMonthInvoice;
	}

	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}

}
