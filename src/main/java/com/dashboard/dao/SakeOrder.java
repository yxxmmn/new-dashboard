package com.dashboard.dao;

public class SakeOrder {

	int orderID;
	int sakeID;
	int sakeQuantity;
	int customerID;
	int empID;
	double sakePrice;
	String dateOrdered;

	public int getOrderID() {
		return orderID;
	}

	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	public int getSakeID() {
		return sakeID;
	}

	public void setSakeID(int sakeID) {
		this.sakeID = sakeID;
	}

	public int getSakeQuantity() {
		return sakeQuantity;
	}

	public void setSakeQuantity(int sakeQuantity) {
		this.sakeQuantity = sakeQuantity;
	}

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public double getSakePrice() {
		return sakePrice;
	}

	public void setSakePrice(double sakePrice) {
		this.sakePrice = sakePrice;
	}

	public String getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(String dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

}
