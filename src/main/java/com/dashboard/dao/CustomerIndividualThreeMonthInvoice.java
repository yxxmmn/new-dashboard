package com.dashboard.dao;

public class CustomerIndividualThreeMonthInvoice {
	int customerID;
	int thisMonthYear;
	int thisMonth;
	int lastMonthYear;
	int lastMonth;
	int twoMonthsAgoYear;
	int twoMonthsAgo;
	double thisMonthInvoice;
	double lastMonthInvoice;
	double twoMonthsAgoInvoice;

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public int getThisMonthYear() {
		return thisMonthYear;
	}

	public void setThisMonthYear(int thisMonthYear) {
		this.thisMonthYear = thisMonthYear;
	}

	public int getThisMonth() {
		return thisMonth;
	}

	public void setThisMonth(int thisMonth) {
		this.thisMonth = thisMonth;
	}

	public int getLastMonthYear() {
		return lastMonthYear;
	}

	public void setLastMonthYear(int lastMonthYear) {
		this.lastMonthYear = lastMonthYear;
	}

	public int getLastMonth() {
		return lastMonth;
	}

	public void setLastMonth(int lastMonth) {
		this.lastMonth = lastMonth;
	}

	public int getTwoMonthsAgoYear() {
		return twoMonthsAgoYear;
	}

	public void setTwoMonthsAgoYear(int twoMonthsAgoYear) {
		this.twoMonthsAgoYear = twoMonthsAgoYear;
	}

	public int getTwoMonthsAgo() {
		return twoMonthsAgo;
	}

	public void setTwoMonthsAgo(int twoMonthsAgo) {
		this.twoMonthsAgo = twoMonthsAgo;
	}

	public double getThisMonthInvoice() {
		return thisMonthInvoice;
	}

	public void setThisMonthInvoice(double thisMonthInvoice) {
		this.thisMonthInvoice = thisMonthInvoice;
	}

	public double getLastMonthInvoice() {
		return lastMonthInvoice;
	}

	public void setLastMonthInvoice(double lastMonthInvoice) {
		this.lastMonthInvoice = lastMonthInvoice;
	}

	public double getTwoMonthsAgoInvoice() {
		return twoMonthsAgoInvoice;
	}

	public void setTwoMonthsAgoInvoice(double twoMonthsAgoInvoice) {
		this.twoMonthsAgoInvoice = twoMonthsAgoInvoice;
	}

}
