package com.dashboard.dao;

public class Appointment {

	String appointmentID;
	int empID;
	String empName;
	int clientID;
	String clientName;
	String appointmentLocation;
	String appointmentDate;
	String appointmentNotes;
	String appointmentStatus;
	int appointmentStatusID;

	public int getAppointmentStatusID() {
		return appointmentStatusID;
	}

	public void setAppointmentStatusID(int appointmentStatusID) {
		this.appointmentStatusID = appointmentStatusID;
	}

	public Appointment() {
	}

	public Appointment(String appointmentID, int empID, String empName, int clientID, String clientName,
			String appointmentLocation, String appointmentDate, String notes) {

		this.appointmentID = appointmentID;
		this.empID = empID;
		this.clientID = clientID;
		this.clientName = clientName;
		this.appointmentLocation = appointmentLocation;
		this.appointmentDate = appointmentDate;
		this.appointmentNotes = notes;
		this.empName = empName;

	}

	public Appointment(String appointmentID, int empID, int clientID, String appointmentLocation,
			String appointmentDate, String notes, String appointmentStatus) {

		this.appointmentID = appointmentID;
		this.empID = empID;
		this.clientID = clientID;
		this.appointmentLocation = appointmentLocation;
		this.appointmentDate = appointmentDate;
		this.appointmentNotes = notes;
		this.appointmentStatus = appointmentStatus;

	}

	public Appointment(String appointmentID, int empID, int clientID, String appointmentLocation,
			String appointmentDate, String notes, int appointmentStatusID) {

		this.appointmentID = appointmentID;
		this.empID = empID;
		this.clientID = clientID;
		this.appointmentLocation = appointmentLocation;
		this.appointmentDate = appointmentDate;
		this.appointmentNotes = notes;
		this.appointmentStatusID = appointmentStatusID;

	}

	public String getAppointmentID() {
		return appointmentID;
	}

	public void setAppointmentID(String appointmentID) {
		this.appointmentID = appointmentID;
	}

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public int getClientID() {
		return clientID;
	}

	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getAppointmentLocation() {
		return appointmentLocation;
	}

	public void setAppointmentLocation(String appointmentLocation) {
		this.appointmentLocation = appointmentLocation;
	}

	public String getAppointmentStatus() {
		return appointmentStatus;
	}

	public void setAppointmentStatus(String appointmentStatus) {
		this.appointmentStatus = appointmentStatus;
	}

	public String getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public String getAppointmentNotes() {
		return appointmentNotes;
	}

	public void setAppointmentNotes(String appointmentNotes) {
		this.appointmentNotes = appointmentNotes;
	}

}
