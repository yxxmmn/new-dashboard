package com.dashboard.dao;

public class ListOfPurchase {
	String appointmentID;
	int sakeOrderID;
	int sakeID;
	String sakeName;
	int sakeQuantity;
	int clientID;
	String clientName;
	int empID;
	String empName;
	String dateOrdered;
	String priceType;
	double discount;
	double totalPriceBeforeDiscount;
	double totalPriceAfterDiscount;
	double pricePerSake;

	public double getPricePerSake() {
		return pricePerSake;
	}

	public void setPricePerSake(double pricePerSake) {
		this.pricePerSake = pricePerSake;
	}

	public String getAppointmentID() {
		return appointmentID;
	}

	public void setAppointmentID(String appointmentID) {
		this.appointmentID = appointmentID;
	}

	public int getSakeOrderID() {
		return sakeOrderID;
	}

	public void setSakeOrderID(int sakeOrderID) {
		this.sakeOrderID = sakeOrderID;
	}

	public int getSakeID() {
		return sakeID;
	}

	public void setSakeID(int sakeID) {
		this.sakeID = sakeID;
	}

	public String getSakeName() {
		return sakeName;
	}

	public void setSakeName(String sakeName) {
		this.sakeName = sakeName;
	}

	public int getSakeQuantity() {
		return sakeQuantity;
	}

	public void setSakeQuantity(int sakeQuantity) {
		this.sakeQuantity = sakeQuantity;
	}

	public int getClientID() {
		return clientID;
	}

	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(String dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public String getPriceType() {
		return priceType;
	}

	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getTotalPriceBeforeDiscount() {
		return totalPriceBeforeDiscount;
	}

	public void setTotalPriceBeforeDiscount(double totalPriceBeforeDiscount) {
		this.totalPriceBeforeDiscount = totalPriceBeforeDiscount;
	}

	public double getTotalPriceAfterDiscount() {
		return totalPriceAfterDiscount;
	}

	public void setTotalPriceAfterDiscount(double totalPriceAfterDiscount) {
		this.totalPriceAfterDiscount = totalPriceAfterDiscount;
	}

}
