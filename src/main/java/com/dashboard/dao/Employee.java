package com.dashboard.dao;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "")
public class Employee {

	int empID;
	String loginID;
	String password;
	String contactNumber;
	String empName;
	String dateOfBirth;
	String address;
	String dateJoined;
	String position;
	String employeeImage;
	String employeeEmail;
	int status;

	public Employee(){}
	
	public Employee(String loginID, String password, String contactNumber, String name, String dateOfBirth,
			String address, String dateJoined, String position, String employeeImage, String employeeEmail,
			int status) {
		super();

		this.loginID = loginID;
		this.password = password;
		this.contactNumber = contactNumber;
		this.empName = name;
		this.dateOfBirth = dateOfBirth;
		this.address = address;
		this.dateJoined = dateJoined;
		this.position = position;
		this.employeeImage = employeeImage;
		this.employeeEmail = employeeEmail;
		this.status = status;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getEmployeeEmail() {
		return employeeEmail;
	}

	public void setEmployeeEmail(String employeeEmail) {
		this.employeeEmail = employeeEmail;
	}

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public String getLoginID() {
		return loginID;
	}

	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getDateJoined() {
		return dateJoined;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDateJoiend() {
		return dateJoined;
	}

	public void setDateJoined(String dateJoined) {
		this.dateJoined = dateJoined;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getEmployeeImage() {
		return employeeImage;
	}

	public void setEmployeeImage(String employeeImage) {
		this.employeeImage = employeeImage;
	}

}
