package com.dashboard.dao;

public class AllClients {
	int clientID;
	int empID;
	String clientName;
	String parentCompany;
	String clientContactNumber;
	String clientEmail;
	String clientCategory;
	String empName;
	String POC1Name;
	String POC1ContactNumber;
	String POC1Email;
	String POC2Name;
	String POC2ContactNumber;
	String POC2Email;
	String clientDateAdded;
	String clientShippingAddress;
	String clientBillingAddress;
	String clientType;
	int clientCategoryID;
	String clientSourceOfClient;

	public AllClients() {
	}

	public AllClients(int clientID, String clientName, String parentCompany, String clientContactNumber,
			String clientEmail, String clientCategory, String empName, String POC1Name, String POC1ContactNumber,
			String POC1Email, String POC2Name, String POC2ContactNumber, String POC2Email, String clientDateAdded,
			String clientShippingAddress, String clientBillingAddress) {

		this.clientID = clientID;
		this.clientName = clientName;
		this.parentCompany = parentCompany;
		this.clientContactNumber = clientContactNumber;
		this.clientEmail = clientEmail;
		this.clientCategory = clientCategory;
		this.empName = empName;
		this.POC1Name = POC1Name;
		this.POC1ContactNumber = POC1ContactNumber;
		this.POC1Email = POC1Email;
		this.POC2Name = POC2Name;
		this.POC2ContactNumber = POC2ContactNumber;
		this.POC2Email = POC2Email;
		this.clientDateAdded = clientDateAdded;
		this.clientShippingAddress = clientShippingAddress;
		this.clientBillingAddress = clientBillingAddress;

	}

	public AllClients(int clientID, int empID, String clientName, String parentCompany, String clientContactNumber,
			String clientEmail, int clientCategoryID, String clientBillingAddress, String clientShippingAddress,
			String POC1Name, String POC1ContactNumber, String POC1Email, String POC2Name, String POC2ContactNumber,
			String POC2Email) {
		this.clientID = clientID;
		this.empID = empID;
		this.clientName = clientName;
		this.parentCompany = parentCompany;
		this.clientContactNumber = clientContactNumber;
		this.clientEmail = clientEmail;
		this.clientCategoryID = clientCategoryID;
		this.clientBillingAddress = clientBillingAddress;
		this.clientShippingAddress = clientShippingAddress;
		this.POC1Name = POC1Name;
		this.POC1ContactNumber = POC1ContactNumber;
		this.POC1Email = POC1Email;
		this.POC2Name = POC2Name;
		this.POC2ContactNumber = POC2ContactNumber;
		this.POC2Email = POC2Email;
	}

	public AllClients(int empID, String clientName, String parentCompany, String clientContactNumber,
			String clientEmail, String clientType, int clientCategoryID, String clientBillingAddress,
			String clientShippingAddress, String POC1Name, String POC1ContactNumber, String POC1Email, String POC2Name,
			String POC2ContactNumber, String POC2Email, String clientDateAdded) {

		this.empID = empID;
		this.clientName = clientName;
		this.parentCompany = parentCompany;
		this.clientContactNumber = clientContactNumber;
		this.clientEmail = clientEmail;
		this.clientType = clientType;
		this.clientCategoryID = clientCategoryID;
		this.clientBillingAddress = clientBillingAddress;
		this.clientShippingAddress = clientShippingAddress;
		this.POC1Name = POC1Name;
		this.POC1ContactNumber = POC1ContactNumber;
		this.POC1Email = POC1Email;
		this.POC2Name = POC2Name;
		this.POC2ContactNumber = POC2ContactNumber;
		this.POC2Email = POC2Email;
		this.clientDateAdded = clientDateAdded;

	}

	public AllClients(int clientID, String clientName) {
		this.clientID = clientID;
		this.clientName = clientName;
	}

	public String getClientSourceOfClient() {
		return clientSourceOfClient;
	}

	public void setClientSourceOfClient(String clientSourceOfClient) {
		this.clientSourceOfClient = clientSourceOfClient;
	}

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	public int getClientCategoryID() {
		return clientCategoryID;
	}

	public void setClientCategoryID(int clientCategoryID) {
		this.clientCategoryID = clientCategoryID;
	}

	public int getClientID() {
		return clientID;
	}

	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getParentCompany() {
		return parentCompany;
	}

	public void setParentCompany(String parentCompany) {
		this.parentCompany = parentCompany;
	}

	public String getClientContactNumber() {
		return clientContactNumber;
	}

	public void setClientContactNumber(String clientContactNumber) {
		this.clientContactNumber = clientContactNumber;
	}

	public String getClientEmail() {
		return clientEmail;
	}

	public void setClientEmail(String clientEmail) {
		this.clientEmail = clientEmail;
	}

	public String getClientCategory() {
		return clientCategory;
	}

	public void setClientCategory(String clientCategory) {
		this.clientCategory = clientCategory;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getPOC1Name() {
		return POC1Name;
	}

	public void setPOC1Name(String pOC1Name) {
		POC1Name = pOC1Name;
	}

	public String getPOC1ContactNumber() {
		return POC1ContactNumber;
	}

	public void setPOC1ContactNumber(String pOC1ContactNumber) {
		POC1ContactNumber = pOC1ContactNumber;
	}

	public String getPOC1Email() {
		return POC1Email;
	}

	public void setPOC1Email(String pOC1Email) {
		POC1Email = pOC1Email;
	}

	public String getPOC2Name() {
		return POC2Name;
	}

	public void setPOC2Name(String pOC2Name) {
		POC2Name = pOC2Name;
	}

	public String getPOC2ContactNumber() {
		return POC2ContactNumber;
	}

	public void setPOC2ContactNumber(String pOC2ContactNumber) {
		POC2ContactNumber = pOC2ContactNumber;
	}

	public String getPOC2Email() {
		return POC2Email;
	}

	public void setPOC2Email(String pOC2Email) {
		POC2Email = pOC2Email;
	}

	public String getClientDateAdded() {
		return clientDateAdded;
	}

	public void setClientDateAdded(String clientDateAdded) {
		this.clientDateAdded = clientDateAdded;
	}

	public String getClientShippingAddress() {
		return clientShippingAddress;
	}

	public void setClientShippingAddress(String clientShippingAddress) {
		this.clientShippingAddress = clientShippingAddress;
	}

	public String getClientBillingAddress() {
		return clientBillingAddress;
	}

	public void setClientBillingAddress(String clientBillingAddress) {
		this.clientBillingAddress = clientBillingAddress;
	}

}
