package com.dashboard.dao;

public class QuotationAmount {
	double quotationAmount;

	public double getQuotationAmount() {
		return quotationAmount;
	}

	public void setQuotationAmount(double quotationAmount) {
		this.quotationAmount = quotationAmount;
	}

}
