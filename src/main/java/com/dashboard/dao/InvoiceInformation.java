package com.dashboard.dao;

public class InvoiceInformation {
	int invoiceID;
	String clientBillingAddress;
	String clientShippingAddress;
	String POC1Name;
	String POC1ContactNumber;
	String POC1Email;
	String appointmentID;
	int sakeOrderID;
	int sakeorder;
	int sakeID;
	String sakeName;
	int sakeQuantity;
	int clientID;
	String clientName;
	int empID;
	String empName;
	String dateOrdered;
	String priceType;
	double discount;
	double totalPriceBeforeDiscount;
	double totalPriceAfterDiscount;
	String skuNumber;
	double pricePerSake;
	double invoiceAmount;
	double GST;
	double totalInvoiceBeforeGST;	

	public int getInvoiceID() {
		return invoiceID;
	}

	public void setInvoiceID(int invoiceID) {
		this.invoiceID = invoiceID;
	}

	public String getClientBillingAddress() {
		return clientBillingAddress;
	}

	public void setClientBillingAddress(String clientBillingAddress) {
		this.clientBillingAddress = clientBillingAddress;
	}

	public String getClientShippingAddress() {
		return clientShippingAddress;
	}

	public void setClientShippingAddress(String clientShippingAddress) {
		this.clientShippingAddress = clientShippingAddress;
	}

	public String getPOC1ContactNumber() {
		return POC1ContactNumber;
	}

	public void setPOC1ContactNumber(String pOC1ContactNumber) {
		POC1ContactNumber = pOC1ContactNumber;
	}

	public String getPOC1Email() {
		return POC1Email;
	}

	public void setPOC1Email(String pOC1Email) {
		POC1Email = pOC1Email;
	}

	public String getAppointmentID() {
		return appointmentID;
	}

	public void setAppointmentID(String appointmentID) {
		this.appointmentID = appointmentID;
	}

	public int getSakeOrderID() {
		return sakeOrderID;
	}

	public void setSakeOrderID(int sakeOrderID) {
		this.sakeOrderID = sakeOrderID;
	}

	public int getSakeorder() {
		return sakeorder;
	}

	public void setSakeorder(int sakeorder) {
		this.sakeorder = sakeorder;
	}

	public int getSakeID() {
		return sakeID;
	}

	public void setSakeID(int sakeID) {
		this.sakeID = sakeID;
	}

	public String getSakeName() {
		return sakeName;
	}

	public void setSakeName(String sakeName) {
		this.sakeName = sakeName;
	}

	public int getSakeQuantity() {
		return sakeQuantity;
	}

	public void setSakeQuantity(int sakeQuantity) {
		this.sakeQuantity = sakeQuantity;
	}

	public int getClientID() {
		return clientID;
	}

	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(String dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public String getPriceType() {
		return priceType;
	}

	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public double getTotalPriceBeforeDiscount() {
		return totalPriceBeforeDiscount;
	}

	public void setTotalPriceBeforeDiscount(double totalPriceBeforeDiscount) {
		this.totalPriceBeforeDiscount = totalPriceBeforeDiscount;
	}

	public double getTotalPriceAfterDiscount() {
		return totalPriceAfterDiscount;
	}

	public void setTotalPriceAfterDiscount(double totalPriceAfterDiscount) {
		this.totalPriceAfterDiscount = totalPriceAfterDiscount;
	}

	public String getSkuNumber() {
		return skuNumber;
	}

	public void setSkuNumber(String skuNumber) {
		this.skuNumber = skuNumber;
	}

	public double getPricePerSake() {
		return pricePerSake;
	}

	public void setPricePerSake(double pricePerSake) {
		this.pricePerSake = pricePerSake;
	}

	public double getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public double getGST() {
		return GST;
	}

	public void setGST(double gST) {
		GST = gST;
	}

	public double getTotalInvoiceBeforeGST() {
		return totalInvoiceBeforeGST;
	}

	public void setTotalInvoiceBeforeGST(double totalInvoiceBeforeGST) {
		this.totalInvoiceBeforeGST = totalInvoiceBeforeGST;
	}

}
