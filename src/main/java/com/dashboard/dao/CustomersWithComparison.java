package com.dashboard.dao;

public class CustomersWithComparison {
	int numberOfNewCustomersThisMonth;
	int numberOfNewCustomersLastMonth;
	double percentage;

	public int getNumberOfNewCustomersThisMonth() {
		return numberOfNewCustomersThisMonth;
	}

	public void setNumberOfNewCustomersThisMonth(int numberOfNewCustomersThisMonth) {
		this.numberOfNewCustomersThisMonth = numberOfNewCustomersThisMonth;
	}

	public int getNumberOfNewCustomersLastMonth() {
		return numberOfNewCustomersLastMonth;
	}

	public void setNumberOfNewCustomersLastMonth(int numberOfNewCustomersLastMonth) {
		this.numberOfNewCustomersLastMonth = numberOfNewCustomersLastMonth;
	}

	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}

}
