package com.dashboard.dao;

public class InvoiceIDFromAppointment {
	int invoiceID;

	public int getInvoiceID() {
		return invoiceID;
	}

	public void setInvoiceID(int invoiceID) {
		this.invoiceID = invoiceID;
	}
}
