package com.dashboard.dao;

public class EmployeeIndividualSixMonthDataNewProspect {
	int empID;
	int thisMonth;
	int lastMonth;
	int twoMonthsAgo;
	int threeMonthsAgo;
	int fourMonthsAgo;
	int fiveMonthsAgo;

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public int getThisMonth() {
		return thisMonth;
	}

	public void setThisMonth(int thisMonth) {
		this.thisMonth = thisMonth;
	}

	public int getLastMonth() {
		return lastMonth;
	}

	public void setLastMonth(int lastMonth) {
		this.lastMonth = lastMonth;
	}

	public int getTwoMonthsAgo() {
		return twoMonthsAgo;
	}

	public void setTwoMonthsAgo(int twoMonthsAgo) {
		this.twoMonthsAgo = twoMonthsAgo;
	}

	public int getThreeMonthsAgo() {
		return threeMonthsAgo;
	}

	public void setThreeMonthsAgo(int threeMonthsAgo) {
		this.threeMonthsAgo = threeMonthsAgo;
	}

	public int getFourMonthsAgo() {
		return fourMonthsAgo;
	}

	public void setFourMonthsAgo(int fourMonthsAgo) {
		this.fourMonthsAgo = fourMonthsAgo;
	}

	public int getFiveMonthsAgo() {
		return fiveMonthsAgo;
	}

	public void setFiveMonthsAgo(int fiveMonthsAgo) {
		this.fiveMonthsAgo = fiveMonthsAgo;
	}
}
