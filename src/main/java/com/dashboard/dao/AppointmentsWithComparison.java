package com.dashboard.dao;

public class AppointmentsWithComparison {
	int thisMonthAppointments;
	int pastMonthAppointments;
	double percentage;

	public int getThisMonthAppointments() {
		return thisMonthAppointments;
	}

	public void setThisMonthAppointments(int thisMonthAppointments) {
		this.thisMonthAppointments = thisMonthAppointments;
	}

	public int getPastMonthAppointments() {
		return pastMonthAppointments;
	}

	public void setPastMonthAppointments(int pastMonthAppointments) {
		this.pastMonthAppointments = pastMonthAppointments;
	}

	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}
}
