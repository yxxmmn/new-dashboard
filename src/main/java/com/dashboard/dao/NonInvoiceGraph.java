package com.dashboard.dao;

public class NonInvoiceGraph {
	String clientDateAdded;
	String clientDateBecomeProspect;
	String clientDateBecomeCustomer;
	String appointmentDate;
	String month;
	int numberOfAppointments;
	int numberOfContacts;
	int numberOfProspects;
	int numberOfCustomers;

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public int getNumberOfAppointments() {
		return numberOfAppointments;
	}

	public void setNumberOfAppointments(int numberOfAppointments) {
		this.numberOfAppointments = numberOfAppointments;
	}

	public int getNumberOfContacts() {
		return numberOfContacts;
	}

	public void setNumberOfContacts(int numberOfContacts) {
		this.numberOfContacts = numberOfContacts;
	}

	public int getNumberOfProspects() {
		return numberOfProspects;
	}

	public void setNumberOfProspects(int numberOfProspects) {
		this.numberOfProspects = numberOfProspects;
	}

	public int getNumberOfCustomers() {
		return numberOfCustomers;
	}

	public void setNumberOfCustomers(int numberOfCustomers) {
		this.numberOfCustomers = numberOfCustomers;
	}

	public String getClientDateAdded() {
		return clientDateAdded;
	}

	public void setClientDateAdded(String clientDateAdded) {
		this.clientDateAdded = clientDateAdded;
	}

	public String getClientDateBecomeProspect() {
		return clientDateBecomeProspect;
	}

	public void setClientDateBecomeProspect(String clientDateBecomeProspect) {
		this.clientDateBecomeProspect = clientDateBecomeProspect;
	}

	public String getClientDateBecomeCustomer() {
		return clientDateBecomeCustomer;
	}

	public void setClientDateBecomeCustomer(String clientDateBecomeCustomer) {
		this.clientDateBecomeCustomer = clientDateBecomeCustomer;
	}

	public String getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

}