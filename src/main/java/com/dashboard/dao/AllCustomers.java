package com.dashboard.dao;

public class AllCustomers {
	int customerID;
	String customerName;
	String customerCategory;
	String empName;
	String billingAddress;
	String shippingAddress;
	String appointmentDate;
	String customerEmail;
	int contactNumber;
	int empID;
	String dateBecomeCustomer;
	String dateAdded;

	public String getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(String dateAdded) {
		this.dateAdded = dateAdded;
	}

	public String getDateBecomeCustomer() {
		return dateBecomeCustomer;
	}

	public void setDateBecomeCustomer(String dateBecomeCustomer) {
		this.dateBecomeCustomer = dateBecomeCustomer;
	}

	public AllCustomers() {
	}

	public AllCustomers(int customerID, int empID, String customerName, String customerCategory, String billingAddress,
			String shippingAddress, String customerEmail, int contactNumber) {
		this.customerID = customerID;
		this.empID = empID;
		this.customerName = customerName;
		this.customerCategory = customerCategory;
		this.billingAddress = billingAddress;
		this.shippingAddress = shippingAddress;
		this.customerEmail = customerEmail;
		this.contactNumber = contactNumber;
	}

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public int getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(int contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerCategory() {
		return customerCategory;
	}

	public void setCustomerCategory(String customerCategory) {
		this.customerCategory = customerCategory;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public String getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

}
