package com.dashboard.dao;

public class BestSeller {
	double invoiceAmount;
	double totalQuantity;
	String sakeBrand;

	public double getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public double getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(double totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public String getSakeBrand() {
		return sakeBrand;
	}

	public void setSakeBrand(String sakeBrand) {
		this.sakeBrand = sakeBrand;
	} 
}
