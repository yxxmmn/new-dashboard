package com.dashboard.dao;

public class NumberOfNewContacts {
	int numberOfNewContacts;

	public int getNumberOfNewContacts() {
		return numberOfNewContacts;
	}

	public void setNumberOfNewContacts(int numberOfNewContacts) {
		this.numberOfNewContacts = numberOfNewContacts;
	}

}
