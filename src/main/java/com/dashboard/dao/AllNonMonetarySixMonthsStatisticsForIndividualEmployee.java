package com.dashboard.dao;

public class AllNonMonetarySixMonthsStatisticsForIndividualEmployee {

	int empID;

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	int thisMonthYear;
	int thisMonth;
	int lastMonthYear;
	int lastMonth;
	int twoMonthsAgoYear;
	int twoMonthsAgo;
	int threeMonthsAgoYear;
	int threeMonthsAgo;
	int fourMonthsAgoYear;
	int fourMonthsAgo;
	int fiveMonthsAgoYear;
	int fiveMonthsAgo;

	int thisMonthAppointments;
	int lastMonthAppointments;
	int twoMonthsAgoAppointments;
	int threeMonthsAgoAppointments;
	int fourMonthsAgoAppointments;
	int fiveMonthsAgoAppointments;

	int thisMonthNewContacts;
	int lastMonthNewContacts;
	int twoMonthsAgoNewContacts;
	int threeMonthsAgoNewContacts;
	int fourMonthsAgoNewContacts;
	int fiveMonthsAgoNewContacts;

	int thisMonthNewProspects;
	int lastMonthNewProspects;
	int twoMonthsAgoNewProspects;
	int threeMonthsAgoNewProspects;
	int fourMonthsAgoNewProspects;
	int fiveMonthsAgoNewProspects;

	int thisMonthNewCustomers;
	int lastMonthNewCustomers;
	int twoMonthsAgoNewCustomers;
	int threeMonthsAgoNewCustomers;
	int fourMonthsAgoNewCustomers;
	int fiveMonthsAgoNewCustomers;

	public int getThisMonthYear() {
		return thisMonthYear;
	}

	public void setThisMonthYear(int thisMonthYear) {
		this.thisMonthYear = thisMonthYear;
	}

	public int getThisMonth() {
		return thisMonth;
	}

	public void setThisMonth(int thisMonth) {
		this.thisMonth = thisMonth;
	}

	public int getLastMonthYear() {
		return lastMonthYear;
	}

	public void setLastMonthYear(int lastMonthYear) {
		this.lastMonthYear = lastMonthYear;
	}

	public int getLastMonth() {
		return lastMonth;
	}

	public void setLastMonth(int lastMonth) {
		this.lastMonth = lastMonth;
	}

	public int getTwoMonthsAgoYear() {
		return twoMonthsAgoYear;
	}

	public void setTwoMonthsAgoYear(int twoMonthsAgoYear) {
		this.twoMonthsAgoYear = twoMonthsAgoYear;
	}

	public int getTwoMonthsAgo() {
		return twoMonthsAgo;
	}

	public void setTwoMonthsAgo(int twoMonthsAgo) {
		this.twoMonthsAgo = twoMonthsAgo;
	}

	public int getThreeMonthsAgoYear() {
		return threeMonthsAgoYear;
	}

	public void setThreeMonthsAgoYear(int threeMonthsAgoYear) {
		this.threeMonthsAgoYear = threeMonthsAgoYear;
	}

	public int getThreeMonthsAgo() {
		return threeMonthsAgo;
	}

	public void setThreeMonthsAgo(int threeMonthsAgo) {
		this.threeMonthsAgo = threeMonthsAgo;
	}

	public int getFourMonthsAgoYear() {
		return fourMonthsAgoYear;
	}

	public void setFourMonthsAgoYear(int fourMonthsAgoYear) {
		this.fourMonthsAgoYear = fourMonthsAgoYear;
	}

	public int getFourMonthsAgo() {
		return fourMonthsAgo;
	}

	public void setFourMonthsAgo(int fourMonthsAgo) {
		this.fourMonthsAgo = fourMonthsAgo;
	}

	public int getFiveMonthsAgoYear() {
		return fiveMonthsAgoYear;
	}

	public void setFiveMonthsAgoYear(int fiveMonthsAgoYear) {
		this.fiveMonthsAgoYear = fiveMonthsAgoYear;
	}

	public int getFiveMonthsAgo() {
		return fiveMonthsAgo;
	}

	public void setFiveMonthsAgo(int fiveMonthsAgo) {
		this.fiveMonthsAgo = fiveMonthsAgo;
	}

	public int getThisMonthAppointments() {
		return thisMonthAppointments;
	}

	public void setThisMonthAppointments(int thisMonthAppointments) {
		this.thisMonthAppointments = thisMonthAppointments;
	}

	public int getLastMonthAppointments() {
		return lastMonthAppointments;
	}

	public void setLastMonthAppointments(int lastMonthAppointments) {
		this.lastMonthAppointments = lastMonthAppointments;
	}

	public int getTwoMonthsAgoAppointments() {
		return twoMonthsAgoAppointments;
	}

	public void setTwoMonthsAgoAppointments(int twoMonthsAgoAppointments) {
		this.twoMonthsAgoAppointments = twoMonthsAgoAppointments;
	}

	public int getThreeMonthsAgoAppointments() {
		return threeMonthsAgoAppointments;
	}

	public void setThreeMonthsAgoAppointments(int threeMonthsAgoAppointments) {
		this.threeMonthsAgoAppointments = threeMonthsAgoAppointments;
	}

	public int getFourMonthsAgoAppointments() {
		return fourMonthsAgoAppointments;
	}

	public void setFourMonthsAgoAppointments(int fourMonthsAgoAppointments) {
		this.fourMonthsAgoAppointments = fourMonthsAgoAppointments;
	}

	public int getFiveMonthsAgoAppointments() {
		return fiveMonthsAgoAppointments;
	}

	public void setFiveMonthsAgoAppointments(int fiveMonthsAgoAppointments) {
		this.fiveMonthsAgoAppointments = fiveMonthsAgoAppointments;
	}

	public int getThisMonthNewContacts() {
		return thisMonthNewContacts;
	}

	public void setThisMonthNewContacts(int thisMonthNewContacts) {
		this.thisMonthNewContacts = thisMonthNewContacts;
	}

	public int getLastMonthNewContacts() {
		return lastMonthNewContacts;
	}

	public void setLastMonthNewContacts(int lastMonthNewContacts) {
		this.lastMonthNewContacts = lastMonthNewContacts;
	}

	public int getTwoMonthsAgoNewContacts() {
		return twoMonthsAgoNewContacts;
	}

	public void setTwoMonthsAgoNewContacts(int twoMonthsAgoNewContacts) {
		this.twoMonthsAgoNewContacts = twoMonthsAgoNewContacts;
	}

	public int getThreeMonthsAgoNewContacts() {
		return threeMonthsAgoNewContacts;
	}

	public void setThreeMonthsAgoNewContacts(int threeMonthsAgoNewContacts) {
		this.threeMonthsAgoNewContacts = threeMonthsAgoNewContacts;
	}

	public int getFourMonthsAgoNewContacts() {
		return fourMonthsAgoNewContacts;
	}

	public void setFourMonthsAgoNewContacts(int fourMonthsAgoNewContacts) {
		this.fourMonthsAgoNewContacts = fourMonthsAgoNewContacts;
	}

	public int getFiveMonthsAgoNewContacts() {
		return fiveMonthsAgoNewContacts;
	}

	public void setFiveMonthsAgoNewContacts(int fiveMonthsAgoNewContacts) {
		this.fiveMonthsAgoNewContacts = fiveMonthsAgoNewContacts;
	}

	public int getThisMonthNewProspects() {
		return thisMonthNewProspects;
	}

	public void setThisMonthNewProspects(int thisMonthNewProspects) {
		this.thisMonthNewProspects = thisMonthNewProspects;
	}

	public int getLastMonthNewProspects() {
		return lastMonthNewProspects;
	}

	public void setLastMonthNewProspects(int lastMonthNewProspects) {
		this.lastMonthNewProspects = lastMonthNewProspects;
	}

	public int getTwoMonthsAgoNewProspects() {
		return twoMonthsAgoNewProspects;
	}

	public void setTwoMonthsAgoNewProspects(int twoMonthsAgoNewProspects) {
		this.twoMonthsAgoNewProspects = twoMonthsAgoNewProspects;
	}

	public int getThreeMonthsAgoNewProspects() {
		return threeMonthsAgoNewProspects;
	}

	public void setThreeMonthsAgoNewProspects(int threeMonthsAgoNewProspects) {
		this.threeMonthsAgoNewProspects = threeMonthsAgoNewProspects;
	}

	public int getFourMonthsAgoNewProspects() {
		return fourMonthsAgoNewProspects;
	}

	public void setFourMonthsAgoNewProspects(int fourMonthsAgoNewProspects) {
		this.fourMonthsAgoNewProspects = fourMonthsAgoNewProspects;
	}

	public int getFiveMonthsAgoNewProspects() {
		return fiveMonthsAgoNewProspects;
	}

	public void setFiveMonthsAgoNewProspects(int fiveMonthsAgoNewProspects) {
		this.fiveMonthsAgoNewProspects = fiveMonthsAgoNewProspects;
	}

	public int getThisMonthNewCustomers() {
		return thisMonthNewCustomers;
	}

	public void setThisMonthNewCustomers(int thisMonthNewCustomers) {
		this.thisMonthNewCustomers = thisMonthNewCustomers;
	}

	public int getLastMonthNewCustomers() {
		return lastMonthNewCustomers;
	}

	public void setLastMonthNewCustomers(int lastMonthNewCustomers) {
		this.lastMonthNewCustomers = lastMonthNewCustomers;
	}

	public int getTwoMonthsAgoNewCustomers() {
		return twoMonthsAgoNewCustomers;
	}

	public void setTwoMonthsAgoNewCustomers(int twoMonthsAgoNewCustomers) {
		this.twoMonthsAgoNewCustomers = twoMonthsAgoNewCustomers;
	}

	public int getThreeMonthsAgoNewCustomers() {
		return threeMonthsAgoNewCustomers;
	}

	public void setThreeMonthsAgoNewCustomers(int threeMonthsAgoNewCustomers) {
		this.threeMonthsAgoNewCustomers = threeMonthsAgoNewCustomers;
	}

	public int getFourMonthsAgoNewCustomers() {
		return fourMonthsAgoNewCustomers;
	}

	public void setFourMonthsAgoNewCustomers(int fourMonthsAgoNewCustomers) {
		this.fourMonthsAgoNewCustomers = fourMonthsAgoNewCustomers;
	}

	public int getFiveMonthsAgoNewCustomers() {
		return fiveMonthsAgoNewCustomers;
	}

	public void setFiveMonthsAgoNewCustomers(int fiveMonthsAgoNewCustomers) {
		this.fiveMonthsAgoNewCustomers = fiveMonthsAgoNewCustomers;
	}

}
