package com.dashboard.dao;

public class AllEmployees {
	int empID;
	String loginID;
	String password;
	String empContactNumber;
	String empName;
	String empDOB;
	String empAddress;
	String empDateJoined;
	String positionName;
	String empEmail;
	int empStatus;
	int empPosition;

	public AllEmployees() {
	}

	public AllEmployees(String loginID, String password, String empContactNumber, String empName, String empDOB,
			String empAddress, String empDateJoined, String empEmail, int empPosition, int empStatus) {
		super();
		this.loginID = loginID;
		this.password = password;
		this.empContactNumber = empContactNumber;
		this.empName = empName;
		this.empDOB = empDOB;
		this.empAddress = empAddress;
		this.empDateJoined = empDateJoined;
		this.empPosition = empPosition;
		this.empEmail = empEmail;
		this.empStatus = empStatus;
	}

	public AllEmployees(String loginID, String password) {
		this.loginID = loginID;
		this.password = password;
	}

	public int getEmpPosition() {
		return empPosition;
	}

	public void setEmpPosition(int empPosition) {
		this.empPosition = empPosition;
	}

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public String getLoginID() {
		return loginID;
	}

	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmpContactNumber() {
		return empContactNumber;
	}

	public void setEmpContactNumber(String empContactNumber) {
		this.empContactNumber = empContactNumber;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpDOB() {
		return empDOB;
	}

	public void setEmpDOB(String empDOB) {
		this.empDOB = empDOB;
	}

	public String getEmpAddress() {
		return empAddress;
	}

	public void setEmpAddress(String empAddress) {
		this.empAddress = empAddress;
	}

	public String getEmpDateJoined() {
		return empDateJoined;
	}

	public void setEmpDateJoined(String empDateJoined) {
		this.empDateJoined = empDateJoined;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public String getEmpEmail() {
		return empEmail;
	}

	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}

	public int getEmpStatus() {
		return empStatus;
	}

	public void setEmpStatus(int empStatus) {
		this.empStatus = empStatus;
	}

}
