package com.dashboard.controller.header;

import java.util.List;
import java.util.Map;

import org.sql2o.Connection;

import com.dashboard.dashboardcloud.model.dao.GenericDao;

@Deprecated
public class LoadMapCommand extends Command2<List<Map<String, Object>>> {

	String selectQry = null;

	public LoadMapCommand(@SuppressWarnings("rawtypes") GenericDao dao) {
		super(null);
		selectQry = dao.getSelectStatement();
	}

	@Override
	public List<Map<String, Object>> run(Connection conn) {
		List<Map<String, Object>> lst = conn.createQuery(selectQry).executeAndFetchTable().asList();
		return lst;

	}

	public static void main(String[] args) {
	}

}
