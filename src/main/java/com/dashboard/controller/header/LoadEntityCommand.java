package com.dashboard.controller.header;

import java.util.List;

import org.sql2o.Connection;

import com.dashboard.dashboardcloud.model.dao.GenericDao;

@SuppressWarnings("rawtypes")
public class LoadEntityCommand<T extends List<? extends GenericDao>> extends Command2<T> {

	GenericDao dao = null;
	String selectQry = null;

	public LoadEntityCommand(GenericDao dao) {
		super(null);
		this.dao = dao;
		selectQry = dao.getSelectStatement();
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public T run(Connection conn) {
		List<? extends GenericDao> lst = conn.createQuery(selectQry).executeAndFetch(dao.getClass());
		return (T) lst;

	}
}
