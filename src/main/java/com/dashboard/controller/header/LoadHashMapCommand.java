package com.dashboard.controller.header;

import java.util.HashMap;
import java.util.List;

import org.sql2o.Connection;

import com.dashboard.dashboardcloud.model.dao.GenericDao;

public class LoadHashMapCommand<K, V extends GenericDao<K>> extends Command2<HashMap<K, V>> {

	GenericDao<K> dao = null;
	String selectQry = null;

	public LoadHashMapCommand(GenericDao<K> dao) {
		super(null);
		this.dao = dao;
		selectQry = dao.getSelectStatement();
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public HashMap<K, V> run(Connection conn) {
		@SuppressWarnings("rawtypes")
		List<? extends GenericDao> lst = conn.createQuery(selectQry).executeAndFetch(dao.getClass());
		result = new HashMap<>();
		for (GenericDao<K> v : lst) {
			result.put(v.getId(), (V) v);
		}
		return result;
	}
}
