package com.dashboard.controller.header;

import java.sql.DriverManager;
import java.sql.SQLException;

import org.sql2o.Connection;

import com.dashboard.exception.GreenException;

public abstract class Command2<T> extends DatabaseConnection {

	// protected Connection conn;
	public T result;

	public Command2(T result) {
		this.result = result;
	}

	public T execute() {
		try (Connection conn = getSql2O().beginTransaction()) {
			result = run(conn);
			conn.commit();
		} catch (RuntimeException rt) {
			System.out.println(rt.getMessage());
			deReg();
			throw rt;
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			deReg();
			throw new GreenException(ex.getMessage());
		}
		return result;
	}

	private void deReg() {
		try {
			DriverManager.deregisterDriver(driver);
		} catch (SQLException e) {
		}
	}

	public abstract T run(Connection conn);
}
