package com.dashboard.controller.header;

import java.sql.DriverManager;
import java.sql.SQLException;

import org.sql2o.Sql2o;

import com.mysql.jdbc.Driver;

public class DatabaseConnection {

	public static String connectionString = null;
	public static boolean quickUpdate;
	protected Driver driver;
	// @Context ServletConfig context;

	public DatabaseConnection() {

	}

	protected Sql2o getSql2O() {
		String url = DatabaseConnection.connectionString;
		if (url == null) {
			url = "jdbc:mysql://172.104.39.184/interricetestdata?user=admin";
		}
		url += "&useSSL=false&useUnicode=yes&characterEncoding=UTF-8";
		String user = "admin";
		String pw = "123qwe!!";
		try {
			driver = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(driver);
		} catch (SQLException e) {
		}
		Sql2o sql2o = new Sql2o(url, user, pw);
		return sql2o;
	}
}
