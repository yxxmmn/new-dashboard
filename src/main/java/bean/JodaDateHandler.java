package bean;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.mortbay.log.Log;

import com.dashboard.dao.RFMGetDetailsByIDClass;

public class JodaDateHandler{
	public String todayDate() {
		new DateTime();
		DateTime now = DateTime.now();
		String pattern = "yyyy-MM-dd hh:mm:ss";
		DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
		String formatted = formatter.print(now);
		return formatted;
	}

	public String oneMonthAgo(String date) {
		String pattern = "yyyy-MM-dd hh:mm:ss";
		DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
		DateTime now = DateTime.parse(date, formatter);
		DateTime oneMonthAgo = now.minusMonths(1);
		String formatted = formatter.print(oneMonthAgo);
		return formatted;

	}

	public String oneYearAgo(String date) {
		String pattern = "yyyy-MM-dd hh:mm:ss";
		DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
		DateTime now = DateTime.parse(date, formatter);
		DateTime oneMonthAgo = now.minusYears(1);
		String formatted = formatter.print(oneMonthAgo);
		return formatted;

	}
	
	public String twoYearsAgo(String date) {
		String pattern = "yyyy-MM-dd hh:mm:ss";
		DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
		DateTime now = DateTime.parse(date, formatter);
		DateTime oneMonthAgo = now.minusYears(2);
		String formatted = formatter.print(oneMonthAgo);
		return formatted;

	}

	public DateTime convertStringToDate(String date) {
		String pattern = "yyyy-MM-dd hh:mm:ss";
		DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
		DateTime now = DateTime.parse(date, formatter);

		return now;
	}

	public String endOfToday() {
		new DateTime();
		DateTime now = DateTime.now();
		DateTime tomorrowStart = now.plusDays(1).withTimeAtStartOfDay();
		String pattern = "yyyy-MM-dd hh:mm:ss";
		DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
		String formatted = formatter.print(tomorrowStart);
		return formatted;
	}
	
	public int daysBeforeNowLastDateOrdered(List<RFMGetDetailsByIDClass> rList) {
		//Handles Date formats
		Date date = new Date(System.currentTimeMillis());

		String myDate = date + " 00:00:00"  ;

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		java.util.Date utilDate = new java.util.Date();
		java.util.Date lastDateOrdered = new java.util.Date();
		
		//Get Customer RFM details via query
		
		try {
			
		    utilDate =  sdf.parse(myDate);
		   lastDateOrdered = sdf.parse(rList.get(0).getLastDateOrdered()+ " 00:00:00");
		    
		} catch (ParseException pe) {
		   // TODO something.
			Log.warn(pe);
		}
		//Last datePurchased in DateTime
		DateTime lastDateOrderedDateTime = new DateTime(lastDateOrdered);
		//Current DateTime
		DateTime currentDateTime = new DateTime(utilDate); 
		return Days.daysBetween(lastDateOrderedDateTime, currentDateTime).getDays() ; // currentDate - lastDateOrdered
	}
	
	public int daysBeforeNowDateBecomeCust(List<RFMGetDetailsByIDClass> rList) {
		//Handles Date formats
		Date date = new Date(System.currentTimeMillis());

		String myDate = date + " 00:00:00"  ;

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
		java.util.Date utilDate = new java.util.Date();
		java.util.Date clientDateBecomeCustomer = new java.util.Date();
		
		//Get Customer RFM details via query
		try {	
		    utilDate = sdf.parse(myDate);
		   clientDateBecomeCustomer = sdf.parse(rList.get(0).getClientDateBecomeCustomer()+ " 00:00:00");
		    
		} catch (ParseException pe) {
		   // TODO something.
			Log.warn(pe);
		}

		//Current DateTime
		DateTime currentDateTime = new DateTime(utilDate); 
		// DateTime clientBecomeCustomer
		DateTime clientDateBecomeCustomerDateTime = new DateTime(clientDateBecomeCustomer);
		return Days.daysBetween(clientDateBecomeCustomerDateTime, currentDateTime).getDays() ; // currentDate - lastDateOrdered
	}

}
