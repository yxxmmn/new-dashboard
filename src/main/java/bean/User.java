package bean;  

public class User {  
	
	private String email;
	private String password;
	boolean isAdminStatus;
	  
	//Getter
	public String getEmail() {  
	    return email;  
	}
	public String getPass() {  
	    return password;  
	} 
	public boolean getAdminStatus(){
		return isAdminStatus;
	}
	//Setter
	public void setEmail(String email) {  
	    this.email = email;  
	}  
	//TODO: Change password field should utilize and alter database first
	public void setPass(String pass) {  
	    this.password = password;  
	}  
	public void setAdminStatus(boolean isAdminStatus){
		this.isAdminStatus = isAdminStatus;
	}
	
}  