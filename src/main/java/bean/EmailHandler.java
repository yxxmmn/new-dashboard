package bean;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

public class EmailHandler {
	public static void main(String[] args) {

	}

	public void sendEmail(String customerEmail, String newPassword) {
		final String username = "interricea@gmail.com";
		final String password = "password12345";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("xxyu.2014@sis.smu.edu.sg"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(customerEmail));
			message.setSubject("Interrice Asia Password Reset");
			message.setText("Hello," + "\n\n Your password has been reset to" + newPassword + ", please login to your account to change your password.");

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}
}
