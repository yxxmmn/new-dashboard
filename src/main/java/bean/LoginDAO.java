package bean;  
import java.sql.*;

import com.dashboard.dao.Employee;  
public class LoginDAO {  
  
public Employee validate(String email, String password) throws SQLException{  
		
		/*boolean status=false;
		
		try{  
			Connection con=ConnectionProvider.getCon();  
			
			//TODO: Change 'pass' to 'password' in database. And you mean table name right not database below?
			//Change the user432 to our database name and use input variable after connecting to the database
			PreparedStatement ps=con.prepareStatement( "select * from user432 where email=? and pass=?");  
			
			//This sets the first and second ? to the username and password
			ps.setString(1, user.getEmail());  
			ps.setString(2, user.getPass());  
			              
			ResultSet rs=ps.executeQuery();  
			
			String email = rs.getString("email");
			String password = rs.getString("pass");
			
			status=rs.next();
			
			//User does not exist -> failed login
			
			//User authentication successful
			              
		}catch(Exception e){
			System.out.println(e);
			System.out.println("Problem at LoginDAO");

		}  
		
		return status;  
	 */
		Employee currentUser = null; 
		boolean status=false;
		Connection con= ConnectionProvider.getCon(); 
		
		try{  			
			//TODO: And you mean table name right not database? Database should be fixed at connection manager entities
			//Change the user432 to our database name and use input variable after connecting to the database
			PreparedStatement ps=con.prepareStatement( "select * from Employee where email=?");  
			
			//This sets the first and second ? to the username and password
			ps.setString(1, email);    
			              
			ResultSet rs=ps.executeQuery();  
			String passwordResult = rs.getString("password");
			//Returning simply status might make debugging harder next time and test case more complicated since only return t/f.
			status=rs.next();
			
			//if passwordResult = null means user does not exist
			if(passwordResult!=null){
				if(password.equals(passwordResult)){
					//User authentication successful
					System.out.println("log in successful");
					//Return the Employee Object here!
					//Perhaps have another class to call
					//currentUser = Database.getEmployeeById(email)
					return currentUser;
				} else {
					//Email exist but authentication failed
					System.out.println("log in failed");
				}
			} else {
				//User does not exist -> failed login
				System.out.println("email does not exist");
			}
			            
		}catch(Exception e){
			System.out.println(e);
			System.out.println("Problem at LoginDAO");
		} finally {
			con.close();
		}
		return currentUser;
	} 


}
